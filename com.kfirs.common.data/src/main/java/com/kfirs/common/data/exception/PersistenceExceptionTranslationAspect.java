/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.data.exception;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.PersistenceExceptionTranslator;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 * @on 1/31/17.
 */
@Aspect
public class PersistenceExceptionTranslationAspect
{
    private static final Logger LOG = getLogger( PersistenceExceptionTranslationAspect.class );

    @Nullable
    private PersistenceExceptionTranslator persistenceExceptionTranslator;

    public void setPersistenceExceptionTranslator(
            @Nullable PersistenceExceptionTranslator persistenceExceptionTranslator )
    {
        this.persistenceExceptionTranslator = persistenceExceptionTranslator;
    }

    @Nullable
    @Around( value = "execution(@org.springframework.transaction.annotation.Transactional * *(..))",
             argNames = "joinPoint" )
    public Object aroundTransactionalMethod( final ProceedingJoinPoint joinPoint ) throws Throwable
    {
        return executeWithExceptionTranslation( joinPoint );
    }

    @Nullable
    private Object executeWithExceptionTranslation( @NotNull ProceedingJoinPoint joinPoint ) throws Throwable
    {
        if( this.persistenceExceptionTranslator != null )
        {
            try
            {
                return joinPoint.proceed( joinPoint.getArgs() );
            }
            catch( RuntimeException ex )
            {
                DataAccessException translated = this.persistenceExceptionTranslator.translateExceptionIfPossible( ex );
                if( translated != null )
                {
                    throw translated;
                }
                else
                {
                    throw ex;
                }
            }
        }
        else
        {
            LOG.warn( "PersistenceExceptionTranslator has not been set on {}", getClass().getSimpleName() );
            return joinPoint.proceed( joinPoint.getArgs() );
        }
    }
}
