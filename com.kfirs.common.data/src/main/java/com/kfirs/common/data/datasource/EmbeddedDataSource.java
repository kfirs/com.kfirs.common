/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.data.datasource;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfiguration;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import com.kfirs.common.util.lang.NotNull;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Objects;
import java.util.stream.Stream;
import javax.sql.DataSource;
import org.mariadb.jdbc.MariaDbDataSource;
import org.slf4j.Logger;

import static java.util.stream.Collectors.joining;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 * @on 5/27/17.
 */
public class EmbeddedDataSource implements DataSource
{
    private static final Logger LOG = getLogger( EmbeddedDataSource.class );

    @NotNull
    private static final String SQL_MODES = Stream.of( "STRICT_TRANS_TABLES",
                                                       "ERROR_FOR_DIVISION_BY_ZERO",
                                                       "NO_AUTO_CREATE_USER",
                                                       "NO_ENGINE_SUBSTITUTION",
                                                       "NO_AUTO_VALUE_ON_ZERO",
                                                       "NO_ZERO_DATE",
                                                       "ONLY_FULL_GROUP_BY" )
                                                  .collect( joining( "," ) );

    @NotNull
    private final Path path;

    @NotNull
    private final DB database;

    @NotNull
    private final DataSource target;

    public EmbeddedDataSource( @NotNull Path workDir, int port, @NotNull String schema ) throws SQLException
    {
        this.path = workDir.toAbsolutePath().normalize();
        this.database = createAndStartDatabase( this.path, port );
        try
        {
            this.database.run( "DROP SCHEMA IF EXISTS " + schema );
            this.database.run( "CREATE SCHEMA IF NOT EXISTS " + schema + " DEFAULT CHARACTER SET utf8" );
        }
        catch( ManagedProcessException e )
        {
            try
            {
                this.database.stop();
            }
            catch( ManagedProcessException e1 )
            {
                LOG.error( "Failed stopping database (stoping because schema '{}' could not be created)", schema, e1 );
            }
            throw new IllegalStateException( "failed creating schema '" + schema + "'", e );
        }
        this.target = new MariaDbDataSource( "localhost", port, schema );
    }

    public void stopDatabase() throws Exception
    {
        try
        {
            this.database.stop();
            LOG.info( "Deleting MariaDB directory at: {}", this.path );
        }
        catch( ManagedProcessException e )
        {
            LOG.error( "Failed stopping MariaDB database '{}'", e );
        }
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        return target.getConnection();
    }

    @Override
    public Connection getConnection( String username, String password ) throws SQLException
    {
        return target.getConnection( username, password );
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException
    {
        return target.getLogWriter();
    }

    @Override
    public void setLogWriter( PrintWriter out ) throws SQLException
    {
        target.setLogWriter( out );
    }

    @Override
    public void setLoginTimeout( int seconds ) throws SQLException
    {
        target.setLoginTimeout( seconds );
    }

    @Override
    public int getLoginTimeout() throws SQLException
    {
        return target.getLoginTimeout();
    }

    @Override
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException
    {
        return target.getParentLogger();
    }

    @Override
    public <T> T unwrap( Class<T> iface ) throws SQLException
    {
        return target.unwrap( iface );
    }

    @Override
    public boolean isWrapperFor( Class<?> iface ) throws SQLException
    {
        return target.isWrapperFor( iface );
    }

    @NotNull
    private static synchronized DB createAndStartDatabase( @NotNull Path path, int port )
    {
        LOG.info( "Setting up embedded MariaDB instance on port {}, located at: {}", port, path );

        // prepare directories
        Path base = path.resolve( "base" );
        Path data = path.resolve( "data" );
        boolean unpackDatabase = !Files.exists( base );
        try
        {
            Files.createDirectories( base );
            Files.createDirectories( data );
        }
        catch( Exception e )
        {
            String msg = "failed setting up directories for embedded MariaDB instance at '" + path + "'";
            throw new IllegalStateException( msg, e );
        }

        // notify to log
        LOG.info( "Starting embedded MariaDB instance" );
        try
        {
            DBConfigurationBuilder configBuilder =
                    DBConfigurationBuilder.newBuilder()

                                          // set character set
                                          .addArg( "--character-set-server=utf8" )

                                          // ensure we use InnoDB, and use it FAST (dev-mode...)
                                          .addArg( "--default-storage-engine=InnoDB" )
                                          .addArg( "--innodb=FORCE" )

                                          // consistency, etc
                                          .addArg( "--sql-mode=" + SQL_MODES )
                                          .addArg( "--transaction-isolation=REPEATABLE-READ" )

                                          // location & port
                                          .setBaseDir( base.toString() )
                                          .setDataDir( data.toString() )
                                          .setPort( port );

            // if current user is root, add "--user=root" which is required by MariaDB
            if( Objects.equals( System.getProperty( "user.name" ), "root" ) )
            {
                configBuilder = configBuilder.addArg( "--user=root" );
            }

            // create the database using the configuration (either self-installing instance via the "newEmbeddedDB"
            // factory method, or by manually creating the DB instance, using NonInstallingDB class).
            DBConfiguration config = configBuilder.build();
            DB db = unpackDatabase ? DB.newEmbeddedDB( config ) : new NonInstallingDB( config );

            // start it up!
            db.start();
            return db;
        }
        catch( Exception e )
        {
            throw new IllegalStateException( "failed starting MariaDB instance", e );
        }
    }

    /**
     * The single purpose of this class is to expose the {@link DB#DB(DBConfiguration) protected constructor} of the
     * {@code DB} class. The only case where we want to directly create the {@code DB} instance ourselves, instead of
     * simply calling {@link DB#newEmbeddedDB(DBConfiguration)} is when we know the database is already unpacked, and we
     * want to skip the {@link DB#install() installation} process (which takes ~7 seconds).
     */
    private static class NonInstallingDB extends DB
    {
        public NonInstallingDB( DBConfiguration config ) throws ManagedProcessException
        {
            super( config );
            prepareDirectories();
        }
    }
}
