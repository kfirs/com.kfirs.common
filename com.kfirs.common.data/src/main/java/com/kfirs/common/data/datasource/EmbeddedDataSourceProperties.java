/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.data.datasource;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author arik
 * @on 6/24/17.
 */
@ConfigurationProperties( prefix = "kfirs.data.embedded" )
public class EmbeddedDataSourceProperties
{
    private boolean enabled = false;

    private Path workDir = Paths.get( "./work/mariadb" );

    private int port = 3306;

    private String schema;

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled( boolean enabled )
    {
        this.enabled = enabled;
    }

    public Path getWorkDir()
    {
        return workDir;
    }

    public void setWorkDir( Path workDir )
    {
        this.workDir = workDir;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort( int port )
    {
        this.port = port;
    }

    public String getSchema()
    {
        return schema;
    }

    public void setSchema( String schema )
    {
        this.schema = schema;
    }
}
