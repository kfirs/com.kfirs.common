/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.data.exception;

import com.kfirs.common.util.lang.NotNull;
import java.util.LinkedList;
import java.util.List;
import org.aspectj.lang.Aspects;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.dao.support.ChainedPersistenceExceptionTranslator;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static org.springframework.context.annotation.AdviceMode.ASPECTJ;

/**
 * @author arik
 * @on 1/30/17.
 */
@Configuration
@EnableTransactionManagement( mode = ASPECTJ )
public class TransactionConfig
{
    @Bean
    @Role( BeanDefinition.ROLE_INFRASTRUCTURE )
    public PersistenceExceptionTranslationAspect persistenceExceptionTranslationAspect(
            @SuppressWarnings( "SpringJavaAutowiringInspection" )
            @NotNull List<PersistenceExceptionTranslator> translators )
    {
        List<PersistenceExceptionTranslator> sortedTranslators = new LinkedList<>( translators );
        AnnotationAwareOrderComparator.sort( sortedTranslators );

        ChainedPersistenceExceptionTranslator translator = new ChainedPersistenceExceptionTranslator();
        sortedTranslators.forEach( translator::addDelegate );

        PersistenceExceptionTranslationAspect aspect = Aspects.aspectOf( PersistenceExceptionTranslationAspect.class );
        aspect.setPersistenceExceptionTranslator( translator );
        return aspect;
    }
}
