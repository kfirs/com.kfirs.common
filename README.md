# README #

## Overview ##

This project provides a common framework for the other projects in the
KFIRS project. Some utilities are simple stuff like path matches, file 
watching, classpath file-system, and some are more higher-level 
frameworks such as Node.js wrappers for Java, Gulp-like build framework,
etc.


## Modules ##

### com.kfirs.common.util ###

This module is very slim, providing mainly the `@NotNull` and `@Nullable`
annotations. These can be used for annotating methods, members and
parameters as such, allowing IDEs to provide hints and warning about
potential bugs.

#### Plans ####

* Add an AspectJ weaver that will enforce `@NotNull` checks on code in runtime.


### com.kfirs.common.test ###
 
This module is intended for use in JUnit test cases where you need a 
temporary work path per test. 


### com.kfirs.common.classpath ###
 
This module provides a Java 7 `FileSystem` over class loaders. This
enables using the Java 7 Path APIs on the class path!


### com.kfirs.common.io ###
 
This module provides various I/O related utilities such as path matchers,
file watchers, etc.


### com.kfirs.common.builder ###
 
This module provides a Gulp-like build system!


### com.kfirs.common.node ###
 
This module provides a NodeJS wrapper for use in Java! Allows for
automatic Node download, installation, and invocation of scripts.


### com.kfirs.common.web ###
 
Makes developing of Spring Boot client-side applications easier by auto-
extracting client-side code to the file-system, and (continuously) 
running build commands on them when they change (as well as on startup).

This complements the existing excellent web development features of 
Spring MVC.


## Changes ##

First consumable release is `1.0.3`.


## Development ##

To develop, you'll need Apache Maven version 3.3.x and above. To deploy
a new version, you'll also need to set up credentials to the relevant
repository in the [KFIRS Maven Repository](http://repo.kfirs.com/) -
specifically the `libs-release-local` and the `libs-snapshot-local`. See
Apache Maven's [settings.xml reference](https://maven.apache.org/settings.html#Servers).

You'll also need to add `external.atlassian.jgitflow` group ID to 
Apache Maven's plugin groups in `settings.xml`. Here's how (put this at
your `settings.xml` file):

    <pluginGroups>
        <pluginGroup>external.atlassian.jgitflow</pluginGroup>
    </pluginGroups>


### Branches ###

The main development is done on the `develop` branch, from which 
`release/X.Y.Z` are created. Once those branches are ready to be released
into the world, they are built, deployed to the [Artifactory Repository](http://repo.kfirs.com/), 
the branch is merged back into `master` and `develop`.

To facilitate this workflow, we use [JGitFlow](http://jgitflow.bitbucket.org/) 
for release management.

#### Creating a release branch ####

To create a release branch, checkout the `develop` branch, and then
execute this:

    mvn jgitflow:release-start
    
This will ask for the version being prepared (will go in the branch
name and in the POMs inside that branch), and it will also ask for the 
next development version (will go in the POMs inside the `develop`
branch).

It will then:

- create the new branch (`release/X.Y.Z`)
- update the POMs inside it to be the SNAPSHOT of the new version (X.Y.Z)
- update the POMs inside `develop` to the next development version
- switch your working copy to the new release branch

Now you can work on your release branch (usually merging any feature 
branches you've developed) and test it. Once it's ready, you can then
finish the release.

#### Finishing a release ####

To finish the release - aka. "release it" - checkout the relevant 
release branch (`release/X.Y.Z`) and then execute this:

    mvn jgitflow:release-finish
    
This will:

- remove the "SNAPSHOT" from the version
- deploy the version to the [Maven repository](http://repo.kfirs.com/)
- merge the branch back into `develop`
- merge the branch back into `master`
- delete the release branch

At this point, the `develop` and `master` branches are fully synced with 
the development in the relevant release branch. The POMs inside the
`develop` branch are still the SNAPSHOT of the next development version
(the one you gave when you created the release branch), and the POMs in
`master` are the released version of your release branch (eg. just 
`X.Y.Z`, without the "-SNAPSHOT" part).
