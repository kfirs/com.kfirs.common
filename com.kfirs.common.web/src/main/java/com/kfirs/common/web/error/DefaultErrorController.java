/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.error;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.rest.exception.RestError;
import com.kfirs.common.web.rest.exception.RestException;
import com.kfirs.common.web.rest.exception.RestExceptions;
import com.kfirs.common.web.trace.TraceManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.web.*;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletRequestAttributes;

import static com.kfirs.common.web.util.HttpServletRequestUtil.getUnderlyingRequest;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;
import static org.springframework.security.web.WebAttributes.ACCESS_DENIED_403;
import static org.springframework.security.web.WebAttributes.AUTHENTICATION_EXCEPTION;

/**
 * @author arik
 * @on 4/18/17.
 */
@Controller
@RequestMapping( "${server.error.path:${error.path:/error}}" )
public class DefaultErrorController extends AbstractErrorController
{
    @NotNull
    public static final String X_ERROR_HEADER_NAME = "x-error";

    private static final Logger LOG = getLogger( DefaultErrorController.class );

    @NotNull
    private final ErrorAttributes errorAttributes;

    @NotNull
    private final MessageSource messageSource;

    @NotNull
    private final ErrorProperties bootErrorProperties;

    @NotNull
    private final TraceManager traceManager;

    public DefaultErrorController( @NotNull ErrorAttributes errorAttributes,
                                   @NotNull List<ErrorViewResolver> errorViewResolvers,
                                   @NotNull MessageSource messageSource,
                                   @NotNull ServerProperties serverProperties,
                                   @NotNull TraceManager traceManager )
    {
        super( errorAttributes, errorViewResolvers );
        this.errorAttributes = errorAttributes;
        this.messageSource = messageSource;
        this.bootErrorProperties = serverProperties.getError();
        this.traceManager = traceManager;
    }

    @Override
    public String getErrorPath()
    {
        return this.bootErrorProperties.getPath();
    }

    /**
     * Renders an error page.
     *
     * @param errorPageRequest  request
     * @param response response
     */
    @RequestMapping( produces = "text/html" )
    public void errorHtml( @NotNull HttpServletRequest errorPageRequest, @NotNull HttpServletResponse response )
    {
        final HttpServletRequest request = getUnderlyingRequest( errorPageRequest );
        
        RestException restEx = getRestException( request );
        this.traceManager.addTraceValue( "exception", restEx );

        response.setStatus( restEx.getHttpStatus().value() );
        response.setHeader( HttpHeaders.CACHE_CONTROL, CacheControl.noStore().mustRevalidate().getHeaderValue() );
        response.setHeader( HttpHeaders.CONTENT_TYPE, TEXT_HTML_VALUE );

        // apply all errors from the REST exception as HTTP response headers
        Stream.concat( restEx.getBindingResult()
                             .getGlobalErrors()
                             .stream()
                             .map( error -> resolveError( error, request.getLocale() ) ),
                       restEx.getBindingResult()
                             .getFieldErrors()
                             .stream()
                             .map( error -> resolveError( error, request.getLocale() ) ) )
              .forEach( error -> {
                  String path = error.getPath();
                  String code = error.getCode();
                  String message = error.getMessage();
                  response.addHeader( X_ERROR_HEADER_NAME, path + ":" + code + ":" + message );
              } );

        // apply all HTTP headers from the REST exception onto the response
        restEx.getHttpHeaders()
              .entrySet()
              .stream()
              .flatMap( entry -> entry.getValue().stream().map( value -> Pair.of( entry.getKey(), value ) ) )
              .forEach( keyValuePair -> response.addHeader( keyValuePair.getKey(), keyValuePair.getValue() ) );

        // set up the response
        PrintWriter writer;
        try
        {
            writer = response.getWriter();
        }
        catch( IOException e )
        {
            LOG.warn( "Failed obtaining writer during 'text/html' error controller handling", e );
            return;
        }

        // HTML prefix
        writer.println( "<!DOCTYPE html>" );
        writer.println( "<html>" );
        writer.println( "  <head>" );
        writer.println( "    <title>Ooops!</title>" );
        writer.println( "  </head>" );
        writer.println( "  <body>" );
        writer.println( "    <h1>An internal error has occurred</h2>" );
        writer.println( "    <p>Unfortunately, that's all we can share at the moment.</p>" );
        writer.println( "  </body>" );
        writer.println( "</html>" );
        writer.flush();
    }

    /**
     * Obtains the current exception being handled by this error controller (through {@link ErrorAttributes}) and
     * applies it to the HTTP response.
     *
     * @param request  request
     * @param response response
     */
    @RequestMapping
    public void error( @NotNull HttpServletRequest request, @NotNull HttpServletResponse response )
    {
        RestException restEx = getRestException( request );
        this.traceManager.addTraceValue( "exception", restEx );
        response.setStatus( restEx.getHttpStatus().value() );

        // apply all errors from the REST exception as HTTP response headers
        Stream.concat( restEx.getBindingResult()
                             .getGlobalErrors()
                             .stream()
                             .map( error -> resolveError( error, request.getLocale() ) ),
                       restEx.getBindingResult()
                             .getFieldErrors()
                             .stream()
                             .map( error -> resolveError( error, request.getLocale() ) ) )
              .forEach( error -> {
                  String path = error.getPath();
                  String code = error.getCode();
                  String message = error.getMessage();
                  response.addHeader( X_ERROR_HEADER_NAME, path + ":" + code + ":" + message );
              } );

        // apply all HTTP headers from the REST exception onto the response
        restEx.getHttpHeaders()
              .entrySet()
              .stream()
              .flatMap( entry -> entry.getValue().stream().map( value -> Pair.of( entry.getKey(), value ) ) )
              .forEach( keyValuePair -> response.addHeader( keyValuePair.getKey(), keyValuePair.getValue() ) );
    }

    @NotNull
    private RestException getRestException( @NotNull HttpServletRequest request )
    {
        ServletRequestAttributes attributes = new ServletRequestAttributes( request );
        Throwable error =
                Optional.ofNullable( request.getAttribute( ACCESS_DENIED_403 ) )
                        .map( Throwable.class::cast )
                        .orElseGet( () -> Optional.ofNullable( request.getAttribute( AUTHENTICATION_EXCEPTION ) )
                                                  .map( Throwable.class::cast )
                                                  .orElseGet( () -> this.errorAttributes.getError( attributes ) ) );
        if( error instanceof AuthenticationException )
        {
            return RestExceptions.authenticationRequired( ( AuthenticationException ) error );
        }
        else if( error instanceof AccessDeniedException )
        {
            return RestExceptions.forbidden( error );
        }
        else if( error == null )
        {
            return RestExceptions.internalServerError();
        }
        else if( !( error instanceof RestException ) )
        {
            return RestExceptions.internalServerError( error );
        }
        else
        {
            return ( RestException ) error;
        }
    }

    @NotNull
    protected RestError resolveError( @NotNull ObjectError objectError, @NotNull Locale locale )
    {
        return resolveError( objectError.getObjectName(),
                             objectError.getCodes(),
                             objectError.getArguments(),
                             locale,
                             objectError.getCode(),
                             objectError.getDefaultMessage() );
    }

    @NotNull
    protected RestError resolveError( @NotNull FieldError objectError, @NotNull Locale locale )
    {
        String objectName = objectError.getObjectName();
        String field = objectError.getField();
        return resolveError( isBlank( objectName )
                             ? field
                             : isBlank( field ) ? objectName : objectName + "." + field,
                             objectError.getCodes(),
                             objectError.getArguments(),
                             locale,
                             objectError.getCode(),
                             objectError.getDefaultMessage() );
    }

    @NotNull
    public RestError resolveError( @NotNull String path,
                                   @NotNull String[] messageCodes,
                                   @Nullable Object[] args,
                                   @NotNull Locale locale,
                                   @NotNull String defaultCode,
                                   @Nullable String defaultMessage )
    {
        for( String code : messageCodes )
        {
            try
            {
                return new RestError( path, code, this.messageSource.getMessage( code, args, locale ) );
            }
            catch( NoSuchMessageException ignore )
            {
            }
        }
        return new RestError( path, defaultCode, Optional.ofNullable( defaultMessage ).orElse( "missing" ) );
    }
}
