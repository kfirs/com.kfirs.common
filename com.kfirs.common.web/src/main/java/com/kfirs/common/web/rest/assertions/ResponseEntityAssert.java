/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.assertions;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.requestid.RequestIdInterceptor;
import com.kfirs.common.web.trace.TraceInterceptor;
import java.net.URI;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.ObjectAssert;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import static com.kfirs.common.web.error.DefaultErrorController.X_ERROR_HEADER_NAME;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpHeaders.*;
import static org.springframework.http.HttpStatus.OK;

/**
 * @author arik
 * @on 2/19/17.
 */
public class ResponseEntityAssert<S extends ResponseEntityAssert<S, T>, T>
        extends AbstractAssert<S, ResponseEntity<T>>
{
    public ResponseEntityAssert( @Nullable ResponseEntity<T> actual )
    {
        super( actual, ResponseEntityAssert.class );
    }

    public ResponseEntityAssert( @Nullable ResponseEntity<T> actual, @NotNull Class<?> selfType )
    {
        super( actual, selfType );
    }

    @NotNull
    public S respondedWithStatusCode( @NotNull HttpStatus expectedStatusCode )
    {
        isNotNull();

        HttpStatus actualStatusCode = this.actual.getStatusCode();
        if( !Objects.equals( expectedStatusCode, actualStatusCode ) )
        {
            Function<HttpStatus, String> statusToString = status -> status.value() + "-" + status.getReasonPhrase();
            failWithMessage( "Expected response to return HTTP %s, but it returned HTTP %s",
                             statusToString.apply( expectedStatusCode ),
                             statusToString.apply( actualStatusCode ) );
        }

        return this.myself;
    }

    @NotNull
    public S respondedWithHeader( @NotNull String header )
    {
        isNotNull();

        HttpHeaders headers = this.actual.getHeaders();
        List<String> values = headers.get( header );
        if( values == null )
        {
            failWithMessage( "Expected response to contain header '%s', but it did not", header );
        }

        return this.myself;
    }

    @NotNull
    public S respondedWithoutHeader( @NotNull String header )
    {
        isNotNull();

        HttpHeaders headers = this.actual.getHeaders();
        List<String> values = headers.get( header );
        if( values != null )
        {
            failWithMessage( "Expected response NOT to contain header '%s', but it did. Received headers:\n%s",
                             header,
                             headers.entrySet()
                                    .stream()
                                    .map( e -> "    " + e.getKey() + ": " + e.getValue() )
                                    .collect( Collectors.joining( "\n" ) ) );
        }

        return this.myself;
    }

    @NotNull
    public S respondedWithHeader( @NotNull String header, @NotNull String value )
    {
        isNotNull();

        HttpHeaders headers = this.actual.getHeaders();
        List<String> values = headers.get( header );
        if( values == null )
        {
            failWithMessage( "Expected response to contain header '%s', but it did not. Received headers:\n%s",
                             header,
                             headers.entrySet()
                                    .stream()
                                    .map( e -> "    " + e.getKey() + ": " + e.getValue() )
                                    .collect( Collectors.joining( "\n" ) ) );
        }
        else if( !values.contains( value ) )
        {
            failWithMessage( "Expected response to contain header '%s' with value '%s', but it responded with: %s", header, value, values );
        }

        return this.myself;
    }

    @NotNull
    public S respondedWithAcceptHeader( @NotNull MediaType... mediaTypes )
    {
        isNotNull();

        HttpHeaders headers = this.actual.getHeaders();
        List<MediaType> actualMediaTypes = headers.getAccept();
        List<MediaType> expectedMediaTypes = Arrays.asList( mediaTypes );
        if( actualMediaTypes == null )
        {
            failWithMessage( "Expected response to contain header '%s', but it did not", ACCEPT );
        }
        else if( !actualMediaTypes.equals( expectedMediaTypes ) )
        {
            failWithMessage( "Expected response to contain header '%s' with values '%s', but it responded with: %s", ACCEPT, expectedMediaTypes, actualMediaTypes );
        }

        return this.myself;
    }

    @NotNull
    public S respondedWithoutAcceptHeader()
    {
        return respondedWithoutHeader( ACCEPT );
    }

    @NotNull
    public S respondedWithAllowHeader( @NotNull HttpMethod... methods )
    {
        isNotNull();

        HttpHeaders headers = this.actual.getHeaders();
        Set<HttpMethod> actualHttpMethods = headers.getAllow();
        Set<HttpMethod> expectedHttpMethods = new HashSet<>( Arrays.asList( methods ) );
        if( actualHttpMethods == null )
        {
            failWithMessage( "Expected response to contain header '%s', but it did not", ALLOW );
        }
        else if( !actualHttpMethods.equals( expectedHttpMethods ) )
        {
            failWithMessage( "Expected response to contain header '%s' with values '%s', but it responded with: %s", ALLOW, expectedHttpMethods, actualHttpMethods );
        }

        return this.myself;
    }

    @NotNull
    public S respondedWithoutAllowHeader()
    {
        return respondedWithoutHeader( ALLOW );
    }

    @NotNull
    public S respondedWithCacheControl( @NotNull CacheControl value )
    {
        return respondedWithHeader( CACHE_CONTROL, value.getHeaderValue() );
    }

    @NotNull
    public S respondedWithoutCacheControl()
    {
        return respondedWithoutHeader( CACHE_CONTROL );
    }

    @NotNull
    public S respondedWithETag()
    {
        return respondedWithHeader( ETAG );
    }

    @NotNull
    public S respondedWithETag( @NotNull String tag )
    {
        return respondedWithHeader( ETAG, "\"" + tag + "\"" );
    }

    @NotNull
    public S respondedWithoutETag()
    {
        return respondedWithoutHeader( ETAG );
    }

    @NotNull
    public S respondedWithLastModified()
    {
        return respondedWithHeader( LAST_MODIFIED );
    }

    @NotNull
    public S respondedWithLastModified( long value )
    {
        return respondedWithHeader( LAST_MODIFIED, value + "" );
    }

    @NotNull
    public S respondedWithoutLastModified()
    {
        return respondedWithoutHeader( LAST_MODIFIED );
    }

    @NotNull
    public S respondedWithRequestId()
    {
        return respondedWithHeader( RequestIdInterceptor.REQUEST_ID_HEADER );
    }

    @NotNull
    public S respondedWithoutRequestId()
    {
        return respondedWithoutHeader( RequestIdInterceptor.REQUEST_ID_HEADER );
    }

    @NotNull
    public S hasErrorHeaders()
    {
        isNotNull();

        if( Optional.ofNullable( actual.getHeaders().get( X_ERROR_HEADER_NAME ) ).orElseGet( Collections::emptyList ).isEmpty() )
        {
            failWithMessage( "Expected response to have errors, but it has no error headers" );
        }

        return this.myself;
    }

    @NotNull
    public S hasNoErrors()
    {
        if( this.actual == null )
        {
            // no response, no errors :)
            return this.myself;
        }

        List<String> errors = Optional.ofNullable( actual.getHeaders().get( X_ERROR_HEADER_NAME ) ).orElseGet( Collections::emptyList );
        if( !errors.isEmpty() )
        {
            failWithMessage( "Expected response to have no error headers, but it has %d errors", errors.size() );
        }

        return this.myself;
    }

    @NotNull
    public S hasNoMissingErrorPlaceholders()
    {
        ResponseEntity<T> actual = this.actual;
        if( actual == null )
        {
            // no response, no missing placeholders :)
            return this.myself;
        }

        Set<String> errorsWithUnresolvedPlaceholders = Optional.ofNullable( actual.getHeaders().get( X_ERROR_HEADER_NAME ) )
                                                               .orElseGet( Collections::emptyList )
                                                               .stream()
                                                               .filter( error -> error.matches( ".*\\{\\d+}.*" ) )
                                                               .collect( Collectors.toSet() );
        if( !errorsWithUnresolvedPlaceholders.isEmpty() )
        {
            String errors = errorsWithUnresolvedPlaceholders.stream().collect( joining( "\n" ) );
            failWithMessage( "Found errors with unresolved placeholders:\n" + errors );
        }

        return this.myself;
    }

    @NotNull
    public S hasErrorHeader( @NotNull String path, @NotNull String code )
    {
        hasNoMissingErrorPlaceholders();

        List<String> errors = actual.getHeaders().get( X_ERROR_HEADER_NAME );
        if( errors.stream().noneMatch( error -> error.startsWith( path + ":" + code + ":" ) ) )
        {
            failWithMessage( "Expected response to contain error:\n"
                             + "    <'%s':'%s'>\n"
                             + "But it does not. Found instead:\n"
                             + "%s",
                             path,
                             code,
                             errors.stream().collect( joining( "\n" ) ) );
            throw new IllegalStateException( "should not happen" );
        }
        return this.myself;
    }

    @NotNull
    public S hasErrorHeader( @NotNull String path, @NotNull String code, @NotNull String message )
    {
        hasNoMissingErrorPlaceholders();

        List<String> errors = Optional.ofNullable( actual.getHeaders().get( X_ERROR_HEADER_NAME ) ).orElseGet( Collections::emptyList );
        if( errors.stream().noneMatch( error -> error.startsWith( path + ":" + code + ":" + message ) ) )
        {
            if( errors.isEmpty() )
            {
                failWithMessage( "Expected response to contain error <'%s':'%s':'%s'>, but it does not. Found no errors at all.",
                                 path,
                                 code,
                                 message );
            }
            else
            {
                failWithMessage( "Expected response to contain error <'%s':'%s':'%s'>, but it does not. Found the following errors:\n%s",
                                 path,
                                 code,
                                 message,
                                 errors.stream().collect( joining( "\n" ) ) );
            }
        }
        return this.myself;
    }

    @NotNull
    public S hasBody()
    {
        isNotNull();

        if( this.actual.getBody() == null )
        {
            failWithMessage( "Expected response to have body, but it does not" );
        }

        return this.myself;
    }

    @NotNull
    public S hasNoBody()
    {
        isNotNull();

        T body = this.actual.getBody();
        if( body != null )
        {
            failWithMessage( "Expected response to have no body, but it does: %s", body );
        }

        return this.myself;
    }

    @NotNull
    public S hasBodyEqualTo( @NotNull T expectedBody )
    {
        isNotNull();

        T actualBody = this.actual.getBody();
        if( !Objects.equals( actualBody, expectedBody ) )
        {
            failWithMessage( "Expected response body to equal to:\n%s\nBut it was:\n%s", expectedBody, actualBody );
        }

        return this.myself;
    }

    public S hasBodyEqualToByFields( @NotNull T expectedBody, @NotNull String... fieldsOrProperties )
    {
        hasBody();

        T actualBody = this.actual.getBody();
        if( fieldsOrProperties.length > 0 )
        {
            assertThat( actualBody ).as( "Expected response body to equal to:\n    %s\nBut it was:\n    %s", expectedBody, actualBody )
                                    .isEqualToComparingOnlyGivenFields( expectedBody, fieldsOrProperties );
        }
        else
        {
            assertThat( actualBody ).as( "Expected response resource to equal to:\n    %s\nBut it was:\n    %s", expectedBody, actualBody )
                                    .isEqualToComparingFieldByField( expectedBody );
        }
        return this.myself;
    }

    @NotNull
    public ObjectAssert<T> assertOnBody()
    {
        return new ObjectAssert<>( this.actual.getBody() );
    }

    @NotNull
    public S hasTracePointer()
    {
        List<String> values = this.actual.getHeaders().get( TraceInterceptor.X_TRACE_LOCATION_HEADER );
        if( values == null || values.isEmpty() )
        {
            failWithMessage( "Expected response to contain trace header, but it did not." );
            return this.myself;
        }
        return this.myself;
    }

    @NotNull
    public S hasTraceEntry( @NotNull RestTemplate rest, @NotNull String traceKey, @NotNull String traceValue )
    {
        hasTracePointer();

        List<String> headerValues = this.actual.getHeaders().get( TraceInterceptor.X_TRACE_LOCATION_HEADER );
        String location = headerValues.get( 0 );

        RequestEntity<Void> requestEntity = RequestEntity.get( URI.create( location ) )
                                                         .accept( MediaType.APPLICATION_JSON_UTF8 )
                                                         .build();
        ResponseEntity<Map> responseEntity = rest.exchange( requestEntity, Map.class );
        if( responseEntity.getStatusCode() != OK )
        {
            failWithMessage( "Failed fetching trace information for response (received HTTP " + responseEntity.getStatusCode() + ")" );
            return this.myself;
        }

        Map trace = responseEntity.getBody();
        Map traceValues = ( Map ) trace.get( "values" );
        Object traceKeyValue = traceValues.get( traceKey );
        if( !Objects.equals( traceValue, traceKeyValue ) )
        {
            failWithMessage( "Could not find trace value with key '" + traceKey + "' and value '" + traceValue + "'. Following trace values found:\n" + traceValues );
            return this.myself;
        }

        return this.myself;
    }
}
