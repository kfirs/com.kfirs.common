/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.rest.exception.RestError;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;

/**
 * @author arik
 * @on 2/6/17.
 */
@SuppressWarnings( "DefaultAnnotationParam" )
@JsonIgnoreProperties( ignoreUnknown = false )
public class RestResponse<ResourceType>
{
    @NotNull
    @JsonInclude( NON_EMPTY )
    private final List<RestError> errors;

    @Nullable
    private final ResourceType resource;

    public RestResponse()
    {
        this.errors = emptyList();
        this.resource = null;
    }

    public RestResponse( @Nullable List<RestError> errors )
    {
        this.errors = unmodifiableList( Optional.ofNullable( errors ).map( LinkedList::new ).orElseGet( LinkedList::new ) );
        this.resource = null;
    }

    @JsonCreator
    public RestResponse( @JsonProperty( "errors" ) @Nullable List<RestError> errors,
                         @JsonProperty( "resource" ) @Nullable ResourceType resource )
    {
        this.errors = unmodifiableList( Optional.ofNullable( errors ).map( LinkedList::new ).orElseGet( LinkedList::new ) );
        this.resource = resource;
    }

    @NotNull
    public List<RestError> getErrors()
    {
        return errors;
    }

    @Nullable
    public ResourceType getResource()
    {
        return this.resource;
    }

    @Override
    public boolean equals( Object o )
    {
        if( this == o )
        {
            return true;
        }
        if( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        RestResponse that = ( RestResponse ) o;
        return Objects.equals( this.errors, that.errors ) &&
               Objects.equals( this.resource, that.resource );
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( this.errors, this.resource );
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder( this )
                .append( "errors", this.errors )
                .append( "resource", this.resource )
                .toString();
    }
}
