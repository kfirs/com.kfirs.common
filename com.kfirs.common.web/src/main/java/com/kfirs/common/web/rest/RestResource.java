/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest;

import com.kfirs.common.util.lang.NotNull;
import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static java.util.Collections.unmodifiableList;

/**
 * @author arik
 * @on 5/26/17.
 */
public final class RestResource
{
    @NotNull
    private final List<Object> resourceChain;

    public RestResource( @NotNull List<Object> resourceChain )
    {
        if( resourceChain.isEmpty() )
        {
            throw new IllegalArgumentException( "resource chain is empty" );
        }
        this.resourceChain = unmodifiableList( resourceChain );
    }

    @NotNull
    public List<Object> getResourceChain()
    {
        return this.resourceChain;
    }

    @NotNull
    public Object getRootResource()
    {
        return this.resourceChain.get( 0 );
    }

    @NotNull
    public Object getResource()
    {
        return this.resourceChain.get( this.resourceChain.size() - 1 );
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder( this )
                .append( "resourceChain", this.resourceChain )
                .toString();
    }
}
