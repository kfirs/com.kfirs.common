/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.interceptors.AutoHandlerInterceptor;
import com.kfirs.common.web.requestid.RequestIdInterceptor;
import com.kfirs.common.web.requestid.RequestIdManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import static com.kfirs.common.web.util.HttpServletRequestUtil.getUnderlyingRequest;
import static java.util.stream.Collectors.toMap;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Lowest order so we will be invoked AFTER the {@link RequestIdInterceptor}.
 *
 * @author arik
 * @on 2/13/17.
 */
@Order( Ordered.LOWEST_PRECEDENCE )
public class TraceInterceptor extends HandlerInterceptorAdapter implements AutoHandlerInterceptor
{
    private static final Logger LOG = getLogger( TraceInterceptor.class );

    @NotNull
    public static final String X_TRACE_LOCATION_HEADER = "X-Trace-Location";

    @NotNull
    private final ObjectWriter objectWriter;

    @NotNull
    private final TraceManager traceManager;

    @NotNull
    private final RequestIdManager requestIdManager;

    @NotNull
    private final TraceRepository traceRepository;

    private final boolean logTrace;

    public TraceInterceptor( @NotNull TraceProperties traceProperties,
                             @NotNull ObjectMapper objectMapper,
                             @NotNull TraceManager traceManager,
                             @NotNull RequestIdManager requestIdManager,
                             @NotNull TraceRepository traceRepository )
    {
        this.logTrace = traceProperties.isLogTrace();
        this.objectWriter = objectMapper.writer()
                                        .without( SerializationFeature.FAIL_ON_EMPTY_BEANS )
                                        .withDefaultPrettyPrinter();
        this.traceManager = traceManager;
        this.requestIdManager = requestIdManager;
        this.traceRepository = traceRepository;
    }

    @Override
    public boolean preHandle( @NotNull HttpServletRequest givenRequest,
                              @NotNull HttpServletResponse response,
                              @NotNull Object handler ) throws Exception
    {
        if( this.traceManager.isTraceRequest() )
        {
            HttpServletRequest request = getUnderlyingRequest( givenRequest );

            // log request information to trace
            Map<String, Collection<String>> headers =
                    Collections.list( request.getHeaderNames() )
                               .stream()
                               .collect( toMap( k -> k, k -> Collections.list( request.getHeaders( k ) ) ) );
            StringBuffer url = request.getRequestURL();
            String queryString = request.getQueryString();
            if( queryString != null )
            {
                url.append( "?" ).append( queryString );
            }
            this.traceManager.addTraceValue( "Request.method", request.getMethod() );
            this.traceManager.addTraceValue( "Request.url", url );
            this.traceManager.addTraceValue( "Request.protocol", request.getProtocol() );
            this.traceManager.addTraceValue( "Request.headers", headers );
            this.traceManager.addTraceValue( "Request.requestedSessionId", request.getRequestedSessionId() );
            this.traceManager.addTraceValue( "Request.hasSession", request.getSession( false ) != null );
            this.traceManager.addTraceValue( "Request.userPrincipal", request.getUserPrincipal() );
            this.traceManager.addTraceValue( "Request.remoteAddr", request.getRemoteAddr() );

            // store the request ID in SLF4J MDC
            String requestId = this.requestIdManager.getOrGenerateUuidForCurrentThread().toString();
            MDC.put( LogbackTraceAppender.MDC_TRACE_KEY, requestId );

            // build the trace URL and send it back to the client as a header in the response
            String scheme = request.getScheme();
            String host = request.getServerName();
            int port = request.getServerPort();
            response.setHeader( X_TRACE_LOCATION_HEADER, scheme + "://" + host + ":" + port + "/trace/" + requestId );
        }
        return true;
    }

    @Override
    public void afterCompletion( @NotNull HttpServletRequest origRequest,
                                 @NotNull HttpServletResponse response,
                                 @NotNull Object handler,
                                 @Nullable Exception ex ) throws Exception
    {
        if( this.traceManager.isTraceRequest() )
        {
            @SuppressWarnings( "unused" )
            HttpServletRequest request = getUnderlyingRequest( origRequest );
            try
            {
                // log response information to trace
                this.traceManager.addTraceValue( "Response.status", HttpStatus.valueOf( response.getStatus() ) );
                this.traceManager.addTraceValue( "Response.headers", response.getHeaderNames().stream().collect( toMap( k -> k, response::getHeaders ) ) );

                // add the full trace information to the trace repository, so it will be available to the trace controller
                String rid = this.requestIdManager.getOrGenerateUuidForCurrentThread().toString();
                TraceRepository.Trace trace = this.traceRepository.add( rid, this.traceManager.getTraceValues(), this.traceManager.getTraceMessages() );

                // if we are configured to also log trace information, do it now
                if( this.logTrace )
                {
                    logTraceInformation( trace );
                }
            }
            finally
            {
                MDC.remove( LogbackTraceAppender.MDC_TRACE_KEY );
            }
        }
    }

    private void logTraceInformation( @NotNull TraceRepository.Trace trace )
    {
        try
        {
            StringBuilder buffer = new StringBuilder( 1024 * 8 );

            buffer.append( "TRACE INFORMATION FOR: " ).append( trace.getKey() ).append( "\n" );
            buffer.append( "============================================================\n" );

            buffer.append( "\nTRACE VALUES:\n" );
            buffer.append( "---------------\n" );
            if( trace.getValues().isEmpty() )
            {
                buffer.append( "None.\n" );
            }
            else
            {
                trace.getValues()
                     .entrySet()
                     .stream()
                     .map( e -> {
                         Object value = e.getValue();
                         try
                         {
                             if( value instanceof Throwable )
                             {
                                 return e.getKey() + ":\n" + ExceptionUtils.getStackTrace( ( Throwable ) value ) + "\n";
                             }
                             else
                             {
                                 return e.getKey() + ": " + this.objectWriter.writeValueAsString( value ) + "\n";
                             }
                         }
                         catch( JsonProcessingException e1 )
                         {
                             return e.getKey() + ": " + value + "\n";
                         }
                     } )
                     .forEach( buffer::append );
            }

            buffer.append( "\nTRACE MESSAGES:\n" );
            buffer.append( "-----------------\n" );
            if( trace.getMessages().isEmpty() )
            {
                buffer.append( "None.\n" );
            }
            else
            {
                trace.getMessages().stream().map( msg -> msg + "\n" ).forEach( buffer::append );
            }

            LOG.info( "\n\n{}\n", buffer );
        }
        catch( Exception e )
        {
            LOG.warn( "Failed logging trace information", e );
        }
    }
}
