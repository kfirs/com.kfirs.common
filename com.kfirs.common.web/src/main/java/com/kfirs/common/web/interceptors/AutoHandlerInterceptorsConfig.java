/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.interceptors;

import com.kfirs.common.util.lang.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author arik
 * @on 3/26/17.
 */
@Configuration
@AutoConfigureBefore( ErrorMvcAutoConfiguration.class ) 
public class AutoHandlerInterceptorsConfig extends WebMvcConfigurerAdapter
{
    @NotNull
    private final List<AutoHandlerInterceptor> autoHandlerInterceptors;

    public AutoHandlerInterceptorsConfig( @NotNull Optional<List<AutoHandlerInterceptor>> autoHandlerInterceptors )
    {
        this.autoHandlerInterceptors = autoHandlerInterceptors.orElseGet( Collections::emptyList );
    }

    @Override
    public void addInterceptors( InterceptorRegistry registry )
    {
        this.autoHandlerInterceptors.forEach( registry::addInterceptor );
    }
}
