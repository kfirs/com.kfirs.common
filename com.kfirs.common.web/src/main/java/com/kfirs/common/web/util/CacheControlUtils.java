/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.util;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.http.CacheControl;

import static java.lang.Long.parseLong;
import static java.util.regex.Pattern.CASE_INSENSITIVE;

/**
 * @author arik
 * @on 5/26/17.
 * @todo incorporate this into standard webapp caching policy 
 */
public final class CacheControlUtils
{
    @NotNull
    private static final Pattern CACHE_PERIOD_PATTERN =
            Pattern.compile( "(?<duration>\\d+)(?<timeunit>nanoseconds|microseconds|milliseconds|seconds|minutes|hours|days)", CASE_INSENSITIVE );

    @Nullable
    public static CacheControl parseCacheControl( @Nullable String value )
    {
        if( value == null )
        {
            return null;
        }
        value = value.trim();

        if( value.isEmpty() )
        {
            throw new IllegalArgumentException( "invalid cache spec '" + value + "'" );
        }
        else if( value.equalsIgnoreCase( "no-store" ) )
        {
            return CacheControl.noStore();
        }
        else if( value.equalsIgnoreCase( "no-cache" ) )
        {
            return CacheControl.noCache().cachePrivate().mustRevalidate();
        }
        else
        {
            Matcher cacheMatcher = CACHE_PERIOD_PATTERN.matcher( value );
            if( !cacheMatcher.matches() )
            {
                throw new IllegalArgumentException( "invalid cache spec '" + value + "'" );
            }
            else
            {
                return CacheControl.maxAge( parseLong( cacheMatcher.group( "duration" ) ),
                                            TimeUnit.valueOf( cacheMatcher.group( "timeunit" ).toUpperCase() ) );
            }
        }
    }

    private CacheControlUtils()
    {
    }
}
