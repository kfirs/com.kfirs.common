/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.handler;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.rest.RestResource;
import com.kfirs.common.web.rest.RestResourceExecutor;
import com.kfirs.common.web.rest.RestResourceLocator;
import com.kfirs.common.web.rest.RestWrapper;
import java.util.Optional;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author arik
 * @on 5/26/17.
 */
public class SimpleRestWrapper implements RestWrapper
{
    @NotNull
    private final Object rootResource;

    public SimpleRestWrapper( @NotNull Object rootResource )
    {
        this.rootResource = rootResource;
    }

    @NotNull
    @Override
    public Optional<Object> resolveAndExecute( @NotNull RestResourceLocator resourceSupplier,
                                               @NotNull RestResourceExecutor resourceExecutor ) throws Exception
    {
        Optional<RestResource> resource = resourceSupplier.resolve();
        if( resource.isPresent() )
        {
            return Optional.ofNullable( resourceExecutor.execute( resource.get() ) );
        }
        else
        {
            return Optional.empty();
        }
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder( this )
                .append( "rootResource", rootResource )
                .toString();
    }
}
