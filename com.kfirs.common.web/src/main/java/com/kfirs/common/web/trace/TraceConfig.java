/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.requestid.RequestIdManager;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author arik
 * @on 5/11/17.
 */
@Configuration
public class TraceConfig
{
    @Bean
    public TraceLogbackConnector traceLogbackConnector()
    {
        return new TraceLogbackConnector();
    }

    @Bean
    public TraceRepository traceRepository()
    {
        return new TraceRepository();
    }

    @Bean
    public TraceProperties traceProperties()
    {
        return new TraceProperties();
    }

    @Bean
    public TraceManager traceManager( @NotNull TraceProperties traceProperties,
                                      @NotNull ObjectProvider<HttpServletRequest> requestProvider )
    {
        return new TraceManager( traceProperties, requestProvider );
    }

    @Bean
    public TraceController traceController( @NotNull ObjectMapper objectMapper,
                                            @NotNull TraceRepository traceRepository )
    {
        return new TraceController( objectMapper, traceRepository );
    }

    @Bean
    public TraceInterceptor traceInterceptor( @NotNull ObjectMapper objectMapper,
                                              @NotNull TraceProperties traceProperties,
                                              @NotNull TraceManager traceManager,
                                              @NotNull RequestIdManager requestIdManager,
                                              @NotNull TraceRepository traceRepository )
    {
        return new TraceInterceptor( traceProperties, objectMapper, traceManager, requestIdManager, traceRepository );
    }
}
