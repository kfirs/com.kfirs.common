/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.exception;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kfirs.common.util.lang.NotNull;
import java.util.Objects;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author arik
 * @on 2/7/17.
 */
public class RestError
{
    @NotNull
    @JsonInclude( JsonInclude.Include.NON_EMPTY )
    private final String path;

    @NotNull
    private final String code;

    @NotNull
    private final String message;

    @JsonCreator
    public RestError( @JsonProperty( "path" ) @NotNull String path,
                      @JsonProperty( "code" ) @NotNull String code,
                      @JsonProperty( "message" ) @NotNull String message )
    {
        this.path = path;
        this.code = code;
        this.message = message;
    }

    @NotNull
    public String getPath()
    {
        return path;
    }

    @NotNull
    public String getCode()
    {
        return code;
    }

    @NotNull
    public String getMessage()
    {
        return message;
    }

    public boolean matches( @NotNull String path, @NotNull String code )
    {
        return Objects.equals( this.path, path ) && Objects.equals( this.code, code );
    }

    public boolean matches( @NotNull String path, @NotNull String code, @NotNull String message )
    {
        return matches( path, code ) && Objects.equals( this.message, message );
    }

    @Override
    public boolean equals( Object o )
    {
        if( this == o )
        {
            return true;
        }
        if( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        RestError error = ( RestError ) o;
        return Objects.equals( path, error.path ) &&
               Objects.equals( code, error.code ) &&
               Objects.equals( message, error.message );
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( path, code, message );
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder( this )
                .append( "path", path )
                .append( "code", code )
                .append( "message", message )
                .toString();
    }
}
