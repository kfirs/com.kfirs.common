/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.handler;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.rest.annotation.PathToken;
import org.springframework.core.MethodParameter;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

/**
 * @author arik
 * @on 5/26/17.
 */
public class PathTokenArgumentResolver implements HandlerMethodArgumentResolver
{
    @NotNull
    private final ConversionService conversionService;

    public PathTokenArgumentResolver( @NotNull ConversionService conversionService )
    {
        this.conversionService = conversionService;
    }

    @Override
    public boolean supportsParameter( @NotNull MethodParameter parameter )
    {
        return parameter.hasParameterAnnotation( PathToken.class );
    }

    @Override
    public Object resolveArgument( @NotNull MethodParameter parameter,
                                   @NotNull ModelAndViewContainer mavContainer,
                                   @NotNull NativeWebRequest webRequest,
                                   @NotNull WebDataBinderFactory binderFactory ) throws Exception
    {
        Object pathToken = webRequest.getAttribute( RestHandler.PATH_TOKEN_ATTRIBUTE_NAME, SCOPE_REQUEST );
        if( pathToken == null )
        {
            throw new IllegalStateException( "path token could not be found!" );
        }
        else
        {
            return this.conversionService.convert( pathToken,
                                                   TypeDescriptor.forObject( pathToken ),
                                                   new TypeDescriptor( parameter ) );
        }
    }
}
