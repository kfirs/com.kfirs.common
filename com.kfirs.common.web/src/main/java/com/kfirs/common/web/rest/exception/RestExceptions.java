/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.kfirs.common.util.lang.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.security.web.csrf.CsrfException;

import static java.util.Arrays.asList;
import static org.springframework.http.HttpHeaders.*;
import static org.springframework.http.HttpStatus.*;

/**
 * @author arik
 * @on 3/3/17.
 */
public class RestExceptions
{
    /* -------------------------------------------------------------------------------------------------------------- */
    /*  HTTP 4xx                                                                                                      */
    /* -------------------------------------------------------------------------------------------------------------- */

    @NotNull
    public static RestException badRequest()
    {
        return new RestException( BAD_REQUEST );
    }

    @NotNull
    public static RestException badRequest( @NotNull Throwable cause )
    {
        return new RestException( cause, BAD_REQUEST );
    }

    @NotNull
    public static RestException conflict()
    {
        return new RestException( CONFLICT );
    }

    @NotNull
    public static RestException conflict( @NotNull Throwable cause )
    {
        return new RestException( cause, CONFLICT );
    }

    @NotNull
    public static RestException dataIntegrityError()
    {
        return conflict().withObjectError( "DataIntegrityError" );
    }

    @NotNull
    public static RestException dataIntegrityError( @NotNull Throwable cause )
    {
        return conflict( cause ).withObjectError( "DataIntegrityError" );
    }

    @NotNull
    public static RestException preconditionFailed( @NotNull String headerName )
    {
        return new RestException( PRECONDITION_FAILED ).withFieldError( "headers." + headerName,
                                                                        "PreconditionFailed",
                                                                        headerName );
    }

    @NotNull
    public static RestException preconditionFailed( @NotNull Throwable cause, @NotNull String headerName )
    {
        return new RestException( cause, PRECONDITION_FAILED ).withFieldError( "headers." + headerName,
                                                                               "PreconditionFailed",
                                                                               headerName );
    }

    @NotNull
    public static RestException forbidden()
    {
        return new RestException( FORBIDDEN ).withObjectError( "PermissionDenied" );
    }

    @NotNull
    public static RestException forbidden( @NotNull Throwable cause )
    {
        if( cause instanceof AuthorizationServiceException )
        {
            return new RestException( cause, FORBIDDEN ).withObjectError( "AuthorizationFailed" );
        }
        else if( cause instanceof CsrfException )
        {
            return new RestException( cause, FORBIDDEN ).withObjectError( "CsrfValidationFailed" );
        }
        else
        {
            return new RestException( cause, FORBIDDEN ).withObjectError( "PermissionDenied" );
        }
    }

    @NotNull
    public static RestException authenticationRequired()
    {
        return new RestException( UNAUTHORIZED ).withObjectError( "Unauthorized" );
    }

    @NotNull
    public static RestException authenticationRequired( @NotNull AuthenticationException cause )
    {
        if( cause instanceof AccountExpiredException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "AccountExpired" );
        }
        else if( cause instanceof CredentialsExpiredException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "AccountCredentialsExpired" );
        }
        else if( cause instanceof DisabledException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "AccountDisabled" );
        }
        else if( cause instanceof LockedException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "AccountLocked" );
        }
        else if( cause instanceof AuthenticationServiceException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "AuthenticationFailed" );
        }
        else if( cause instanceof BadCredentialsException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "BadCredentials" );
        }
        else if( cause instanceof InsufficientAuthenticationException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "InsufficientAuthentication" );
        }
        else if( cause instanceof ProviderNotFoundException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "BadCredentials" );
        }
        else if( cause instanceof CookieTheftException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "RememberMeAuthenticationFailed" );
        }
        else if( cause instanceof InvalidCookieException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "RememberMeAuthenticationFailed" );
        }
        else if( cause instanceof RememberMeAuthenticationException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "RememberMeAuthenticationFailed" );
        }
        else if( cause instanceof SessionAuthenticationException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "TooManySessions" );
        }
        else if( cause instanceof UsernameNotFoundException )
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "UnknownAccount" );
        }
        else
        {
            return new RestException( cause, UNAUTHORIZED ).withObjectError( "Unauthorized" );
        }
    }

    @NotNull
    public static RestException idUpdateNotAllowed()
    {
        return badRequest().withObjectError( "IdUpdateNotAllowed" );
    }

    @NotNull
    public static RestException illegalValue()
    {
        return illegalValue( "" );
    }

    @NotNull
    public static RestException illegalValue( @NotNull String field )
    {
        return badRequest().withFieldError( field.isEmpty() ? "values" : "values." + field, "IllegalValue" );
    }

    @NotNull
    public static RestException illegalValue( @NotNull Throwable cause )
    {
        return illegalValue( cause, "" );
    }

    @NotNull
    public static RestException illegalValue( @NotNull Throwable cause, @NotNull String field )
    {
        return badRequest( cause ).withFieldError( field.isEmpty() ? "values" : "values." + field, "IllegalValue" );
    }

    @NotNull
    public static RestException illegalJsonValue( @NotNull JsonProcessingException cause )
    {
        return illegalJsonValue( cause, "" );
    }

    @NotNull
    public static RestException illegalJsonValue( @NotNull JsonProcessingException cause, @NotNull String pathPrefix )
    {
        if( cause instanceof JsonMappingException )
        {
            JsonMappingException mappingException = ( JsonMappingException ) cause;
            List<JsonMappingException.Reference> path = mappingException.getPath();
            if( path.isEmpty() )
            {
                return illegalValue( cause, pathPrefix );
            }
            else if( pathPrefix.isEmpty() )
            {
                return illegalValue( cause, path.get( path.size() - 1 ).getFieldName() );
            }
            else
            {
                return illegalValue( cause, pathPrefix + "." + path.get( path.size() - 1 ).getFieldName() );
            }
        }
        else
        {
            return requestContentUnreadable( cause );
        }
    }

    @NotNull
    public static RestException methodNotAllowed( @NotNull HttpMethod... allowedHttpMethods )
    {
        return methodNotAllowed( new HashSet<>( asList( allowedHttpMethods ) ) );
    }

    @NotNull
    public static RestException methodNotAllowed( @NotNull Set<HttpMethod> allowedHttpMethods )
    {
        RestException e = new RestException( METHOD_NOT_ALLOWED ).withFieldError( "method",
                                                                                  "MethodNotAllowed",
                                                                                  allowedHttpMethods );
        e.getHttpHeaders().setAllow( allowedHttpMethods );
        return e;
    }

    @NotNull
    public static RestException methodNotAllowed( @NotNull Throwable cause, @NotNull HttpMethod... allowedHttpMethods )
    {
        return methodNotAllowed( cause, new HashSet<>( asList( allowedHttpMethods ) ) );
    }

    @NotNull
    public static RestException methodNotAllowed( @NotNull Throwable cause,
                                                  @NotNull Set<HttpMethod> allowedHttpMethods )
    {
        RestException e =
                new RestException( cause, METHOD_NOT_ALLOWED ).withFieldError( "method",
                                                                               "MethodNotAllowed",
                                                                               allowedHttpMethods );
        e.getHttpHeaders().setAllow( allowedHttpMethods );
        return e;
    }

    @NotNull
    public static RestException notAcceptable()
    {
        return new RestException( NOT_ACCEPTABLE ).withFieldError( "headers.Accept", "UnacceptableMediaType" );
    }

    @NotNull
    public static RestException notAcceptable( @NotNull Throwable cause )
    {
        return new RestException( cause, NOT_ACCEPTABLE ).withFieldError( "headers.Accept", "UnacceptableMediaType" );
    }

    @NotNull
    public static RestException notFound()
    {
        return notFoundWithoutErrors().withObjectError( "ResourceNotFound" );
    }

    @NotNull
    public static RestException notFoundWithoutErrors()
    {
        return new RestException( NOT_FOUND );
    }

    @NotNull
    public static RestException notFound( @NotNull Throwable cause )
    {
        return new RestException( cause, NOT_FOUND ).withObjectError( "ResourceNotFound" );
    }

    @NotNull
    public static RestException requestContentUnreadable()
    {
        return badRequest().withFieldError( "body", "ContentUnreadable" );
    }

    @NotNull
    public static RestException requestContentUnreadable( @NotNull Throwable cause )
    {
        return badRequest( cause ).withFieldError( "body", "ContentUnreadable" );
    }

    @NotNull
    public static RestException ifMatchHeaderMissing()
    {
        return new RestException( PRECONDITION_REQUIRED ).withFieldError( "headers." + IF_MATCH, "RequestHeaderMissing", IF_MATCH );
    }

    @NotNull
    public static RestException requestParameterMissing( @NotNull String parameterName )
    {
        return badRequest().withFieldError( "values." + parameterName, "RequestParameterMissing", parameterName );
    }

    @NotNull
    public static RestException requestParameterMissing( @NotNull Throwable cause, @NotNull String parameterName )
    {
        return badRequest( cause ).withFieldError( "values." + parameterName, "RequestParameterMissing", parameterName );
    }

    @NotNull
    public static RestException requestPartMissing( @NotNull String partName )
    {
        return badRequest().withFieldError( "multipart." + partName, "RequestPartMissing", partName );
    }

    @NotNull
    public static RestException requestPartMissing( @NotNull Throwable cause, @NotNull String partName )
    {
        return badRequest( cause ).withFieldError( "multipart." + partName, "RequestPartMissing", partName );
    }

    @NotNull
    public static RestException resourceIsStale()
    {
        return conflict().withObjectError( "ResourceIsStale" );
    }

    @NotNull
    public static RestException resourceIsStale( @NotNull Throwable cause )
    {
        return conflict( cause ).withObjectError( "ResourceIsStale" );
    }

    @NotNull
    public static RestException sortingNotAllowed()
    {
        return badRequest().withFieldError( "values._sort", "SortNotAllowed" );
    }

    @NotNull
    public static RestException ifNoneMatchNotSupported()
    {
        return unsupportedHeader( IF_NONE_MATCH );
    }

    @NotNull
    public static RestException ifModifiedSinceNotSupported()
    {
        return unsupportedHeader( IF_MODIFIED_SINCE );
    }

    @NotNull
    public static RestException ifUnmodifiedSinceNotSupported()
    {
        return unsupportedHeader( IF_UNMODIFIED_SINCE );
    }

    @NotNull
    public static RestException unsupportedHeader( @NotNull String headerName )
    {
        return badRequest().withFieldError( "headers." + headerName, "UnsupportedHeader", headerName );
    }

    @NotNull
    public static RestException unsupportedMediaType( @NotNull String contentType,
                                                      @NotNull MediaType... acceptedMediaTypes )
    {
        return unsupportedMediaType( contentType, asList( acceptedMediaTypes ) );
    }

    @NotNull
    public static RestException unsupportedMediaType( @NotNull String contentType,
                                                      @NotNull List<MediaType> acceptedMediaTypes )
    {
        RestException e = new RestException( UNSUPPORTED_MEDIA_TYPE ).withFieldError( "headers.Content-Type",
                                                                                      "UnsupportedMediaType",
                                                                                      contentType );
        if( !acceptedMediaTypes.isEmpty() )
        {
            e.getHttpHeaders().setAccept( acceptedMediaTypes );
        }
        return e;
    }

    @NotNull
    public static RestException unsupportedMediaType( @NotNull String contentType,
                                                      @NotNull Throwable cause,
                                                      @NotNull MediaType... acceptedMediaTypes )
    {
        return unsupportedMediaType( contentType, cause, asList( acceptedMediaTypes ) );
    }

    @NotNull
    public static RestException unsupportedMediaType( @NotNull String contentType,
                                                      @NotNull Throwable cause,
                                                      @NotNull List<MediaType> acceptedMediaTypes )
    {
        RestException e = new RestException( cause, UNSUPPORTED_MEDIA_TYPE ).withFieldError( "headers.Content-Type",
                                                                                             "UnsupportedMediaType",
                                                                                             contentType );
        if( !acceptedMediaTypes.isEmpty() )
        {
            e.getHttpHeaders().setAccept( acceptedMediaTypes );
        }
        return e;
    }

    @NotNull
    public static RestException versionUpdateNotAllowed()
    {
        return badRequest().withObjectError( "VersionUpdateNotAllowed" );
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    /*  HTTP 5xx                                                                                                      */
    /* -------------------------------------------------------------------------------------------------------------- */

    @NotNull
    public static RestException internalServerError()
    {
        return new RestException( INTERNAL_SERVER_ERROR ).withObjectError( "InternalServerError" );
    }

    @NotNull
    public static RestException internalServerError( @NotNull Throwable cause )
    {
        return new RestException( cause, INTERNAL_SERVER_ERROR ).withObjectError( "InternalServerError" );
    }

    @NotNull
    public static RestException notImplemented()
    {
        return new RestException( NOT_IMPLEMENTED );
    }

    @NotNull
    public static RestException notImplemented( @NotNull Throwable cause )
    {
        return new RestException( cause, NOT_IMPLEMENTED );
    }

    @NotNull
    public static RestException notAudited()
    {
        return notImplemented().withObjectError( "NotAudited" );
    }

    @NotNull
    public static RestException notAudited( @NotNull Throwable cause )
    {
        return notImplemented( cause ).withObjectError( "NotAudited" );
    }

    @NotNull
    public static RestException notIdentifiable()
    {
        return notImplemented().withObjectError( "NotIdentifiable" );
    }

    @NotNull
    public static RestException notVersioned()
    {
        return notImplemented().withObjectError( "NotVersioned" );
    }

    @NotNull
    public static RestException serviceUnavailable()
    {
        return new RestException( SERVICE_UNAVAILABLE ).withObjectError( "ServiceUnavailable" );
    }

    @NotNull
    public static RestException serviceUnavailable( @NotNull Throwable cause )
    {
        return new RestException( cause, SERVICE_UNAVAILABLE ).withObjectError( "ServiceUnavailable" );
    }
}
