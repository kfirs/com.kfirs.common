/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.requestid;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.util.UUID;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import static com.kfirs.common.web.requestid.RequestIdInterceptor.REQUEST_ID_HEADER;

/**
 * @author arik
 * @on 3/31/17.
 */
public class RequestIdPropagatingClientHttpInterceptor implements ClientHttpRequestInterceptor
{
    @NotNull
    private final RequestIdManager requestIdManager;

    public RequestIdPropagatingClientHttpInterceptor( @NotNull RequestIdManager requestIdManager )
    {
        this.requestIdManager = requestIdManager;
    }

    @Override
    public ClientHttpResponse intercept( @NotNull HttpRequest request,
                                         byte[] body,
                                         @NotNull ClientHttpRequestExecution execution )
            throws IOException
    {
        UUID currentId = this.requestIdManager.getOrGenerateUuidForCurrentThread();
        request.getHeaders().set( REQUEST_ID_HEADER, currentId.toString() );
        return execution.execute( request, body );
    }
}
