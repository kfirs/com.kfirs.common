/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.security;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * @author arik
 * @on 6/25/17.
 */
public class AjaxAwareBasicAuthenticationEntryPoint implements AuthenticationEntryPoint
{
    @NotNull
    private final String realmName;

    public AjaxAwareBasicAuthenticationEntryPoint( @NotNull String realmName )
    {
        this.realmName = realmName;
    }

    @Override
    public void commence( HttpServletRequest request,
                          HttpServletResponse response,
                          AuthenticationException authException ) throws IOException, ServletException
    {
        // only send the "WWW-Authenticate" header back to the client if this is NOT an AJAX request
        // AJAX request send the "X-Requested-With" header with the "XMLHttpRequest" value; in those cases, we
        // will ONLY set the HTTP status code to 401, but without the "WWW-Authenticate" header, so the browser
        // will not show an authentication popup.
        String requestWith = request.getHeader( "x-requested-with" );
        if( !"XMLHttpRequest".equalsIgnoreCase( requestWith ) )
        {
            response.addHeader( "WWW-Authenticate", "Basic realm=\"" + this.realmName + "\"" );
        }
        response.sendError( HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage() );
    }
}
