/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web;

import com.kfirs.common.web.error.DefaultErrorController;
import com.kfirs.common.web.error.ErrorConfig;
import com.kfirs.common.web.interceptors.AutoHandlerInterceptorsConfig;
import com.kfirs.common.web.requestid.RequestIdConfig;
import com.kfirs.common.web.rest.RestConfig;
import com.kfirs.common.web.server.ServerConfig;
import com.kfirs.common.web.trace.TraceConfig;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configures infrastructure web components. <p>Must be invoked <b>before</b> {@link ErrorMvcAutoConfiguration} because
 * we want it NOT to define the default {@link org.springframework.boot.autoconfigure.web.ErrorController
 * ErrorController}, and that will only happen if it will see our {@link DefaultErrorController ErrorController}.</p>
 *
 * @author arik
 * @on 2/11/17.
 */
@Configuration
@Import( {
                 AutoHandlerInterceptorsConfig.class,
                 RequestIdConfig.class,
                 ServerConfig.class,
                 RestConfig.class,
                 TraceConfig.class,
                 ErrorConfig.class
         } )
@AutoConfigureBefore( ErrorMvcAutoConfiguration.class )
public class WebConfig
{
}
