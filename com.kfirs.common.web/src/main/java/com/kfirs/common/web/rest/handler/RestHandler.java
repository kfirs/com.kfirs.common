/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.handler;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.rest.RestProperties;
import com.kfirs.common.web.rest.RestWrapper;
import com.kfirs.common.web.rest.annotation.RestRoot;
import com.kfirs.common.web.rest.exception.RestExceptions;
import com.kfirs.common.web.trace.TraceManager;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.core.convert.ConversionService;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.expression.spel.support.StandardTypeConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.DefaultSessionAttributeStore;
import org.springframework.web.bind.support.SessionAttributeStore;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.annotation.ModelFactory;
import org.springframework.web.method.annotation.SessionAttributesHandler;
import org.springframework.web.method.support.HandlerMethodArgumentResolverComposite;
import org.springframework.web.method.support.InvocableHandlerMethod;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestDataBinderFactory;
import org.springframework.web.servlet.support.RequestContextUtils;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * @author arik
 * @on 5/11/17.
 */
@RestController
public class RestHandler
{
    @NotNull
    static final String PATH_TOKEN_ATTRIBUTE_NAME = RestHandler.class.getName() + "#pathToken";

    @NotNull
    private final ApplicationContext applicationContext;

    @NotNull
    private final ConversionService conversionService;

    @NotNull
    private final HandlerMethodArgumentResolverComposite argumentResolvers;

    @NotNull
    private final WebBindingInitializer webBindingInitializer;

    @NotNull
    private final SessionAttributeStore sessionAttributeStore = new DefaultSessionAttributeStore();

    @NotNull
    private final Map<Class<?>, SessionAttributesHandler> sessionAttributesHandlerCache = new ConcurrentHashMap<>( 64 );

    @NotNull
    private final ParameterNameDiscoverer parameterNameDiscoverer = new DefaultParameterNameDiscoverer();

    @NotNull
    private final List<Object> rootResources;

    @NotNull
    private final TraceManager traceManager;

    public RestHandler( @NotNull ApplicationContext applicationContext,
                        @NotNull ConversionService conversionService,
                        @NotNull RequestMappingHandlerAdapter requestMappingHandlerAdapter,
                        @NotNull RestProperties restProperties,
                        @NotNull TraceManager traceManager )
    {
        this.applicationContext = applicationContext;

        //////////////////////////////////////////////////////////////////////////////////////////
        // WE MIMIC THE BEHAVIOR OF SPRING'S DEFAULT RequestMappingHandlerAdapter CREATION HERE
        // ---------------------------------------------------------------------------------------
        // In case of Spring upgrades, please ensure the process is still correctling copying
        // Spring's flow, or alternatively, if Spring decide to provide an API for this, use
        // that instead.
        //
        // SEE: RequestMappingHandlerAdapter() (the default constructor)
        // SEE: WebMvcConfigurationSupport.requestMappingHandlerAdapter() (the @Bean method)
        //////////////////////////////////////////////////////////////////////////////////////////
        this.webBindingInitializer = requestMappingHandlerAdapter.getWebBindingInitializer();
        HandlerMethodArgumentResolverComposite argumentResolvers = new HandlerMethodArgumentResolverComposite();
        argumentResolvers.addResolver( new PathTokenArgumentResolver( conversionService ) );
        argumentResolvers.addResolvers( requestMappingHandlerAdapter.getArgumentResolvers() );
        this.argumentResolvers = argumentResolvers;

        // REST setup
        this.conversionService = conversionService;
        this.traceManager = traceManager;

        // find the REST roots: if a REST root bean is given in the configuration, use that; otherwise, all beans with @RestRoot
        String rootBeanName = restProperties.getRootBean();
        if( rootBeanName != null )
        {
            this.rootResources = singletonList( applicationContext.getBean( rootBeanName ) );
        }
        else
        {
            this.rootResources = new LinkedList<>( applicationContext.getBeansWithAnnotation( RestRoot.class ).values() );
            this.rootResources.sort( new AnnotationAwareOrderComparator() );
        }
    }

    @NotNull
    @RequestMapping( path = "/**", method = { OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE } )
    public Object handle( @NotNull HttpServletRequest request, @NotNull HttpServletResponse response ) throws Exception
    {
        RestResourceExecutorImpl resourceExecutor = new RestResourceExecutorImpl( this.traceManager,
                                                                                  this::invokeMethod,
                                                                                  this::evaluateCondition,
                                                                                  request,
                                                                                  response );
        for( Object root : this.rootResources )
        {
            RestResourceLocatorImpl resourceLocator = new RestResourceLocatorImpl( this::invokeMethod,
                                                                                   this::evaluateCondition,
                                                                                   root,
                                                                                   request,
                                                                                   response );
            RestWrapper restWrapper = root instanceof RestWrapper ? ( RestWrapper ) root : new SimpleRestWrapper( root );
            Optional<Object> result = restWrapper.resolveAndExecute( resourceLocator, resourceExecutor );
            if( result.isPresent() )
            {
                return result.get();
            }
        }
        throw RestExceptions.notFound();
    }

    private boolean evaluateCondition( @NotNull HttpServletRequest request,
                                       @NotNull Object evalRoot,
                                       @NotNull String condition )
    {
        if( condition.isEmpty() )
        {
            return true;
        }

        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setBeanResolver( new BeanFactoryResolver( this.applicationContext ) );
        context.setRootObject( evalRoot );
        context.setTypeConverter( new StandardTypeConverter( this.conversionService ) );
        context.setVariable( "request", request );

        SpelExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression( condition );
        return exp.getValue( context, Boolean.class );
    }

    private Object invokeMethod( @NotNull Object target,
                                 @NotNull Method handlerMethod,
                                 @NotNull HttpServletRequest request,
                                 @NotNull HttpServletResponse response ) throws Exception
    {
        WebDataBinderFactory binderFactory = new ServletRequestDataBinderFactory( emptyList(), this.webBindingInitializer );
        ModelFactory modelFactory =
                new ModelFactory(
                        emptyList(),
                        binderFactory,
                        this.sessionAttributesHandlerCache.computeIfAbsent(
                                target.getClass(),
                                k -> new SessionAttributesHandler( k, this.sessionAttributeStore ) ) );

        InvocableHandlerMethod invocableMethod = new InvocableHandlerMethod( target, handlerMethod );
        invocableMethod.setHandlerMethodArgumentResolvers( this.argumentResolvers );
        invocableMethod.setDataBinderFactory( binderFactory );
        invocableMethod.setParameterNameDiscoverer( this.parameterNameDiscoverer );

        ServletWebRequest webRequest = new ServletWebRequest( request, response );
        ModelAndViewContainer mavContainer = new ModelAndViewContainer();
        mavContainer.addAllAttributes( RequestContextUtils.getInputFlashMap( request ) );
        modelFactory.initModel( webRequest, mavContainer, invocableMethod );
        mavContainer.setIgnoreDefaultModelOnRedirect( false );

        return invocableMethod.invokeForRequest( webRequest, mavContainer );
    }
}
