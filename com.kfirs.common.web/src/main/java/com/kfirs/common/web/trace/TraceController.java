/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author arik
 * @on 5/15/17.
 */
@Controller
public class TraceController
{
    @NotNull
    private static final CacheControl ENTRY_CACHE_CONTROL = CacheControl.maxAge( 1, TimeUnit.HOURS ).mustRevalidate();

    @NotNull
    private static final CacheControl NO_ENTRY_CACHE_CONTROL = CacheControl.noStore();

    @NotNull
    private final ObjectWriter objectWriter;

    @NotNull
    private final TraceRepository traceRepository;

    public TraceController( @NotNull ObjectMapper objectMapper,
                            @NotNull TraceRepository traceRepository )
    {
        this.objectWriter = objectMapper.writer().without( SerializationFeature.FAIL_ON_EMPTY_BEANS ).withDefaultPrettyPrinter();
        this.traceRepository = traceRepository;
    }

    @RequestMapping( path = "/trace/{requestId}", method = GET, produces = TEXT_HTML_VALUE )
    public void showRequestForBrowser( @PathVariable String requestId, @NotNull HttpServletResponse response )
            throws IOException
    {
        Optional<TraceRepository.Trace> traceHolder = this.traceRepository.get( requestId );
        if( !traceHolder.isPresent() )
        {
            response.setStatus( HttpStatus.NOT_FOUND.value() );
            response.setHeader( HttpHeaders.CACHE_CONTROL, NO_ENTRY_CACHE_CONTROL.getHeaderValue() );
            return;
        }
        TraceRepository.Trace trace = traceHolder.get();

        // set up the response
        response.setHeader( HttpHeaders.CACHE_CONTROL, ENTRY_CACHE_CONTROL.getHeaderValue() );
        response.setHeader( HttpHeaders.CONTENT_TYPE, TEXT_HTML_VALUE );
        PrintWriter writer = response.getWriter();

        // HTML prefix
        writer.println( "<!DOCTYPE html>" );
        writer.println( "<html>" );
        writer.println( "  <head>" );
        writer.println( "    <title>TRACE: " + requestId + "</title>" );
        writer.println( "  </head>" );
        writer.println( "  <body>" );
        writer.println( "    <h1>Trace log for request \"" + requestId + "\"</h2>" );
        writer.println( "    <br>" );

        // write trace objects
        writer.println( "    <h2>Trace objects:</h2>" );
        writer.println( "    <pre>" );
        trace.getValues()
             .entrySet()
             .stream()
             .map( e -> {
                 Object value = e.getValue();
                 try
                 {
                     if( value instanceof Throwable )
                     {
                         return e.getKey() + ":\n" + ExceptionUtils.getStackTrace( ( Throwable ) value );
                     }
                     else
                     {
                         return e.getKey() + ": " + this.objectWriter.writeValueAsString( value );
                     }
                 }
                 catch( JsonProcessingException e1 )
                 {
                     return e.getKey() + ": " + value;
                 }
             } )
             .map( StringEscapeUtils::escapeHtml4 )
             .forEach( writer::println );
        writer.println( "    </pre>" );

        // write trace messages
        writer.println( "    <h2>Trace messages:</h2>" );
        writer.println( "    <pre>" );
        trace.getMessages().stream().map( StringEscapeUtils::escapeHtml4 ).forEach( writer::println );
        writer.println( "    </pre>" );

        // HTML suffix
        writer.println( "  </body>" );
        writer.println( "</html>" );
    }

    @RequestMapping( path = "/trace/{requestId}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE )
    public void showRequestAsJson( @PathVariable String requestId, @NotNull HttpServletResponse response )
            throws IOException
    {
        Optional<TraceRepository.Trace> traceHolder = this.traceRepository.get( requestId );
        if( !traceHolder.isPresent() )
        {
            response.setStatus( HttpStatus.NOT_FOUND.value() );
            response.setHeader( HttpHeaders.CACHE_CONTROL, NO_ENTRY_CACHE_CONTROL.getHeaderValue() );
            return;
        }
        TraceRepository.Trace trace = traceHolder.get();

        // set up the response
        response.setHeader( HttpHeaders.CACHE_CONTROL, ENTRY_CACHE_CONTROL.getHeaderValue() );
        response.setHeader( HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE );
        this.objectWriter.writeValue( response.getWriter(), trace );
    }
}
