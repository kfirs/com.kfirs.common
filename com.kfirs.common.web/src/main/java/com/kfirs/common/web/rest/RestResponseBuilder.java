/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.rest;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.rest.exception.RestError;
import java.util.LinkedList;
import java.util.List;

/**
 * @author arik
 * @on 6/23/17.
 */
public final class RestResponseBuilder<ResourceType>
{
    @NotNull
    public static <ResourceType> RestResponseBuilder<ResourceType> newBuilder()
    {
        return new RestResponseBuilder<>();
    }

    @NotNull
    public static <ResourceType> RestResponseBuilder<ResourceType> newBuilder( @Nullable ResourceType resource )
    {
        return new RestResponseBuilder<>( resource );
    }

    @NotNull
    public static <ResourceType> RestResponseBuilder<ResourceType> newBuilder( @NotNull String path,
                                                                               @NotNull String code,
                                                                               @NotNull String message )
    {
        return new RestResponseBuilder<>( path, code, message );
    }

    @NotNull
    public static <ResourceType> RestResponseBuilder<ResourceType> newBuilder( @NotNull RestError error )
    {
        return new RestResponseBuilder<>( error );
    }

    @NotNull
    private final List<RestError> errors = new LinkedList<>();

    @Nullable
    private ResourceType resource;

    private RestResponseBuilder()
    {
    }

    private RestResponseBuilder( @Nullable ResourceType resource )
    {
        this.resource = resource;
    }

    private RestResponseBuilder( @NotNull String path,
                                 @NotNull String code,
                                 @NotNull String message )
    {
        this( new RestError( path, code, message ) );
    }

    private RestResponseBuilder( @NotNull RestError error )
    {
        this.errors.add( error );
    }

    @NotNull
    public RestResponseBuilder<ResourceType> withError( @NotNull String path,
                                                        @NotNull String code,
                                                        @NotNull String message )
    {
        return withError( new RestError( path, code, message ) );
    }

    @NotNull
    public RestResponseBuilder<ResourceType> withError( @NotNull RestError error )
    {
        this.errors.add( error );
        return this;
    }

    @NotNull
    public RestResponseBuilder<ResourceType> withResource( @NotNull ResourceType resource )
    {
        this.resource = resource;
        return this;
    }

    @NotNull
    public RestResponse<ResourceType> build()
    {
        return new RestResponse<>( this.errors, this.resource );
    }
}
