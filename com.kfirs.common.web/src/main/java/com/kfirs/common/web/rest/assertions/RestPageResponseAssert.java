/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.assertions;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.util.*;
import org.assertj.core.api.ListAssert;
import org.springframework.http.ResponseEntity;

import static java.util.Objects.requireNonNull;

/**
 * @author arik
 * @on 2/19/17.
 */
public class RestPageResponseAssert<S extends RestPageResponseAssert<S, T, X>, T extends com.kfirs.common.web.rest.Page<X>, X>
        extends ResponseEntityAssert<S, T>
{
    public RestPageResponseAssert( @Nullable ResponseEntity<T> actual )
    {
        super( actual, RestPageResponseAssert.class );
    }

    public RestPageResponseAssert( @Nullable ResponseEntity<T> actual, @NotNull Class<?> selfType )
    {
        super( actual, selfType );
    }

    @NotNull
    public S hasPageNumber( Integer expectedPageNumber )
    {
        hasBody();
        Integer actualPageNumber = requireNonNull( this.actual.getBody() ).getPageNumber();
        if( !Objects.equals( expectedPageNumber, actualPageNumber ) )
        {
            failWithMessage( "Expected page number to be %d, but it was %d", expectedPageNumber, actualPageNumber );
        }
        return this.myself;
    }

    @NotNull
    public S hasPageSize( Integer expectedPageSize )
    {
        hasBody();
        Integer actualPageSize = requireNonNull( this.actual.getBody() ).getPageSize();
        if( !Objects.equals( expectedPageSize, actualPageSize ) )
        {
            failWithMessage( "Expected page size to be %d, but it was %d", expectedPageSize, actualPageSize );
        }
        return this.myself;
    }

    @NotNull
    public S hasTotalItems( @Nullable Integer expectedTotal )
    {
        return hasTotalItems( Optional.ofNullable( expectedTotal ).map( Integer::longValue ).orElse( null ) );
    }

    @NotNull
    public S hasTotalItems( @Nullable Long expectedTotal )
    {
        hasBody();
        Long actualTotal = requireNonNull( this.actual.getBody() ).getTotalCount();
        if( !Objects.equals( expectedTotal, actualTotal ) )
        {
            failWithMessage( "Expected page to have %d total, but it was %d", expectedTotal, actualTotal );
        }
        return this.myself;
    }

    @NotNull
    public S hasItems()
    {
        hasBody();
        if( requireNonNull( this.actual.getBody() ).getItems().isEmpty() )
        {
            failWithMessage( "Expected page to have items, but it did not" );
        }
        return this.myself;
    }

    @NotNull
    public ListAssert<X> assertOnItemsAsList()
    {
        hasBody();
        Collection<X> items = requireNonNull( this.actual.getBody() ).getItems();
        return RestAssertions.assertThat( items instanceof List ? ( List<X> ) items : new LinkedList<>( items ) );
    }
}
