/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.handler;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.rest.RestResource;
import com.kfirs.common.web.rest.RestResourceLocator;
import com.kfirs.common.web.rest.annotation.Path;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.aop.framework.Advised;
import org.springframework.core.annotation.AnnotatedElementUtils;

import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static org.springframework.util.ReflectionUtils.getUniqueDeclaredMethods;

/**
 * @author arik
 * @on 5/26/17.
 */
public class RestResourceLocatorImpl implements RestResourceLocator
{
    @NotNull
    private final HttpMethodInvoker httpMethodInvoker;

    @NotNull
    private final HttpConditionEvaluator httpConditionEvaluator;

    @NotNull
    private final Object rootResource;

    @NotNull
    private final HttpServletRequest request;

    @NotNull
    private final HttpServletResponse response;

    public RestResourceLocatorImpl( @NotNull HttpMethodInvoker httpMethodInvoker,
                                    @NotNull HttpConditionEvaluator httpConditionEvaluator,
                                    @NotNull Object rootResource,
                                    @NotNull HttpServletRequest request,
                                    @NotNull HttpServletResponse response )
    {
        this.httpMethodInvoker = httpMethodInvoker;
        this.httpConditionEvaluator = httpConditionEvaluator;
        this.rootResource = rootResource;
        this.request = request;
        this.response = response;
    }

    @NotNull
    @Override
    public Optional<RestResource> resolve() throws Exception
    {
        // if the URI is just "/" or "", we're actually testing the root resource, and will not be traversing downstream
        String uri = this.request.getRequestURI();
        if( uri.isEmpty() || uri.equalsIgnoreCase( "/" ) )
        {
            return Optional.of( new RestResource( singletonList( this.rootResource ) ) );
        }

        // traverse URI corresponding to the resource object methods
        // if a URI token could not be translated to a resource method, return an empty Optional object,
        // signaling that this resource cannot serve this URI (will result in 404)
        List<Object> resources = new LinkedList<>();
        resources.add( this.rootResource );
        for( String token : uri.split( "/" ) )
        {
            if( !token.isEmpty() )
            {
                Object resource = resources.get( resources.size() - 1 );
                Class<?> resourceClass = resource.getClass();
                if( resource instanceof Advised )
                {
                    Advised advised = ( Advised ) resource;
                    resourceClass = advised.getTargetSource().getTargetClass();
                }
                Method[] uniqueDeclaredMethods = getUniqueDeclaredMethods( resourceClass );
                Method method = Stream.of( uniqueDeclaredMethods )
                                      .map( m -> Pair.of( m, methodMatchesPath( this.request, resource, m, token ) ) )
                                      .filter( pair -> pair.getRight() != null )
                                      .sorted( comparing( Pair::getRight ) )
                                      .map( Pair::getLeft )
                                      .findFirst()
                                      .orElse( null );
                if( method != null )
                {
                    this.request.setAttribute( RestHandler.PATH_TOKEN_ATTRIBUTE_NAME, token );
                    Object nextResource = this.httpMethodInvoker.invokeMethod( resource, method, this.request, this.response );
                    if( nextResource != null )
                    {
                        resources.add( nextResource );
                    }
                    else
                    {
                        return Optional.empty();
                    }
                }
                else
                {
                    return Optional.empty();
                }
            }
        }

        // found a matching resource path!
        return Optional.of( new RestResource( resources ) );
    }

    @Nullable
    private MethodMatchType methodMatchesPath( @NotNull HttpServletRequest request,
                                               @NotNull Object resource,
                                               @NotNull Method method,
                                               @NotNull String pathToken )
    {
        Path pathAnn = AnnotatedElementUtils.findMergedAnnotation( method, Path.class );
        if( pathAnn == null || !this.httpConditionEvaluator.evaluate( request, resource, pathAnn.conditional() ) )
        {
            // not matching
            return null;
        }

        String methodName = method.getName();
        String methodRegexToken = pathAnn.regex();
        if( !methodRegexToken.isEmpty() )
        {
            return pathToken.matches( methodRegexToken ) ? MethodMatchType.REGEX : null;
        }

        String methodPathToken = pathAnn.path();
        if( !methodPathToken.isEmpty() )
        {
            return pathToken.equalsIgnoreCase( methodPathToken ) ? MethodMatchType.EXPLICIT_PATH : null;
        }
        else if( methodName.startsWith( "get" ) )
        {
            return pathToken.equalsIgnoreCase( methodName.substring( "get".length() ) ) ? MethodMatchType.IMPLICIT_PATH : null;
        }
        else if( methodName.startsWith( "resolve" ) )
        {
            return pathToken.equalsIgnoreCase( methodName.substring( "resolve".length() ) ) ? MethodMatchType.IMPLICIT_PATH : null;
        }
        else
        {
            return pathToken.equalsIgnoreCase( methodName ) ? MethodMatchType.IMPLICIT_PATH : null;
        }
    }

    private enum MethodMatchType
    {
        IMPLICIT_PATH,
        EXPLICIT_PATH,
        REGEX
    }
}
