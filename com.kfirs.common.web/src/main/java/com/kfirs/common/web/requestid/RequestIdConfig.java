/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.requestid;

import com.kfirs.common.util.lang.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author arik
 * @on 6/24/17.
 */
@Configuration
public class RequestIdConfig
{
    @Bean
    public ThreadBoundRequestIdManager threadBoundRequestIdManager()
    {
        return new ThreadBoundRequestIdManager();
    }

    @Bean
    public RequestIdPropagatingClientHttpInterceptor requestIdPropagatingClientHttpInterceptor(
            @NotNull RequestIdManager requestIdManager )
    {
        return new RequestIdPropagatingClientHttpInterceptor( requestIdManager );
    }

    @Bean
    public RequestIdInterceptor uniqueRequestIdInterceptor( @NotNull RequestIdManager requestIdManager )
    {
        return new RequestIdInterceptor( requestIdManager );
    }
}
