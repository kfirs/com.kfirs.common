/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.requestid;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.interceptors.AutoHandlerInterceptor;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * @author arik
 * @on 2/13/17.
 */
public class RequestIdInterceptor extends HandlerInterceptorAdapter implements AutoHandlerInterceptor
{
    // TODO arik: the request ID header name should be customizable by the application
    public static final String REQUEST_ID_HEADER = "X-Request-Id";

    @NotNull
    private final RequestIdManager requestIdManager;

    public RequestIdInterceptor( @NotNull RequestIdManager requestIdManager )
    {
        this.requestIdManager = requestIdManager;
    }

    @Override
    public boolean preHandle( @NotNull HttpServletRequest request,
                              @NotNull HttpServletResponse response,
                              @NotNull Object handler ) throws Exception
    {
        UUID uuid = Optional.ofNullable( request.getHeader( REQUEST_ID_HEADER ) )
                            .map( this.requestIdManager::setUuidForCurrentThread )
                            .orElseGet( this.requestIdManager::getOrGenerateUuidForCurrentThread );
        response.setHeader( REQUEST_ID_HEADER, uuid.toString() );
        return true;
    }

    @Override
    public void afterCompletion( @NotNull HttpServletRequest request,
                                 @NotNull HttpServletResponse response,
                                 @NotNull Object handler,
                                 @Nullable Exception ex ) throws Exception
    {
        this.requestIdManager.releaseUuidForCurrentThread();
    }
}
