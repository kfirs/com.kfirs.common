/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.server;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.interceptors.AutoHandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * @author arik
 * @on 2/13/17.
 */
public class ServerHeaderSettingInterceptor extends HandlerInterceptorAdapter implements AutoHandlerInterceptor
{
    @NotNull
    private final GitProperties gitProperties;

    @NotNull
    private final BuildProperties buildProperties;

    public ServerHeaderSettingInterceptor( @NotNull GitProperties gitProperties,
                                           @NotNull BuildProperties buildProperties )
    {
        this.gitProperties = gitProperties;
        this.buildProperties = buildProperties;
    }

    @Override
    public boolean preHandle( @NotNull HttpServletRequest request,
                              @NotNull HttpServletResponse response,
                              @NotNull Object handler ) throws Exception
    {
        String productName = this.buildProperties.getName();
        String productVersion = this.buildProperties.getVersion();
        String shortCommitId = this.gitProperties.getShortCommitId();
        response.setHeader( "Server", productName + "-" + productVersion + " (" + shortCommitId + ")" );
        return true;
    }
}
