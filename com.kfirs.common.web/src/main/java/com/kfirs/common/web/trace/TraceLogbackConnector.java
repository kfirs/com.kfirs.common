/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import ch.qos.logback.classic.LoggerContext;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.EventListener;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 * @on 5/16/17.
 */
public class TraceLogbackConnector
{
    private static final Logger LOG = getLogger( TraceLogbackConnector.class );

    @EventListener
    public void connectTraceManagerToLogbackContext( @NotNull ApplicationReadyEvent event )
    {
        ILoggerFactory loggerFactory = LoggerFactory.getILoggerFactory();
        if( loggerFactory instanceof LoggerContext )
        {
            ConfigurableApplicationContext applicationContext = event.getApplicationContext();
            TraceManagerLocatorImpl traceManagerLocator = new TraceManagerLocatorImpl( applicationContext );

            LoggerContext loggerContext = ( LoggerContext ) loggerFactory;
            loggerContext.putObject( LogbackTraceAppender.TRACE_MANAGER_LOCATOR_KEY, traceManagerLocator );
        }
        else
        {
            throw new IllegalStateException( "could not find LoggerContext from SLF4J!" );
        }
    }

    private class TraceManagerLocatorImpl implements TraceManagerLocator
    {
        @NotNull
        private final ApplicationContext applicationContext;

        public TraceManagerLocatorImpl( @NotNull ApplicationContext applicationContext )
        {
            this.applicationContext = applicationContext;
        }

        @Nullable
        @Override
        public TraceManager getTraceManager()
        {
            try
            {
                return this.applicationContext.getBean( TraceManager.class );
            }
            catch( BeansException e )
            {
                LOG.warn( "Attempted to lookup TraceManager outside of a request context!", e );
                return null;
            }
        }
    }
}
