/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author arik
 * @on 5/28/17.
 * @todo add sort information
 */
public class Page<T>
{
    private final Integer pageCount;

    private final Integer pageNumber;

    private final Integer pageSize;

    private final Long totalCount;

    private final Boolean first;

    private final Boolean last;

    private final Boolean hasNext;

    private final Boolean hasPrevious;

    @NotNull
    private final Collection<T> items;

    public Page( @NotNull org.springframework.data.domain.Page<T> page )
    {
        this.pageCount = page.getTotalPages();
        this.pageNumber = page.getNumber();
        this.pageSize = page.getSize();
        this.totalCount = page.getTotalElements();
        this.first = page.isFirst();
        this.last = page.isLast();
        this.hasNext = page.hasNext();
        this.hasPrevious = page.hasPrevious();
        this.items = page.getContent();
    }

    @JsonCreator
    public Page( @JsonProperty( "pageCount" ) Integer pageCount,
                 @JsonProperty( "pageNumber" ) Integer pageNumber,
                 @JsonProperty( "pageSize" ) Integer pageSize,
                 @JsonProperty( "totalCount" ) Long totalCount,
                 @JsonProperty( "first" ) Boolean first,
                 @JsonProperty( "last" ) Boolean last,
                 @JsonProperty( "hasNext" ) Boolean hasNext,
                 @JsonProperty( "hasPrevious" ) Boolean hasPrevious,
                 @JsonProperty( "items" ) @Nullable Collection<T> items )
    {
        this.pageCount = pageCount;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.first = first;
        this.last = last;
        this.hasNext = hasNext;
        this.hasPrevious = hasPrevious;
        this.items = Optional.ofNullable( items ).orElseGet( Collections::<T>emptyList );
    }

    public Integer getPageCount()
    {
        return pageCount;
    }

    public Integer getPageNumber()
    {
        return pageNumber;
    }

    public Integer getPageSize()
    {
        return pageSize;
    }

    public Long getTotalCount()
    {
        return totalCount;
    }

    public Boolean isFirst()
    {
        return first;
    }

    public Boolean isLast()
    {
        return last;
    }

    public Boolean isHasNext()
    {
        return hasNext;
    }

    public Boolean isHasPrevious()
    {
        return hasPrevious;
    }

    @NotNull
    public Collection<T> getItems()
    {
        return items;
    }

    @Override
    public boolean equals( Object o )
    {
        if( this == o )
        {
            return true;
        }
        if( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        Page<?> page = ( Page<?> ) o;
        return pageCount == page.pageCount &&
               pageNumber == page.pageNumber &&
               pageSize == page.pageSize &&
               totalCount == page.totalCount &&
               first == page.first &&
               last == page.last &&
               hasNext == page.hasNext &&
               hasPrevious == page.hasPrevious &&
               Objects.equals( items, page.items );
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( pageCount, pageNumber, pageSize, totalCount, first, last, hasNext, hasPrevious, items );
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder( this )
                .append( "pageCount", pageCount )
                .append( "pageNumber", pageNumber )
                .append( "pageSize", pageSize )
                .append( "totalCount", totalCount )
                .append( "first", first )
                .append( "last", last )
                .append( "hasNext", hasNext )
                .append( "hasPrevious", hasPrevious )
                .append( "items", items )
                .toString();
    }
}
