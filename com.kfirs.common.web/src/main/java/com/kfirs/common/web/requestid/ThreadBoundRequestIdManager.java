/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.requestid;

import com.kfirs.common.util.lang.NotNull;
import java.util.UUID;

/**
 * @author arik
 * @on 2/15/17.
 */
public class ThreadBoundRequestIdManager implements RequestIdManager
{
    @NotNull
    private static final ThreadLocal<UUID> uuid = ThreadLocal.withInitial( UUID::randomUUID );

    @NotNull
    @Override
    public UUID getOrGenerateUuidForCurrentThread()
    {
        return uuid.get();
    }

    @NotNull
    @Override
    public UUID setUuidForCurrentThread( @NotNull String uuid )
    {
        ThreadBoundRequestIdManager.uuid.set( UUID.fromString( uuid ) );
        return getOrGenerateUuidForCurrentThread();
    }

    @Override
    public void releaseUuidForCurrentThread()
    {
        uuid.remove();
    }
}
