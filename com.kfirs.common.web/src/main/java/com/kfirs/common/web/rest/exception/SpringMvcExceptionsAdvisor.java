/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.rest.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.rest.handler.RestHandler;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author arik
 * @on 6/24/17.
 */
@RestControllerAdvice( assignableTypes = RestHandler.class )
@Order( 0 )
public class SpringMvcExceptionsAdvisor
{
    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleNoSuchRequestHandling(
            @NotNull HttpServletRequest request,
            @SuppressWarnings( "deprecation" )
            @NotNull org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException e )
    {
        throw RestExceptions.notFound( e );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleNoHandlerFoundException( @NotNull HttpServletRequest request,
                                                            @NotNull NoHandlerFoundException e )
    {
        throw RestExceptions.notFound( e );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleMissingServletRequestParameterException(
            @NotNull HttpServletRequest request,
            @NotNull MissingServletRequestParameterException e )
    {
        throw RestExceptions.requestParameterMissing( e, e.getParameterName() );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleServletRequestBindingException( @NotNull HttpServletRequest request,
                                                                   @NotNull ServletRequestBindingException e )
    {
        throw RestExceptions.badRequest( e );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleMethodArgumentNotValidException( @NotNull HttpServletRequest request,
                                                                    @NotNull MethodArgumentNotValidException e )
    {
        throw RestExceptions.badRequest( e ).copyBindingErrors( e.getBindingResult() );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleMissingServletRequestPartException( @NotNull HttpServletRequest request,
                                                                       @NotNull MissingServletRequestPartException e )
    {
        throw RestExceptions.requestPartMissing( e, e.getRequestPartName() );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleBindException( @NotNull HttpServletRequest request, @NotNull BindException e )
    {
        throw RestExceptions.badRequest( e ).copyBindingErrors( e.getBindingResult() );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleHttpMessageNotReadableException( @NotNull HttpServletRequest request,
                                                                    @NotNull HttpMessageNotReadableException e )
    {
        Throwable cause = e.getCause();
        if( cause instanceof JsonMappingException )
        {
            JsonMappingException mappingException = ( JsonMappingException ) cause;
            List<JsonMappingException.Reference> path = mappingException.getPath();
            String pathField = path.isEmpty() ? "" : path.get( path.size() - 1 ).getFieldName();
            throw RestExceptions.illegalValue( e, pathField );
        }
        else
        {
            throw RestExceptions.requestContentUnreadable( e );
        }
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleHttpRequestMethodNotSupportedException(
            @NotNull HttpServletRequest request,
            @NotNull HttpRequestMethodNotSupportedException e )
    {
        throw RestExceptions.methodNotAllowed( e, e.getSupportedHttpMethods() );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleHttpMediaTypeNotSupportedException( @NotNull HttpServletRequest request,
                                                                       @NotNull HttpMediaTypeNotSupportedException e )
    {
        throw RestExceptions.unsupportedMediaType( request.getContentType(), e, e.getSupportedMediaTypes() );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleHttpMediaTypeNotAcceptableException( @NotNull HttpServletRequest request,
                                                                        @NotNull HttpMediaTypeNotAcceptableException e )
    {
        throw RestExceptions.notAcceptable( e );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleHttpMessageNotWritableException( @NotNull HttpServletRequest request,
                                                                    @NotNull HttpMessageNotWritableException e )
    {
        throw RestExceptions.internalServerError( e );
    }
}
