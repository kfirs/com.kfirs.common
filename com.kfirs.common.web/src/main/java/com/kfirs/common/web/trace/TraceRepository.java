/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.kfirs.common.util.lang.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author arik
 * @on 5/15/17.
 */
public class TraceRepository
{
    @NotNull
    private final Cache<String, Trace> traceCache = Caffeine.newBuilder()
                                                            .expireAfterWrite( 5, TimeUnit.MINUTES )
                                                            .initialCapacity( 100 )
                                                            .maximumSize( 1000 )
                                                            .build();

    @NotNull
    public Trace add( @NotNull String key, @NotNull Map<String, Object> values, @NotNull List<String> messages )
    {
        Trace trace = new Trace( key, values, messages );
        this.traceCache.put( key, trace );
        return trace;
    }

    @NotNull
    public Optional<Trace> get( @NotNull String key )
    {
        return Optional.ofNullable( this.traceCache.getIfPresent( key ) );
    }

    public static class Trace
    {
        @NotNull
        private final String key;

        @NotNull
        private final Map<String, Object> values;

        @NotNull
        private final List<String> messages;

        private Trace( @NotNull String key, @NotNull Map<String, Object> values, @NotNull List<String> messages )
        {
            this.key = key;
            this.values = values;
            this.messages = messages;
        }

        @NotNull
        public String getKey()
        {
            return this.key;
        }

        @NotNull
        public Map<String, Object> getValues()
        {
            return this.values;
        }

        @NotNull
        public List<String> getMessages()
        {
            return this.messages;
        }
    }
}
