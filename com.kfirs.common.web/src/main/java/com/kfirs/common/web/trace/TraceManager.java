/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import ch.qos.logback.classic.LoggerContext;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;

import static com.kfirs.common.web.trace.TraceProperties.TraceSendingStrategy.ALWAYS;
import static com.kfirs.common.web.trace.TraceProperties.TraceSendingStrategy.ON_TRACE_PARAMETER;
import static com.kfirs.common.web.util.HttpServletRequestUtil.getUnderlyingRequest;
import static java.lang.Boolean.parseBoolean;
import static java.util.Arrays.asList;

/**
 * @author arik
 * @on 4/19/17.
 */
public class TraceManager
{
    @NotNull
    private static final String TRACE_MARKER_KEY = TraceManager.class.getName() + "#marker";

    @NotNull
    private static final String TRACE_MESSAGES_KEY = TraceManager.class.getName() + "#messages";

    @NotNull
    private static final String TRACE_VALUES_KEY = TraceManager.class.getName() + "#values";

    @NotNull
    private final TraceProperties properties;

    @NotNull
    private final ObjectProvider<HttpServletRequest> requestProvider;

    public TraceManager( @NotNull TraceProperties properties,
                         @NotNull ObjectProvider<HttpServletRequest> requestProvider )
    {
        this.properties = properties;
        this.requestProvider = requestProvider;

        ILoggerFactory loggerFactory = LoggerFactory.getILoggerFactory();
        if( loggerFactory instanceof LoggerContext )
        {
            LoggerContext loggerContext = ( LoggerContext ) loggerFactory;
            loggerContext.putObject( LogbackTraceAppender.TRACE_MANAGER_LOCATOR_KEY, this );
        }
        else
        {
            throw new IllegalStateException( "could not find LoggerContext from SLF4J!" );
        }
    }

    public boolean isTraceRequest()
    {
        HttpServletRequest request = getUnderlyingRequest( this.requestProvider.getObject() );

        Object marker = request.getAttribute( TRACE_MARKER_KEY );
        if( marker instanceof Boolean )
        {
            return ( Boolean ) marker;
        }

        // TODO arik: allow trace IP whitelist to be dynamic (eg. from a database); perhaps a "TraceWhitelistProvider"?
        List<String> traceIpWhitelist = asList( this.properties.getTraceIpWhitelist() );
        if( !traceIpWhitelist.contains( request.getRemoteAddr() ) )
        {
            request.setAttribute( TRACE_MARKER_KEY, false );
            return false;
        }

        TraceProperties.TraceSendingStrategy strategy = this.properties.getTraceSendingStrategy();
        if( strategy == ALWAYS )
        {
            request.setAttribute( TRACE_MARKER_KEY, true );
            return true;
        }

        String value = request.getParameter( "_trace" );
        boolean trace = strategy == ON_TRACE_PARAMETER && value != null && ( value.trim().isEmpty() || parseBoolean( value ) );
        request.setAttribute( TRACE_MARKER_KEY, trace );
        return trace;
    }

    @NotNull
    public List<String> getTraceMessages()
    {
        if( !isTraceRequest() )
        {
            return Collections.emptyList();
        }

        Object traceMessagesListObject = getUnderlyingRequest( this.requestProvider.getObject() ).getAttribute( TRACE_MESSAGES_KEY );
        if( traceMessagesListObject == null )
        {
            return Collections.emptyList();
        }
        else if( traceMessagesListObject instanceof List )
        {
            @SuppressWarnings( "unchecked" )
            List<String> traceMessagesList = ( List<String> ) traceMessagesListObject;
            return traceMessagesList;
        }
        else
        {
            return Collections.emptyList();
        }
    }

    void trace( @NotNull String message )
    {
        if( isTraceRequest() )
        {
            HttpServletRequest request = getUnderlyingRequest( this.requestProvider.getObject() );
            Object traceMessageListObject = request.getAttribute( TRACE_MESSAGES_KEY );
            if( traceMessageListObject == null )
            {
                traceMessageListObject = new LinkedList<>();
                request.setAttribute( TRACE_MESSAGES_KEY, traceMessageListObject );
            }
            if( traceMessageListObject instanceof List )
            {
                @SuppressWarnings( "unchecked" )
                List<String> traceMessagesList = ( List<String> ) traceMessageListObject;
                traceMessagesList.add( message );
            }
        }
    }

    @NotNull
    public Map<String, Object> getTraceValues()
    {
        if( !isTraceRequest() )
        {
            return Collections.emptyMap();
        }

        Object traceValuesMapObject = this.requestProvider.getObject().getAttribute( TRACE_VALUES_KEY );
        if( traceValuesMapObject == null )
        {
            return Collections.emptyMap();
        }
        else if( traceValuesMapObject instanceof Map )
        {
            @SuppressWarnings( "unchecked" )
            Map<String, Object> traceValuesMap = ( Map<String, Object> ) traceValuesMapObject;
            return traceValuesMap;
        }
        else
        {
            return Collections.emptyMap();
        }
    }

    public void addTraceValue( @NotNull String key, @Nullable Object value )
    {
        if( isTraceRequest() )
        {
            HttpServletRequest request = getUnderlyingRequest( this.requestProvider.getObject() );

            Object traceValuesMapObject = request.getAttribute( TRACE_VALUES_KEY );
            if( traceValuesMapObject == null )
            {
                traceValuesMapObject = new LinkedHashMap<>();
                request.setAttribute( TRACE_VALUES_KEY, traceValuesMapObject );
            }

            if( traceValuesMapObject instanceof Map )
            {
                @SuppressWarnings( "unchecked" )
                Map<String, Object> traceValuesMap = ( Map<String, Object> ) traceValuesMapObject;
                traceValuesMap.put( key, value );
            }
        }
    }
}
