/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.security;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * @author arik
 * @on 6/24/17.
 */
public class ExceptionSavingAuthenticationEntryPoint implements AuthenticationEntryPoint
{
    @NotNull
    private final DefaultErrorAttributes defaultErrorAttributes;

    @NotNull
    private final AuthenticationEntryPoint targetAuthenticationEntryPoint;

    public ExceptionSavingAuthenticationEntryPoint(
            @NotNull DefaultErrorAttributes defaultErrorAttributes,
            @NotNull AuthenticationEntryPoint targetAuthenticationEntryPoint )
    {
        this.defaultErrorAttributes = defaultErrorAttributes;
        this.targetAuthenticationEntryPoint = targetAuthenticationEntryPoint;
    }

    @Override
    public void commence( HttpServletRequest request,
                          HttpServletResponse response,
                          AuthenticationException authException ) throws IOException, ServletException
    {
        // save the exception in the error attributes, so it is available for RestExceptionController
        this.defaultErrorAttributes.resolveException( request, response, null, authException );
        this.targetAuthenticationEntryPoint.commence( request, response, authException );
    }
}
