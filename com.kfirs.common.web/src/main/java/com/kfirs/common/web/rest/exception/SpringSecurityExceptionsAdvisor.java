/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.rest.exception;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.rest.handler.RestHandler;
import javax.servlet.http.HttpServletRequest;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author arik
 * @on 6/24/17.
 */
@RestControllerAdvice( assignableTypes = RestHandler.class )
@Order( 0 )
public class SpringSecurityExceptionsAdvisor
{
    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleAuthenticationException( @NotNull HttpServletRequest request,
                                                            @NotNull AuthenticationException e )
    {
        throw RestExceptions.authenticationRequired( e );
    }

    @NotNull
    @ExceptionHandler
    public ResponseEntity<?> handleAccessDeniedException( @NotNull HttpServletRequest request,
                                                          @NotNull AccessDeniedException e )
    {
        throw RestExceptions.forbidden( e );
    }
}
