/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.security;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

/**
 * @author arik
 * @on 6/24/17.
 */
public class ExceptionSavingAccessDeniedHandler extends AccessDeniedHandlerImpl
{
    @NotNull
    private final DefaultErrorAttributes defaultErrorAttributes;

    public ExceptionSavingAccessDeniedHandler(
            @NotNull DefaultErrorAttributes defaultErrorAttributes )
    {
        this.defaultErrorAttributes = defaultErrorAttributes;
    }

    @Override
    public void handle( HttpServletRequest request,
                        HttpServletResponse response,
                        AccessDeniedException accessDeniedException ) throws IOException, ServletException
    {
        // save the exception in the error attributes, so it is available for RestExceptionController
        this.defaultErrorAttributes.resolveException( request, response, null, accessDeniedException );
        request.setAttribute( WebAttributes.ACCESS_DENIED_403, accessDeniedException );

        // call super
        super.handle( request, response, accessDeniedException );
    }
}
