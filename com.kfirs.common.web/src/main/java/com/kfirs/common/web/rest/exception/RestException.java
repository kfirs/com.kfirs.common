/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.exception;

import com.kfirs.common.util.lang.NotNull;
import java.util.HashMap;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @author arik
 * @on 2/14/17.
 */
public class RestException extends RuntimeException
{
    @NotNull
    private final HttpStatus status;

    @NotNull
    private final HttpHeaders headers = new HttpHeaders();

    @NotNull
    private final BindingResult bindErrors;

    public RestException( @NotNull HttpStatus status )
    {
        this( status, new MapBindingResult( new HashMap<>(), "request" ) );
    }

    public RestException( @NotNull HttpStatus status, @NotNull BindingResult bindingResult )
    {
        this.status = status;
        this.bindErrors = bindingResult;
    }

    public RestException( @NotNull Throwable cause, @NotNull HttpStatus status )
    {
        this( cause, status, new MapBindingResult( new HashMap<>(), "request" ) );
    }

    public RestException( @NotNull Throwable cause, @NotNull HttpStatus status, @NotNull BindingResult bindingResult )
    {
        super( cause );
        this.status = status;
        this.bindErrors = bindingResult;
    }

    @NotNull
    public RestException copyBindingErrors( @NotNull BindingResult bindingResult )
    {
        bindingResult.getGlobalErrors().forEach( this.bindErrors::addError );
        bindingResult.getFieldErrors()
                     .forEach( fieldError -> this.bindErrors.addError(
                             new FieldError( "request.values",
                                             isBlank( fieldError.getObjectName() )
                                             ? fieldError.getField()
                                             : fieldError.getObjectName() + "." + fieldError.getField(),
                                             fieldError.getRejectedValue(),
                                             fieldError.isBindingFailure(),
                                             fieldError.getCodes(),
                                             fieldError.getArguments(),
                                             fieldError.getDefaultMessage() ) ) );
        return this;
    }

    @NotNull
    public RestException withObjectError( @NotNull String errorCode, @NotNull Object... args )
    {
        this.bindErrors.reject( errorCode, args, null );
        return this;
    }

    @NotNull
    public RestException withFieldError( @NotNull String field, @NotNull String errorCode, @NotNull Object... args )
    {
        this.bindErrors.rejectValue( field, errorCode, args, null );
        return this;
    }

    @NotNull
    public HttpStatus getHttpStatus()
    {
        return this.status;
    }

    @NotNull
    public HttpHeaders getHttpHeaders()
    {
        return this.headers;
    }

    @NotNull
    public BindingResult getBindingResult()
    {
        return this.bindErrors;
    }

    @Override
    public String getMessage()
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder( this )
                .append( "status", status )
                .append( "headers", headers )
                .append( "bindErrors", bindErrors )
                .toString();
    }
}
