/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.rest.exception.CommonExceptionsAdvisor;
import com.kfirs.common.web.rest.exception.SpringMvcExceptionsAdvisor;
import com.kfirs.common.web.rest.exception.SpringSecurityExceptionsAdvisor;
import com.kfirs.common.web.rest.handler.RestHandler;
import com.kfirs.common.web.trace.TraceManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

/**
 * @author arik
 * @on 5/11/17.
 */
@Configuration
@ConditionalOnProperty( prefix = "kfirs.web.rest", name = "enabled", havingValue = "true", matchIfMissing = true )
public class RestConfig
{
    @Bean
    public RestProperties restProperties()
    {
        return new RestProperties();
    }

    @Bean
    public CommonExceptionsAdvisor commonExceptionsAdvisor()
    {
        return new CommonExceptionsAdvisor();
    }

    @Bean
    public SpringMvcExceptionsAdvisor springMvcExceptionsAdvisor()
    {
        return new SpringMvcExceptionsAdvisor();
    }

    @Bean
    public SpringSecurityExceptionsAdvisor springSecurityExceptionsAdvisor()
    {
        return new SpringSecurityExceptionsAdvisor();
    }

    @Bean
    public RestHandler restController( @NotNull ApplicationContext applicationContext,
                                       @NotNull ConversionService conversionService,
                                       @NotNull RequestMappingHandlerAdapter requestMappingHandlerAdapter,
                                       @NotNull RestProperties restProperties,
                                       @NotNull TraceManager traceManager )
    {
        return new RestHandler( applicationContext,
                                conversionService,
                                requestMappingHandlerAdapter,
                                restProperties,
                                traceManager );
    }
}
