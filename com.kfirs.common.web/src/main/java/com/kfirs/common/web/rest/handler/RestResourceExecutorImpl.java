/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest.handler;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.rest.RestResource;
import com.kfirs.common.web.rest.RestResourceExecutor;
import com.kfirs.common.web.rest.RestResponse;
import com.kfirs.common.web.rest.annotation.*;
import com.kfirs.common.web.rest.exception.RestExceptions;
import com.kfirs.common.web.trace.TraceManager;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static com.kfirs.common.web.error.DefaultErrorController.X_ERROR_HEADER_NAME;
import static java.util.stream.Collectors.toList;
import static org.springframework.core.annotation.AnnotatedElementUtils.findMergedAnnotationAttributes;
import static org.springframework.util.ReflectionUtils.getUniqueDeclaredMethods;

/**
 * @author arik
 * @on 5/26/17.
 */
public class RestResourceExecutorImpl implements RestResourceExecutor
{
    @NotNull
    private final TraceManager traceManager;

    @NotNull
    private final HttpMethodInvoker httpMethodInvoker;

    @NotNull
    private final HttpConditionEvaluator httpConditionEvaluator;

    @NotNull
    private final HttpServletRequest request;

    @NotNull
    private final HttpServletResponse response;

    public RestResourceExecutorImpl( @NotNull TraceManager traceManager,
                                     @NotNull HttpMethodInvoker httpMethodInvoker,
                                     @NotNull HttpConditionEvaluator httpConditionEvaluator,
                                     @NotNull HttpServletRequest request,
                                     @NotNull HttpServletResponse response )
    {
        this.traceManager = traceManager;
        this.httpMethodInvoker = httpMethodInvoker;
        this.httpConditionEvaluator = httpConditionEvaluator;
        this.request = request;
        this.response = response;
    }

    @Nullable
    @Override
    public Object execute( @NotNull RestResource restResource ) throws Exception
    {
        // resolve HTTP method, it's associated annotation (@Get, @Post, etc) and the REST resource details
        HttpMethod httpMethod = HttpMethod.resolve( this.request.getMethod().toUpperCase() );
        List<Object> resourceChain = restResource.getResourceChain();
        Object resource = restResource.getResource();

        // build trace information, if this is a traced request
        addTraceValue( "resourceChain", resourceChain.stream().map( Object::toString ).collect( toList() ) );
        addTraceValue( "resource", resource.toString() );

        // if this is an OPTIONS request, we won't actually invoke any of the resource's handler methods; we'll just
        // return HTTP 200 with the "Allow" header specifying which HTTP methods are supported by this resource
        if( httpMethod == HttpMethod.OPTIONS )
        {
            Set<HttpMethod> allowedHttpMethods = getAllowedHttpMethods( this.request, resource );
            return ResponseEntity.ok()
                                 .allow( allowedHttpMethods.toArray( new HttpMethod[ 0 ] ) )
                                 .build();
        }

        // iterate the handler methods for this HTTP method, until we find one that supports the current request's
        // HTTP method and is also unconditional or its condition evaluates to "true"; if no such handler method is
        // found, we'll send back HTTP 405 with an "Allow" header specifying all HTTP methods the resource supports
        Class<? extends Annotation> httpAnn = getHttpAnnotation( httpMethod );
        for( Method handlerMethod : getUniqueDeclaredMethods( resource.getClass() ) )
        {
            AnnotationAttributes attrs = findMergedAnnotationAttributes( handlerMethod,
                                                                         httpAnn,
                                                                         false,
                                                                         false );
            if( attrs == null )
            {
                // this method does not support this HTTP method
                continue;
            }

            String conditional = attrs.getString( "conditional" );

            // build trace information, if this is a traced request
            addTraceValue( "condition", conditional );

            // evaluate condition
            if( this.httpConditionEvaluator.evaluate( this.request, resource, conditional ) )
            {
                // build trace information, if this is a traced request
                addTraceValue( "method", handlerMethod.toGenericString() );

                // we don't allow requesting the @PathToken for handler methods (just for @Path methods)
                this.request.removeAttribute( RestHandler.PATH_TOKEN_ATTRIBUTE_NAME );

                // invoke the method and build an HTTP response entity from it
                Object returnValue = this.httpMethodInvoker.invokeMethod( resource, handlerMethod, this.request, this.response );

                // build trace information, if this is a traced request
                addTraceValue( "returnValue", returnValue );

                // wrap to an HTTP response entity
                return buildResponseEntity( returnValue );
            }
        }

        // if we got this far, it means no resource supported this request's HTTP method; therefor, send back HTTP-405
        // (method-not-allowed) with all the HTTP methods that the matched resources *do* support.
        throw RestExceptions.methodNotAllowed( getAllowedHttpMethods( request, resource ) );
    }

    @NotNull
    private Class<? extends Annotation> getHttpAnnotation( @NotNull HttpMethod httpMethod )
    {
        switch( httpMethod )
        {
            case GET:
            case HEAD:
                return Get.class;
            case POST:
                return Post.class;
            case PUT:
                return Put.class;
            case PATCH:
                return Patch.class;
            case DELETE:
                return Delete.class;
            default:
                throw RestExceptions.notImplemented();
        }
    }

    @NotNull
    private Set<HttpMethod> getAllowedHttpMethods( @NotNull HttpServletRequest request, @NotNull Object resource )
            throws Exception
    {
        Set<HttpMethod> allowedHttpMethods = new LinkedHashSet<>();
        allowedHttpMethods.add( HttpMethod.OPTIONS );

        for( Method method : resource.getClass().getMethods() )
        {
            Get get = method.getAnnotation( Get.class );
            if( get != null && this.httpConditionEvaluator.evaluate( request, resource, get.conditional() ) )
            {
                allowedHttpMethods.add( HttpMethod.GET );
                allowedHttpMethods.add( HttpMethod.HEAD );
            }

            Post post = method.getAnnotation( Post.class );
            if( post != null && this.httpConditionEvaluator.evaluate( request, resource, post.conditional() ) )
            {
                allowedHttpMethods.add( HttpMethod.POST );
            }

            Put put = method.getAnnotation( Put.class );
            if( put != null && this.httpConditionEvaluator.evaluate( request, resource, put.conditional() ) )
            {
                allowedHttpMethods.add( HttpMethod.PUT );
            }

            Patch patch = method.getAnnotation( Patch.class );
            if( patch != null && this.httpConditionEvaluator.evaluate( request, resource, patch.conditional() ) )
            {
                allowedHttpMethods.add( HttpMethod.PATCH );
            }

            Delete delete = method.getAnnotation( Delete.class );
            if( delete != null && this.httpConditionEvaluator.evaluate( request, resource, delete.conditional() ) )
            {
                allowedHttpMethods.add( HttpMethod.DELETE );
            }
        }
        return allowedHttpMethods;
    }

    @NotNull
    private ResponseEntity<?> buildResponseEntity( @Nullable Object returnValue )
    {
        if( returnValue == null )
        {
            return ResponseEntity.noContent().build();
        }
        else if( returnValue instanceof RestResponse )
        {
            RestResponse<?> restResponse = ( RestResponse<?> ) returnValue;
            ResponseEntity.BodyBuilder response = ResponseEntity.ok();

            // apply all errors from the REST exception as HTTP response headers
            restResponse.getErrors()
                        .forEach( error -> {
                            String path = error.getPath();
                            String code = error.getCode();
                            String message = error.getMessage();
                            response.header( X_ERROR_HEADER_NAME, path + ":" + code + ":" + message );
                        } );

            // and apply the REST response's resource as the response entity's payload/body
            return response.body( restResponse.getResource() );
        }
        else if( returnValue instanceof ResponseEntity )
        {
            ResponseEntity<?> originalEntity = ( ResponseEntity<?> ) returnValue;

            // build a new response entity with the original response entity's status code and headers
            ResponseEntity.BodyBuilder newEntity = ResponseEntity.status( originalEntity.getStatusCode() )
                                                                 .headers( originalEntity.getHeaders() );

            // if the response entity's body is a RestResponse itself, translate it to our new response; otherwise, set
            // it as the new response entity's body, as is
            Object originalBody = originalEntity.getBody();
            if( originalBody instanceof RestResponse )
            {
                RestResponse<?> restResponse = ( RestResponse<?> ) originalBody;

                // apply all errors from the REST exception as HTTP response headers
                restResponse.getErrors()
                            .forEach( error -> {
                                String path = error.getPath();
                                String code = error.getCode();
                                String message = error.getMessage();
                                newEntity.header( X_ERROR_HEADER_NAME, path + ":" + code + ":" + message );
                            } );

                // and apply the REST response's resource as the response entity's payload/body
                return newEntity.body( restResponse.getResource() );
            }
            else
            {
                return newEntity.body( originalBody );
            }
        }
        else
        {
            return ResponseEntity.ok().body( returnValue );
        }
    }

    private void addTraceValue( @NotNull String key, @Nullable Object value )
    {
        if( this.traceManager.isTraceRequest() )
        {
            this.traceManager.addTraceValue( key, value );
        }
    }
}
