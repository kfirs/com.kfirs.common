/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import com.kfirs.common.util.lang.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author arik
 * @on 4/20/17.
 */
@ConfigurationProperties( "kfirs.web.trace" )
public class TraceProperties
{
    public enum TraceSendingStrategy
    {
        ALWAYS,
        NEVER,
        ON_TRACE_PARAMETER
    }

    @NotNull
    private TraceSendingStrategy traceSendingStrategy = TraceSendingStrategy.ON_TRACE_PARAMETER;

    private boolean logTrace = false;

    private String[] traceIpWhitelist = new String[] { "127.0.0.1", "::1", "0:0:0:0:0:0:0:1" };

    @NotNull
    public TraceSendingStrategy getTraceSendingStrategy()
    {
        return traceSendingStrategy;
    }

    public void setTraceSendingStrategy( @NotNull TraceSendingStrategy traceSendingStrategy )
    {
        this.traceSendingStrategy = traceSendingStrategy;
    }

    public boolean isLogTrace()
    {
        return logTrace;
    }

    public void setLogTrace( boolean logTrace )
    {
        this.logTrace = logTrace;
    }

    public String[] getTraceIpWhitelist()
    {
        return traceIpWhitelist;
    }

    public void setTraceIpWhitelist( String[] traceIpWhitelist )
    {
        this.traceIpWhitelist = traceIpWhitelist;
    }
}
