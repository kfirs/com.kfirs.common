/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import org.slf4j.MDC;

import static java.util.Objects.requireNonNull;

/**
 * @author arik
 * @on 5/14/17.
 */
public class LogbackTraceAppender extends AppenderBase<ILoggingEvent>
{
    @NotNull
    public static final String TRACE_MANAGER_LOCATOR_KEY = TraceManager.class.getName();

    @NotNull
    public static final String MDC_TRACE_KEY = TraceManager.class.getName() + "#mdc";

    @Nullable
    private PatternLayout layout;

    @Override
    public void start()
    {
        if( this.layout == null )
        {
            this.layout = new PatternLayout();
            this.layout.setPattern( "%date{HH:mm:ss.SSS} %5p --- [%t] %-40.40logger{39} : %m%n-%wEx" );
            this.layout.setContext( getContext() );
            this.layout.start();
        }
        super.start();
    }

    @Override
    protected void append( @NotNull ILoggingEvent eventObject )
    {
        if( !isStarted() )
        {
            return;
        }

        if( MDC.get( MDC_TRACE_KEY ) != null )
        {
            Object traceManagerLocatorObject = getContext().getObject( TRACE_MANAGER_LOCATOR_KEY );
            if( traceManagerLocatorObject instanceof TraceManagerLocator )
            {
                TraceManagerLocator traceManagerLocator = ( TraceManagerLocator ) traceManagerLocatorObject;
                TraceManager traceManager = traceManagerLocator.getTraceManager();
                if( traceManager != null )
                {
                    String msg = requireNonNull( this.layout ).doLayout( eventObject );
                    if( msg != null )
                    {
                        traceManager.trace( msg );
                    }
                }
            }
        }
    }

    @Nullable
    public PatternLayout getLayout()
    {
        return this.layout;
    }

    public void setLayout( @Nullable PatternLayout layout )
    {
        this.layout = layout;
    }
}
