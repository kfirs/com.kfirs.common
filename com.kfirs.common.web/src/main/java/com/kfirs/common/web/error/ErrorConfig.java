/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.web.error;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.trace.TraceManager;
import java.util.List;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorViewResolver;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author arik
 * @on 6/28/17.
 */
@Configuration
public class ErrorConfig
{
    @Bean
    public ErrorProperties kfirsErrorProperties()
    {
        return new ErrorProperties();
    }

    @Bean
    @ConditionalOnProperty( prefix = "kfirs.web.error", name = "enabled", havingValue = "true", matchIfMissing = true )
    public DefaultErrorController defaultErrorController( @NotNull ErrorAttributes errorAttributes,
                                                          @NotNull List<ErrorViewResolver> errorViewResolvers,
                                                          @NotNull MessageSource messageSource,
                                                          @NotNull ServerProperties serverProperties,
                                                          @NotNull TraceManager traceManager )
    {
        return new DefaultErrorController( errorAttributes, errorViewResolvers, messageSource, serverProperties, traceManager );
    }
}
