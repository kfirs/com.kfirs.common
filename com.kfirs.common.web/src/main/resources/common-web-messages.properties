#
# Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use
# this file except in compliance with the License. You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under
# the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either expressed or implied.
#
# See the License for the specific language governing permissions and limitations under the License.
#
AccountExpired=This account has expired
AccountCredentialsExpired=Credentials for this account have expired
AccountDisabled=This account is disabled
AccountLocked=This account is locked
AuthenticationFailed=Internal error occurred during authentication
BadCredentials=Incorrect credentials provided
InsufficientAuthentication=Stronger authentication required
RememberMeAuthenticationFailed=Remember-me authentication failed
TooManySessions=Too many concurrent logins
Unauthorized=Authentication required
UnknownAccount=Unknown account

# HTTP 403 errors
AuthorizationFailed=Failed granting authorization
CsrfValidationFailed=CSRF validation failed
PermissionDenied=Permission denied

# HTTP 4xx errors
ContentUnreadable=Failed reading request content
DataIntegrityError=Data cannot be updated, as it will cause constraint violations
ExpectationFailed=Expectation failed for header ''{0}''
IdUpdateNotAllowed=Updating entity ID is not allowed
IllegalValue=Illegal value
MethodNotAllowed=HTTP method not allowed for this resource. Allowed methods: {0}
PreconditionFailed=Precondition failed for header ''{0}''
RequestHeaderMissing=Required header ''{0}'' is missing
RequestParameterMissing=Required parameter ''{0}'' is missing
RequestPartMissing=Request part ''{0}'' is missing
ResourceIsStale=Resource has already been updated in another transaction, please refresh and try again
ResourceNotFound=Resource not found
SortNotAllowed=Sorting is not allowed for this resource
UnacceptableMediaType=Unable to render response in an acceptable media type
UnsupportedHeader=Header ''{0}'' is not allowed for this resource & method
UnsupportedMediaType=Request content was sent in an unsupported format (''{0}'')
VersionUpdateNotAllowed=Updating entity versions is not allowed (please use conditional "If-*-Match" headers instead)

# HTTP 5xx errors
InternalServerError=An internal error has occurred
NotAudited=Resource is not audited
NotIdentifiable=Resource is not an identifiable resource
NotImplemented=HTTP method {0} is not implemented for ''{1}''
NotVersioned=Resource is not a versioned resource
ServiceUnavailable=Service is unavailable
