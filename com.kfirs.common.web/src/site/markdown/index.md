## Welcome to the KFIRS Common project

This project provides useful components to be used throughout the rest
of the KFIRS projects. Utilities such as file-watching, classpath
handling, I/O, testing, etc are all provided here.

Effort is done to ensure that existing utilities provided by other
projects (such as [Apache Commons](http://commons.apache.org), 
[Spring](http://spring.io), etc) is not duplicated and are used wherever
possible. The goal is to extend these with additional functionality, or,
when dimmed useful, provide simpler APIs.

### Site map ###

The Common project is composed of several modules (see menu on the left).
Each module has some documentation about its features and use-cases, and
live Javadocs for your convenience. In addition, you can use the
[aggregated Javadocs](apidocs/index.html) to view the Javadocs for all
modules together.
