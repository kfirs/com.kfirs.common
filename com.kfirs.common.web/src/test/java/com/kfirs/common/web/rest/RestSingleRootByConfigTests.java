/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest;

import com.kfirs.common.web.WebConfig;
import java.net.URI;
import java.util.stream.Stream;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.RequestEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import static com.kfirs.common.web.rest.assertions.RestAssertions.assertThatResponse;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

/**
 * @author arik
 * @on 5/12/17.
 */
@SpringBootTest( webEnvironment = RANDOM_PORT, properties = "kfirs.web.rest.rootBean=root" )
@ActiveProfiles( "test" )
public class RestSingleRootByConfigTests
{
    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private TestRestTemplate rest;

    @Test
    public void testSingleRootByConfigDetected()
    {
        Stream.of( GET, HEAD, POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> {
                    System.out.println( "Using " + httpMethod + ": " );
                    assertThatResponse(
                            this.rest.exchange(
                                    RequestEntity.method( httpMethod, URI.create( "/?_trace" ) )
                                                 .accept( APPLICATION_JSON_UTF8 )
                                                 .build(),
                                    Object.class ) )
                            .respondedWithStatusCode( METHOD_NOT_ALLOWED );
                } );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.options( URI.create( "/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        Object.class ) )
                .respondedWithStatusCode( OK )
                .respondedWithAllowHeader( OPTIONS );
    }

    @Configuration
    @EnableAutoConfiguration
    @Import( WebConfig.class )
    public static class Config
    {
        @Bean
        public Root root()
        {
            return new Root();
        }
    }

    public static class Root
    {
    }
}
