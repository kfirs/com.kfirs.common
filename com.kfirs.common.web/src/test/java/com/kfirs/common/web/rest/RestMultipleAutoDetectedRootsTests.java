/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest;

import com.kfirs.common.web.WebConfig;
import com.kfirs.common.web.rest.annotation.Get;
import com.kfirs.common.web.rest.annotation.Path;
import com.kfirs.common.web.rest.annotation.Put;
import com.kfirs.common.web.rest.annotation.RestRoot;
import java.net.URI;
import java.util.stream.Stream;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.http.RequestEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import static com.kfirs.common.web.rest.assertions.RestAssertions.assertThatResponse;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

/**
 * @author arik
 * @on 5/12/17.
 */
@SpringBootTest( webEnvironment = RANDOM_PORT )
@ActiveProfiles( "test" )
public class RestMultipleAutoDetectedRootsTests
{
    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private TestRestTemplate rest;

    @Test
    public void testMultipleRoot()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.options( URI.create( "/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        Object.class ) )
                .respondedWithStatusCode( OK )
                .respondedWithAllowHeader( OPTIONS, GET, HEAD );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasNoErrors()
                .hasBodyEqualTo( "Hello GET of: /" );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.put( URI.create( "/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( METHOD_NOT_ALLOWED )
                .respondedWithAllowHeader( OPTIONS, GET, HEAD );

        Stream.of( POST, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( METHOD_NOT_ALLOWED ) );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/root1?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasNoErrors()
                .hasBodyEqualTo( "Hello GET of: /root1" );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/root2?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasNoErrors()
                .hasBodyEqualTo( "Hello GET of: /root2" );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/root3?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( NOT_FOUND );
    }

    @Configuration
    @EnableAutoConfiguration
    @Import( WebConfig.class )
    public static class Config
    {
        @Bean
        public Root1 root1()
        {
            return new Root1();
        }

        @Bean
        public Root2 root2()
        {
            return new Root2();
        }
    }

    @RestRoot
    @Order( 1 )
    public static class Root1
    {
        @Get
        public String get()
        {
            return "Hello GET of: /";
        }

        @Path( path = "root1" )
        public TestResource getRoot1()
        {
            return new TestResource( "/root1" );
        }

        @Override
        public String toString()
        {
            return new ToStringBuilder( this ).toString();
        }
    }

    @RestRoot
    @Order( 2 )
    public static class Root2
    {
        @Put
        public String put()
        {
            return "Hello PUT of: /";
        }

        @Path( path = "root2" )
        public TestResource getRoot2()
        {
            return new TestResource( "/root2" );
        }

        @Override
        public String toString()
        {
            return new ToStringBuilder( this ).toString();
        }
    }

    public static class TestResource
    {
        private final String value;

        public TestResource( String value )
        {
            this.value = value;
        }

        @Get
        public String get()
        {
            return "Hello GET of: " + this.value;
        }

        @Override
        public String toString()
        {
            return new ToStringBuilder( this )
                    .append( "value", this.value )
                    .toString();
        }
    }
}
