/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.trace;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.web.trace.TraceProperties.TraceSendingStrategy;
import java.util.Arrays;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.kfirs.common.web.trace.TraceProperties.TraceSendingStrategy.ALWAYS;
import static com.kfirs.common.web.trace.TraceProperties.TraceSendingStrategy.NEVER;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author arik
 * @on 4/25/17.
 */
public class TraceManagerTest
{
    @Test
    public void testThatTraceIsDeniedOnNeverStrategy()
    {
        assertThat( createTraceMgr( "127.0.0.1", NEVER, "::1", "1.1.1.1", "2.2.2.2" ).isTraceRequest() ).isFalse();
        assertThat( createTraceMgr( "127.0.0.1", NEVER, "127.0.0.1" ).isTraceRequest() ).isFalse();
        assertThat( createTraceMgr( "127.0.0.1", NEVER ).isTraceRequest() ).isFalse();
    }

    @Test
    public void testThatTraceIsAllowedOnAlwaysStrategy()
    {
        assertThat( createTraceMgr( "127.0.0.1", ALWAYS, "::1", "1.1.1.1", "2.2.2.2" ).isTraceRequest() ).isFalse();
        assertThat( createTraceMgr( "127.0.0.1", ALWAYS, "127.0.0.1" ).isTraceRequest() ).isTrue();
    }

    @Test
    public void testThatTraceIsDeniedOnEmptyWhitelist()
    {
        Arrays.asList( "127.0.0.1", "::1", "3.0.0.0" ).forEach(
                address -> Stream.of( TraceSendingStrategy.values() ).forEach(
                        strategy -> assertThat( createTraceMgr( address, strategy ).isTraceRequest() ).isFalse() ) );
    }

    @Test
    public void testThatTraceIsDeniedOnNonEmptyWhitelist()
    {
        Arrays.asList( "127.0.0.1", "::1", "3.0.0.0" ).forEach(
                address -> Stream.of( TraceSendingStrategy.values() ).forEach(
                        strategy -> assertThat( createTraceMgr( address,
                                                                strategy,
                                                                "1.1.1.1", "2.2.2.2" )
                                                        .isTraceRequest() ).isFalse() ) );
    }

    @Test
    public void testThatTraceIsDeniedOnNonEmptyWhitelistWithABlankEntry()
    {
        Arrays.asList( "", "127.0.0.1", "::1", "3.0.0.0" ).forEach(
                address -> Stream.of( TraceSendingStrategy.values() ).forEach(
                        strategy -> assertThat( createTraceMgr( address,
                                                                strategy,
                                                                "1.1.1.1", "2.2.2.2" )
                                                        .isTraceRequest() ).isFalse() ) );
    }

    @Test
    public void testThatTraceIsAllowedForWhitelistAddressAndAlwaysStrategy()
    {
        TraceManager traceMgr = createTraceMgr( "127.0.0.1", ALWAYS, "1.1.1.1", "2.2.2.2" );
        assertThat( traceMgr.isTraceRequest() ).isFalse();
    }

    @NotNull
    private TraceManager createTraceMgr( @NotNull String remoteAddr,
                                         @NotNull TraceSendingStrategy strategy,
                                         @NotNull String... whitelist )
    {
        return new TraceManager( createTraceProperties( strategy, whitelist ), new ObjectProvider<HttpServletRequest>()
        {
            @Override
            public HttpServletRequest getObject( Object... args ) throws BeansException
            {
                return createRequest( remoteAddr );
            }

            @Override
            public HttpServletRequest getIfAvailable() throws BeansException
            {
                return createRequest( remoteAddr );
            }

            @Override
            public HttpServletRequest getIfUnique() throws BeansException
            {
                return createRequest( remoteAddr );
            }

            @Override
            public HttpServletRequest getObject() throws BeansException
            {
                return createRequest( remoteAddr );
            }
        } );
    }

    @NotNull
    private TraceProperties createTraceProperties( @NotNull TraceSendingStrategy strategy,
                                                   @NotNull String... whitelist )
    {
        TraceProperties props = new TraceProperties();
        props.setTraceIpWhitelist( whitelist );
        props.setTraceSendingStrategy( strategy );
        return props;
    }

    @NotNull
    private HttpServletRequest createRequest( @NotNull String remoteAddr )
    {
        MockServletContext context = new MockServletContext();
        return MockMvcRequestBuilders.get( "http://localhost" )
                                     .with( req ->
                                            {
                                                req.setRemoteAddr( remoteAddr );
                                                return req;
                                            } )
                                     .buildRequest( context );
    }
}
