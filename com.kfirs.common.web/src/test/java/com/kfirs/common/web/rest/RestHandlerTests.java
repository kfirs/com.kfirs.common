/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.rest;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import com.kfirs.common.web.WebConfig;
import com.kfirs.common.web.rest.annotation.Get;
import com.kfirs.common.web.rest.annotation.Path;
import com.kfirs.common.web.rest.annotation.PathToken;
import com.kfirs.common.web.rest.annotation.RestRoot;
import com.kfirs.common.web.rest.exception.RestError;
import com.kfirs.common.web.rest.exception.RestException;
import com.kfirs.common.web.trace.TraceManager;
import java.net.URI;
import java.util.stream.Stream;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import static com.kfirs.common.web.rest.assertions.RestAssertions.assertThatResponse;
import static java.util.Collections.singletonList;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

/**
 * @author arik
 * @on 5/12/17.
 */
@SpringBootTest( webEnvironment = RANDOM_PORT )
@ActiveProfiles( "test" )
public class RestHandlerTests
{
    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private TestRestTemplate rest;

    @Test
    public void testRoot()
    {
        Stream.of( GET, HEAD, POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( METHOD_NOT_ALLOWED ) );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.options( URI.create( "/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        Object.class ) )
                .respondedWithStatusCode( OK )
                .respondedWithAllowHeader( OPTIONS );
    }

    @Test
    public void testImplicitlNestedPath()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.options( URI.create( "/implicitlyNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        Object.class ) )
                .respondedWithStatusCode( OK )
                .respondedWithAllowHeader( OPTIONS, GET, HEAD );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/implicitlyNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasBodyEqualTo( "Hello GET" );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.head( URI.create( "/implicitlyNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasNoBody();

        Stream.of( POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/implicitlyNested1/?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( METHOD_NOT_ALLOWED ) );
    }

    @Test
    public void testImplicitIdenticalNestedPath()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.options( URI.create( "/implicitIdenticalNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        Object.class ) )
                .respondedWithStatusCode( OK )
                .respondedWithAllowHeader( OPTIONS, GET, HEAD );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/implicitIdenticalNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasBodyEqualTo( "Hello GET" );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.head( URI.create( "/implicitIdenticalNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasNoBody();

        Stream.of( POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/implicitIdenticalNested1/?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( METHOD_NOT_ALLOWED ) );
    }

    @Test
    public void testExplicitlNestedPath()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.options( URI.create( "/explicitlyNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        Object.class ) )
                .respondedWithStatusCode( OK )
                .respondedWithAllowHeader( OPTIONS, GET, HEAD );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/explicitlyNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasBodyEqualTo( "Hello GET" );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.head( URI.create( "/explicitlyNested1/?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasNoBody();

        Stream.of( POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/explicitlyNested1/?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( METHOD_NOT_ALLOWED ) );
    }

    @Test
    public void testRegexInnerNestedPath()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.options( URI.create( "/explicitlyNested1/1?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        Object.class ) )
                .respondedWithStatusCode( OK )
                .respondedWithAllowHeader( OPTIONS, GET, HEAD );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/explicitlyNested1/2?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasBodyEqualTo( "Hello GET of: 2" );

        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.head( URI.create( "/explicitlyNested1/3?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasNoBody();

        Stream.of( POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/explicitlyNested1/4?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( METHOD_NOT_ALLOWED ) );
    }

    @Test
    public void testUnknownPath()
    {
        Stream.of( OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/unknown?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( NOT_FOUND ) );

        Stream.of( OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/implicitlyNested1/unknown?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( NOT_FOUND ) );

        Stream.of( OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE ).forEach(
                httpMethod -> assertThatResponse(
                        this.rest.exchange(
                                RequestEntity.method( httpMethod, URI.create( "/explicitlyNested1/unknown?_trace" ) )
                                             .accept( APPLICATION_JSON_UTF8 )
                                             .build(),
                                Object.class ) )
                        .respondedWithStatusCode( NOT_FOUND ) );
    }

    @Test
    public void testNullReturnValue()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/nullReturnValue?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( NO_CONTENT )
                .hasNoBody();
    }

    @Test
    public void testRestResponseReturnValue()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/restResponseReturnValue?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasErrorHeader( "testPath", "testCode", "testMessage" )
                .hasBodyEqualTo( "Hello GET of RestResponse" )
                .hasTraceEntry( this.rest.getRestTemplate(), "k1", "v1" );
    }

    @Test
    public void testResponseEntityReturnValue()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/responseEntityReturnValue?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( OK )
                .hasBodyEqualTo( "Hello GET of ResponseEntity" );
    }

    @Test
    public void testUnexpectedExceptionFromHandler()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/throwsIllegalStateExceptionResource?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( INTERNAL_SERVER_ERROR )
                .hasErrorHeader( "request", "InternalServerError" )
                .hasNoBody();
    }

    @Test
    public void testRestExceptionFromHandler()
    {
        assertThatResponse(
                this.rest.exchange(
                        RequestEntity.get( URI.create( "/throwsBadGatewayExceptionResource?_trace" ) )
                                     .accept( APPLICATION_JSON_UTF8 )
                                     .build(),
                        String.class ) )
                .respondedWithStatusCode( BAD_GATEWAY )
                .hasErrorHeader( "request", "badGateway" )
                .hasNoBody();
    }

    @Configuration
    @EnableAutoConfiguration
    @Import( WebConfig.class )
    public static class Config
    {
        @Bean
        public Root root( @NotNull TraceManager traceManager )
        {
            return new Root( traceManager );
        }
    }

    @RestRoot
    public static class Root
    {
        @NotNull
        private final TraceManager traceManager;

        public Root( @NotNull TraceManager traceManager )
        {
            this.traceManager = traceManager;
        }

        @Path
        public Nested1 getImplicitlyNested1()
        {
            return new Nested1();
        }

        @Path
        public Nested1 implicitIdenticalNested1()
        {
            return new Nested1();
        }

        @Path( path = "explicitlyNested1" )
        public Nested1 getExplicitlyNested1OnlyByAnnotationPath()
        {
            return new Nested1();
        }

        @Path
        public GetReturnsNullResource nullReturnValue()
        {
            return new GetReturnsNullResource();
        }

        @Path
        public GetReturnsRestResponseResource restResponseReturnValue()
        {
            return new GetReturnsRestResponseResource( this.traceManager );
        }

        @Path
        public GetReturnsResponseEntityResource responseEntityReturnValue()
        {
            return new GetReturnsResponseEntityResource();
        }

        @Path
        public GetThrowsIllegalStateExceptionResource throwsIllegalStateExceptionResource()
        {
            return new GetThrowsIllegalStateExceptionResource();
        }

        @Path
        public GetThrowsBadGatewayExceptionResource throwsBadGatewayExceptionResource()
        {
            return new GetThrowsBadGatewayExceptionResource();
        }
    }

    public static class Nested1
    {
        @Get
        public String get()
        {
            return "Hello GET";
        }

        @Path( regex = "\\d+" )
        public InnerNested getNested1ByRegex( @PathToken int value )
        {
            return new InnerNested( value );
        }
    }

    public static class InnerNested
    {
        private final int value;

        public InnerNested( int value )
        {
            this.value = value;
        }

        @Get
        public String get()
        {
            return "Hello GET of: " + this.value;
        }
    }

    public static class GetReturnsNullResource
    {
        @Nullable
        @Get
        public String get()
        {
            return null;
        }
    }

    public static class GetReturnsRestResponseResource
    {
        @NotNull
        private final TraceManager traceManager;

        public GetReturnsRestResponseResource( @NotNull TraceManager traceManager )
        {
            this.traceManager = traceManager;
        }

        @Nullable
        @Get
        public RestResponse<String> get()
        {
            this.traceManager.addTraceValue( "k1", "v1" );
            return new RestResponse<>( singletonList( new RestError( "testPath", "testCode", "testMessage" ) ),
                                       "Hello GET of RestResponse" );
        }
    }

    public static class GetReturnsResponseEntityResource
    {
        @Nullable
        @Get
        public ResponseEntity<String> get()
        {
            return ResponseEntity.ok( "Hello GET of ResponseEntity" );
        }
    }

    public static class GetThrowsIllegalStateExceptionResource
    {
        @Nullable
        @Get
        public String get()
        {
            throw new IllegalStateException( "alarm alarm" );
        }
    }

    public static class GetThrowsBadGatewayExceptionResource
    {
        @Nullable
        @Get
        public String get()
        {
            throw new RestException( BAD_GATEWAY ).withObjectError( "badGateway" );
        }
    }
}
