/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.web.requestid;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author arik
 * @on 2/15/17.
 */
public class ThreadBoundRequestIdManagerTest
{
    @Test
    public void testUniqueRequestId() throws InterruptedException
    {
        final ThreadBoundRequestIdManager manager = new ThreadBoundRequestIdManager();
        final Set<UUID> uuids = new HashSet<>();

        ExecutorService executor = Executors.newFixedThreadPool( 5 );
        IntStream.range( 0, 1000 )
                 .mapToObj( i -> ( Runnable ) () -> uuids.add( manager.getOrGenerateUuidForCurrentThread() ) )
                 .forEach( executor::submit );

        Thread.sleep( 1000 );

        executor.shutdown();
        executor.awaitTermination( 10, TimeUnit.SECONDS );

        // even though we submit 1000 tasks, they only run on 5 threads - so there should only be 5 UUIDs
        assertThat( uuids ).hasSize( 5 );
    }

    @Test
    public void testUniqueRequestIdRemoved() throws InterruptedException
    {
        ThreadBoundRequestIdManager manager = new ThreadBoundRequestIdManager();

        UUID uuid = manager.getOrGenerateUuidForCurrentThread();
        IntStream.range( 0, 1000 ).forEach( i -> assertThat( uuid ).isEqualTo( manager.getOrGenerateUuidForCurrentThread() ) );

        assertThat( uuid ).isEqualTo( manager.getOrGenerateUuidForCurrentThread() );
        manager.releaseUuidForCurrentThread();
        assertThat( uuid ).isNotEqualTo( manager.getOrGenerateUuidForCurrentThread() );
    }
}