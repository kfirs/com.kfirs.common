/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.matcher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.springframework.util.AntPathMatcher;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author arik
 * @on 2/1/16.
 */
public class DelegatingPathMatcherTest
{
    private final Map<String, List<String>> matchingFixtures;

    private final Map<String, List<String>> nonMatchingFixtures;

    public DelegatingPathMatcherTest()
    {
        Map<String, List<String>> m = new HashMap<>();
        m.put( "/home/arik/hello.txt", singletonList( "/home/**/*" ) );
        m.put( "/home/arik/hello.txt", asList( "/home/**/*", "!/home/arik/world.txt" ) );
        m.put( "/home/arik/hello.txt", asList( "/home/**/*", "!/home/arik/hello*", "/home/arik/hello.txt" ) );
        this.matchingFixtures = m;

        m = new HashMap<>();
        m.put( "/home/arik/hello.txt", asList( "/home/**/*", "!/home/arik/hello.txt" ) );
        m.put( "/home/arik/hello.txt", singletonList( "*.txt" ) );
        m.put( "/home/arik/hello.txt", singletonList( "**/*.txt" ) );
        this.nonMatchingFixtures = m;
    }

    @Test
    public void testMatchVarargs()
    {
        PathMatcher matcher = new DelegatingPathMatcher( new AntPathMatcher() );
        this.matchingFixtures.entrySet().forEach(
                e -> assertThat( matcher.match( e.getKey(), e.getValue().toArray( new String[ 0 ] ) ) ).isTrue() );
        this.nonMatchingFixtures.entrySet().forEach(
                e -> assertThat( matcher.match( e.getKey(), e.getValue().toArray( new String[ 0 ] ) ) ).isFalse() );
    }

    @Test
    public void testMatchList()
    {
        PathMatcher matcher = new DelegatingPathMatcher( new AntPathMatcher() );
        this.matchingFixtures.entrySet().forEach( e -> assertThat( matcher.match( e.getKey(), e.getValue() ) ).isTrue() );
        this.nonMatchingFixtures.entrySet().forEach( e -> assertThat( matcher.match( e.getKey(), e.getValue() ) ).isFalse() );
    }
}
