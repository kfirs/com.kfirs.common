/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.messages;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.context.support.ResourceBundleMessageSource;

import static java.util.Locale.ENGLISH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * @author arik
 * @on 3/26/17.
 */
public class CodeDelegatingMessageSourceTest
{
    private CodeDelegatingMessageSource msgs;

    @Before
    public void setUp() throws Exception
    {
        ResourceBundleMessageSource target = new ResourceBundleMessageSource();
        target.setBeanClassLoader( getClass().getClassLoader() );
        target.setBundleClassLoader( getClass().getClassLoader() );
        target.setAlwaysUseMessageFormat( true );
        target.setBasename( "com.kfirs.common.util.messages.messages" );
        target.setDefaultEncoding( "UTF-8" );
        this.msgs = new CodeDelegatingMessageSource( target );
    }

    @After
    public void tearDown() throws Exception
    {
        this.msgs = null;
    }

    @Test
    public void testGetMessageWithoutDefault()
    {
        assertThat( this.msgs.getMessage( "k1", null, "def1", ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( "k2", null, "def2", ENGLISH ) ).isEqualTo( "v2" );
        assertThat( this.msgs.getMessage( "k3", null, "def3", ENGLISH ) ).isEqualTo( "def3" );
        assertThat( this.msgs.getMessage( "dk1", null, "def1", ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( "dk2", null, "def2", ENGLISH ) ).isEqualTo( "v1" );
        assertThatThrownBy( () -> this.msgs.getMessage( "dk3", null, ENGLISH ) ).isInstanceOf( NoSuchMessageException.class );
    }

    @Test
    public void testGetMessageWithDefault()
    {
        assertThat( this.msgs.getMessage( "k1", null, "def1", ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( "k2", null, "def2", ENGLISH ) ).isEqualTo( "v2" );
        assertThat( this.msgs.getMessage( "k3", null, "def3", ENGLISH ) ).isEqualTo( "def3" );
        assertThat( this.msgs.getMessage( "dk1", null, "def1", ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( "dk2", null, "def2", ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( "dk3", null, "def3", ENGLISH ) ).isEqualTo( "def3" );
    }

    @Test
    public void testGetMessageFromResolvable()
    {
        assertThat( this.msgs.getMessage( new DefaultMessageSourceResolvable( new String[] {
                                                  "k1",
                                                  "k2",
                                                  "k3"
                                          }, null, "def1" ),
                                          ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( new DefaultMessageSourceResolvable( new String[] {
                                                  "k2",
                                                  "k1",
                                                  "k3"
                                          }, null, "def2" ),
                                          ENGLISH ) ).isEqualTo( "v2" );
        assertThat( this.msgs.getMessage( new DefaultMessageSourceResolvable( new String[] {
                                                  "k3",
                                                  "k1",
                                                  "k2"
                                          }, null, "def3" ),
                                          ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( new DefaultMessageSourceResolvable( new String[] {
                                                  "dk1",
                                                  "k3",
                                                  "k1",
                                                  "k2"
                                          }, null, "def1" ),
                                          ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( new DefaultMessageSourceResolvable( new String[] {
                                                  "dk2",
                                                  "k3",
                                                  "k1",
                                                  "k2"
                                          }, null, "def2" ),
                                          ENGLISH ) ).isEqualTo( "v1" );
        assertThat( this.msgs.getMessage( new DefaultMessageSourceResolvable( new String[] {
                                                  "dk3",
                                                  "k3",
                                                  "k1",
                                                  "k2"
                                          }, null, "def2" ),
                                          ENGLISH ) ).isEqualTo( "v1" );
    }
}
