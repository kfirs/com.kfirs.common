/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.test.JulToSlfRule;
import com.kfirs.common.test.WorkPathRule;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.stream.Collectors;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.loader.LaunchedURLClassLoader;

import static com.kfirs.common.util.io.path.classpath.ClasspathUtils.CLASS_LOADER_KEY;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * @author arik
 * @on 4/10/16.
 */
public class ClasspathFileSystemTest
{
    @Rule
    public JulToSlfRule julToSlfRule = new JulToSlfRule();

    @Rule
    public WorkPathRule work = new WorkPathRule();

    @Test
    public void testGetClassLoaderAndProvider() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );
        assertThat( fs.getClassLoader() ).isSameAs( classLoader );
        assertThat( fs.provider() ).isSameAs( provider );
    }

    @Test
    public void testDetectURLClassLoaderUrls() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testFile = this.work.getPath().resolve( "testDir/testFile" );
        Files.createDirectories( testFile.getParent() );
        Files.write( testFile, "value".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );

        // create classpath file-system with it
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { this.work.getPath().toUri().toURL() };
        URLClassLoader urlClassLoader = new URLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file from our URLClassLoader
        assertThat( Files.exists( fs.getPath( "/testDir/testFile" ) ) ).isTrue();
    }

    @Test
    public void testResourcesFromDiffUrlsButSamePackage() throws IOException
    {
        // prepare first file in package 'pkg'
        Path file1 = this.work.getPath().resolve( "url1/pkg/file1" );
        Files.createDirectories( file1.getParent() );
        Files.write( file1, "file1".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );

        // prepare second file in package 'pkg'
        Path file2 = this.work.getPath().resolve( "url2/pkg/file2" );
        Files.createDirectories( file2.getParent() );
        Files.write( file2, "file2".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );

        // create classpath file-system with it
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = {
                file1.getParent().getParent().toUri().toURL(),
                file2.getParent().getParent().toUri().toURL()
        };
        URLClassLoader urlClassLoader = new URLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test files from our URLClassLoader
        assertThat( Files.exists( fs.getPath( "/pkg/file1" ) ) ).isTrue();
        assertThat( Files.exists( fs.getPath( "/pkg/file2" ) ) ).isTrue();

        // ensure that 'pkg' contains both files even through they come from different URLs
        ClasspathPath pkgPath = fs.getPath( "pkg" );
        List<Path> children = Files.list( pkgPath ).collect( Collectors.toList() );
        assertThat( children.size() ).isEqualTo( 2 );
        assertThat( children.contains( fs.getPath( "/pkg/file1" ) ) ).isTrue();
        assertThat( children.contains( fs.getPath( "/pkg/file2" ) ) ).isTrue();
    }

    @Test
    public void testCloseFileSystem() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );
        assertThat( fs.isOpen() ).isTrue();
        fs.close();
        assertThat( fs.isOpen() ).isFalse();
        try
        {
            fs.isReadOnly();
            failBecauseExceptionWasNotThrown( ClosedFileSystemException.class );
        }
        catch( ClosedFileSystemException ignore )
        {
        }
    }

    @Test
    public void testIsReadOnlyIsTrue() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );
        assertThat( fs.isReadOnly() ).isTrue();
    }

    @Test
    public void testSeparatorIsSlash() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );
        assertThat( fs.getSeparator() ).isEqualTo( "/" );
    }

    @Test
    public void testGetRoots() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        List<Path> roots = fs.getRootDirectories();
        assertThat( roots.size() ).isEqualTo( 1 );
        assertThat( roots.get( 0 ) ).isEqualTo( fs.getPath( "/" ) );

        ClasspathPath root = fs.getRoot();
        assertThat( root ).isEqualTo( fs.getPath( "/" ) );

        // assert root node has less than 30 direct children. while not a real ampirical test, it's more of a sanity
        // that we didn't load up all nodes directly under the root (a bug that happened once - this is it's regression test)
        PathNode rootNode = root.getPathNode();
        assertThat( rootNode ).isNotNull();
        assertThat( rootNode.getChildren().size() ).isLessThan( 30 );
    }

    @Test
    public void testGetFileStores() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        List<FileStore> roots = fs.getFileStores();
        assertThat( roots.size() ).isEqualTo( 1 );
    }

    @Test
    public void testSupportedFileAttributeViews() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        Set<String> supportedFileAttributeViews = fs.supportedFileAttributeViews();
        assertThat( supportedFileAttributeViews.size() ).isEqualTo( 2 );
        assertThat( supportedFileAttributeViews.contains( "basic" ) ).isTrue();
        assertThat( supportedFileAttributeViews.contains( "classpath" ) ).isTrue();
    }

    @Test
    public void testGetPath() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        assertThat( fs.getPath( "/" ).toString() ).isEqualTo( "/" );
        assertThat( fs.getPath( "/arik" ).toString() ).isEqualTo( "/arik" );
        assertThat( fs.getPath( "arik" ).toString() ).isEqualTo( "arik" );
        assertThat( fs.getPath( "/arik", "kfir" ).toString() ).isEqualTo( "/arik/kfir" );
        assertThat( fs.getPath( "arik", "kfir" ).toString() ).isEqualTo( "arik/kfir" );
        assertThat( fs.getPath( "arik/kfir" ).toString() ).isEqualTo( "arik/kfir" );
        assertThat( fs.getPath( "arik/kfir" ).toString() ).isEqualTo( "arik/kfir" );
    }

    @Test
    public void testGetPathMatcher() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        PathMatcher globPathMatcher = fs.getPathMatcher( "glob:**.class" );
        assertThat( globPathMatcher.matches( Paths.get( "arik.class" ) ) ).isTrue();
        assertThat( globPathMatcher.matches( Paths.get( "/a/b/c.class" ) ) ).isTrue();
        assertThat( globPathMatcher.matches( Paths.get( "/a/b/c.txt" ) ) ).isFalse();
    }

    @Test
    public void testUserPrincipalLookupServiceNotSupported() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        try
        {
            fs.getUserPrincipalLookupService();
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testNewWatchServiceUnsupported() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        try
        {
            fs.newWatchService();
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testNewDirectoryStream() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        List<Path> paths = new LinkedList<>();
        try( DirectoryStream<Path> stream = fs.newDirectoryStream( fs.getPath( "/test" ), path -> true ) )
        {
            for( Path path : stream )
            {
                assertThat( path ).isInstanceOf( ClasspathPath.class );
                paths.add( path );
            }
        }
        assertThat( paths.size() ).isEqualTo( 1 );
        assertThat( paths.get( 0 ).toString() ).isEqualTo( "/test/test1" );
    }

    @Test
    public void testSpringBootNestedJarUrls() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testFile = this.work.getPath().resolve( "testFile" );
        Files.write( testFile, "value".getBytes(), CREATE_NEW );

        // create inner jar file
        Path innerJarPath = this.work.getPath().resolve( "inner.jar" );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( innerJarPath, CREATE_NEW ) ) )
        {
            JarEntry entry = new JarEntry( testFile.getFileName().toString() );
            jarOutputStream.putNextEntry( entry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();
        }

        // create outer jar file
        Path outerJarPath = this.work.getPath().resolve( "outer.jar" );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( outerJarPath, CREATE_NEW ) ) )
        {
            JarEntry entry = new JarEntry( "inner.jar" );
            jarOutputStream.putNextEntry( entry );
            jarOutputStream.write( Files.readAllBytes( innerJarPath ) );
            jarOutputStream.closeEntry();
        }

        // create classpath file-system with our jar
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { new URL( "jar:file:" + outerJarPath + "!/inner.jar!/" ) };
        LaunchedURLClassLoader urlClassLoader = new LaunchedURLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file
        ClasspathPath path = fs.getPath( "/testFile" );
        assertThat( Files.exists( path ) ).isTrue();
        assertThat( Files.isRegularFile( path ) ).isTrue();
    }

    @Test
    public void testSpringBootBootInfClassesUrls() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testFile = this.work.getPath().resolve( "testFile" );
        Files.write( testFile, "value".getBytes(), CREATE_NEW );

        // create jar file with "BOOT-INF/classes/testFile" entry, which Spring should expose as just "/testFile"
        Path outerJarPath = this.work.getPath().resolve( "outer.jar" );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( outerJarPath, CREATE_NEW ) ) )
        {
            JarEntry entry = new JarEntry( "BOOT-INF/classes/" + testFile.getFileName().toString() );
            jarOutputStream.putNextEntry( entry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();
        }

        // create classpath file-system with our jar
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { new URL( "jar:file:" + outerJarPath + "!/BOOT-INF/classes!/" ) };
        LaunchedURLClassLoader urlClassLoader = new LaunchedURLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file
        ClasspathPath path = fs.getPath( "/testFile" );
        assertThat( Files.exists( path ) ).isTrue();
        assertThat( Files.isRegularFile( path ) ).isTrue();
    }

    @Test
    public void testDuplicateEntries() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testFile = this.work.getPath().resolve( "testFile" );

        // create first jar file
        Path jar1Path = this.work.getPath().resolve( "test1.jar" );
        Files.write( testFile, "from-jar1".getBytes( "UTF-8" ), CREATE_NEW );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( jar1Path, CREATE_NEW ) ) )
        {
            JarEntry entry = new JarEntry( testFile.getFileName().toString() );
            jarOutputStream.putNextEntry( entry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();
        }

        // create second jar file
        Path jar2Path = this.work.getPath().resolve( "test2.jar" );
        Files.write( testFile, "from-jar2".getBytes( "UTF-8" ), TRUNCATE_EXISTING );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( jar2Path, CREATE_NEW ) ) )
        {
            JarEntry entry = new JarEntry( testFile.getFileName().toString() );
            jarOutputStream.putNextEntry( entry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();
        }

        // create third jar file (entry is dir!)
        Path jar3Path = this.work.getPath().resolve( "test3.jar" );
        Files.write( testFile, "from-jar3".getBytes( "UTF-8" ), TRUNCATE_EXISTING );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( jar3Path, CREATE_NEW ) ) )
        {
            JarEntry entry = new JarEntry( testFile.getFileName().toString() + "/file" );
            jarOutputStream.putNextEntry( entry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();
        }

        // delete our file
        Files.delete( testFile );

        // create classpath file-system with our jar
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { jar1Path.toUri().toURL(), jar2Path.toUri().toURL(), jar3Path.toUri().toURL() };
        URLClassLoader urlClassLoader = new URLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file
        ClasspathPath path = fs.getPath( "/testFile" );
        assertThat( Files.exists( path ) ).isTrue();
        assertThat( Files.isRegularFile( path ) ).isTrue();
        assertThat( new String( Files.readAllBytes( path ), "UTF-8" ) ).isEqualTo( "from-jar1" );
    }
}
