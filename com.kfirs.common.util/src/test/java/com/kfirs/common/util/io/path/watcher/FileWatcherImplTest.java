/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.watcher;

import com.kfirs.common.test.WorkPathRule;
import com.kfirs.common.util.io.path.matcher.DelegatingPathMatcher;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;

import static com.kfirs.common.util.io.path.watcher.FileChangedEvent.ChangeType.CREATED;
import static com.kfirs.common.util.io.path.watcher.FileChangedEvent.ChangeType.MODIFIED;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author arik
 * @on 12/25/15.
 */
public class FileWatcherImplTest
{
    @Rule
    public WorkPathRule workPathRule = new WorkPathRule();

    private FileChangeListener eventPublisher;

    private FileWatcherImpl watcher;

    private Path f1;

    @Before
    public void setup() throws IOException
    {
        this.eventPublisher = mock( FileChangeListener.class );
        this.watcher = new FileWatcherImpl( new DelegatingPathMatcher() );
        f1 = this.workPathRule.getPath().resolve( "f1" );
    }

    @After
    public void tearDown()
    {
        this.watcher.close();
        this.watcher = null;
        this.eventPublisher = null;
    }

    @Test
    public void testWatchFileCreated() throws IOException, InterruptedException
    {
        // start a watcher over our work dir
        this.watcher.watch( f1, this.eventPublisher );

        // create a file
        Files.write( f1, "f1".getBytes( "UTF-8" ) );

        // verify calls to event publisher
        Thread.sleep( 2000 );
        InOrder verifier = inOrder( this.eventPublisher );
        verifier.verify( this.eventPublisher ).fileChanged( eq( new FileChangedEvent( f1, f1, CREATED ) ) );
        verifier.verifyNoMoreInteractions();
    }

    @Test
    public void testWatchFileUpdated() throws IOException, InterruptedException
    {
        // create a file
        Files.write( f1, "f1".getBytes( "UTF-8" ) );
        Thread.sleep( 2000 );

        // start a watcher over our work dir
        this.watcher.watch( f1, this.eventPublisher );

        // update the file
        Files.write( f1, "f1-1".getBytes( "UTF-8" ) );

        // verify calls to event publisher
        Thread.sleep( 2000 );
        InOrder verifier = inOrder( this.eventPublisher );
        verifier.verify( this.eventPublisher ).fileChanged( eq( new FileChangedEvent( f1, f1, MODIFIED ) ) );
        verifier.verifyNoMoreInteractions();
    }

    @Test
    public void testWatchPreExistingDirectory() throws IOException, InterruptedException
    {
        // create a file
        Files.write( f1, "f1".getBytes( "UTF-8" ) );

        // start a watcher over our work dir
        this.watcher.watch( this.workPathRule.getPath(), this.eventPublisher );

        // verify calls to event publisher
        Thread.sleep( 2000 );
        verifyZeroInteractions( this.eventPublisher );
    }

    @Test
    public void testCreateAndDeleteWithoutEvent() throws IOException, InterruptedException
    {
        // start a watcher over our work dir
        this.watcher.watch( this.workPathRule.getPath(), this.eventPublisher );
        this.watcher.setScanInterval( 10000 );

        // wait a little
        Thread.sleep( 1000 );

        // create a file
        Files.write( f1, "f1".getBytes( "UTF-8" ) );

        // delete a file
        Files.delete( f1 );

        // verify calls to event publisher
        Thread.sleep( 2000 );
        verifyZeroInteractions( this.eventPublisher );
    }

    @Test
    public void testCreateAndUpdateWithoutGap() throws IOException, InterruptedException
    {
        // start a watcher over our work dir
        this.watcher.watch( this.workPathRule.getPath(), this.eventPublisher );

        // create a file
        Files.createFile( f1 );

        // update a file
        Files.write( f1, "f1-2".getBytes( "UTF-8" ) );

        // verify calls to event publisher
        Thread.sleep( 2000 );
        InOrder verifier = inOrder( this.eventPublisher );
        verifier.verify( this.eventPublisher ).fileChanged( eq( new FileChangedEvent( this.workPathRule.getPath(),
                                                                                      Paths.get( "f1" ),
                                                                                      FileChangedEvent.ChangeType.CREATED ) ) );
        verifier.verifyNoMoreInteractions();
    }

    @Test
    public void testCreateAndUpdateWithGap() throws IOException, InterruptedException
    {
        // start a watcher over our work dir
        this.watcher.watch( this.workPathRule.getPath(), this.eventPublisher );
        Thread.sleep( 2000 );

        // create a file
        Files.createFile( f1 );

        // generate a delay, causing the watcher to seperate the CREATED and MODIFIED events to two different events
        Thread.sleep( 2000 );

        // update a file
        Files.write( f1, "f1-2".getBytes( "UTF-8" ) );
        Thread.sleep( 2000 );

        // verify calls to event publisher
        InOrder verifier = inOrder( this.eventPublisher );
        verifier.verify( this.eventPublisher ).fileChanged( eq( new FileChangedEvent( this.workPathRule.getPath(),
                                                                                      Paths.get( "f1" ),
                                                                                      FileChangedEvent.ChangeType.CREATED ) ) );
        verifier.verify( this.eventPublisher ).fileChanged( eq( new FileChangedEvent( this.workPathRule.getPath(),
                                                                                      Paths.get( "f1" ),
                                                                                      MODIFIED ) ) );
        verifier.verifyNoMoreInteractions();
    }
}
