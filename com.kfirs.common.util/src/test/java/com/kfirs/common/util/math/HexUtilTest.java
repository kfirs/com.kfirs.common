/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.math;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author arik
 * @on 3/1/16.
 */
public class HexUtilTest
{
    public static final byte[] BYTES = new byte[] {
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29
    };

    @Test
    public void testBytesToHex()
    {
        assertThat( HexUtil.bytesToHex( BYTES ) ).isEqualTo( "0a0b0c0d0e0f101112131415161718191a1b1c1d" );
    }
}
