/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.test.JulToSlfRule;
import com.kfirs.common.test.WorkPathRule;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileTime;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import org.junit.Rule;
import org.junit.Test;

import static com.kfirs.common.util.io.path.classpath.ClasspathUtils.CLASS_LOADER_KEY;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * @author arik
 * @on 4/10/16.
 */
public class ClasspathFileAttributesImplTest
{
    @Rule
    public JulToSlfRule julToSlfRule = new JulToSlfRule();

    @Rule
    public WorkPathRule work = new WorkPathRule();

    @Test
    public void testNonExistingPathThrowsNoSuchFileException() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        ClasspathPath path = fs.getPath( "/something/unknown" );
        BasicFileAttributeView view = provider.getFileAttributeView( path, BasicFileAttributeView.class );
        try
        {
            //noinspection ConstantConditions
            view.readAttributes();
            failBecauseExceptionWasNotThrown( NoSuchFileException.class );
        }
        catch( NoSuchFileException ignore )
        {
        }
    }

    @Test
    public void testAttributesOfNonJarClasspathFile() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testFile = this.work.getPath().resolve( "testDir/testFile" );
        Files.createDirectories( testFile.getParent() );
        Files.write( testFile, "value".getBytes(), CREATE_NEW );
        BasicFileAttributeView fileAttrsView = Files.getFileAttributeView( testFile, BasicFileAttributeView.class );
        FileTime modifyTime = FileTime.from( ZonedDateTime.now().minus( 2, DAYS ).toInstant() );
        FileTime accessTime = FileTime.from( ZonedDateTime.now().minus( 1, DAYS ).toInstant() );
        fileAttrsView.setTimes( modifyTime, accessTime, null );

        // create classpath file-system with it
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { this.work.getPath().toUri().toURL() };
        URLClassLoader urlClassLoader = new URLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file
        ClasspathPath path = fs.getPath( "/testDir/testFile" );
        assertThat( Files.exists( path ) ).isTrue();
        assertThat( Files.isRegularFile( path ) ).isTrue();

        // now test accessors
        ClasspathFileAttributesImpl attrs = new ClasspathFileAttributesImpl( path );
        assertThat( attrs.lastModifiedTime() ).isEqualTo( modifyTime );
        assertThat( attrs.lastAccessTime() ).isEqualTo( accessTime );
        assertThat( attrs.size() ).isEqualTo( Files.size( testFile ) );
        assertThat( attrs.isRegularFile() ).isTrue();
        assertThat( attrs.isDirectory() ).isFalse();
        assertThat( attrs.isSymbolicLink() ).isFalse();
        assertThat( attrs.isOther() ).isFalse();

        // now test tomap
        Map<String, Object> expectedAttrsMap = Files.readAttributes( testFile, "*" );
        Map<String, Object> actualAttrsMap = attrs.toMap( "*" );
        assertThat( expectedAttrsMap ).isNotNull();
        assertThat( actualAttrsMap ).isNotNull();
        assertThat( actualAttrsMap.get( "creationTime" ) ).isEqualTo( expectedAttrsMap.get( "creationTime" ) );
        assertThat( actualAttrsMap.get( "lastModifiedTime" ) ).isEqualTo( expectedAttrsMap.get( "lastModifiedTime" ) );
        assertThat( actualAttrsMap.get( "lastAccessTime" ) ).isEqualTo( expectedAttrsMap.get( "lastAccessTime" ) );
        assertThat( actualAttrsMap.get( "size" ) ).isEqualTo( expectedAttrsMap.get( "size" ) );
        assertThat( actualAttrsMap.get( "isDirectory" ) ).isEqualTo( expectedAttrsMap.get( "isDirectory" ) );
        assertThat( actualAttrsMap.get( "isRegularFile" ) ).isEqualTo( expectedAttrsMap.get( "isRegularFile" ) );
        assertThat( expectedAttrsMap.get( "isSymbolicLink" ) ).isEqualTo( actualAttrsMap.get( "isSymbolicLink" ) );
        assertThat( expectedAttrsMap.get( "isOther" ) ).isEqualTo( actualAttrsMap.get( "isOther" ) );
    }

    @Test
    public void testAttributesOfNonJarClasspathDirs() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testDir = this.work.getPath().resolve( "testDir" );
        Files.createDirectories( testDir );
        Files.write( testDir.resolve( "testFile" ), "value".getBytes(), CREATE_NEW );
        BasicFileAttributeView fileAttrsView = Files.getFileAttributeView( testDir, BasicFileAttributeView.class );
        FileTime modifyTime = FileTime.from( ZonedDateTime.now().minus( 2, DAYS ).toInstant() );
        FileTime accessTime = FileTime.from( ZonedDateTime.now().minus( 1, DAYS ).toInstant() );
        fileAttrsView.setTimes( modifyTime, accessTime, null );

        // create classpath file-system with it
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { this.work.getPath().toUri().toURL() };
        URLClassLoader urlClassLoader = new URLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file
        ClasspathPath path = fs.getPath( "/testDir" );
        assertThat( Files.exists( path ) ).isTrue();
        assertThat( Files.isDirectory( path ) ).isTrue();

        // now test accessors
        ClasspathFileAttributesImpl attrs = new ClasspathFileAttributesImpl( path );
        assertThat( attrs.lastModifiedTime() ).isEqualTo( modifyTime );
        assertThat( Files.size( testDir ) ).isEqualTo( attrs.size() );
        assertThat( attrs.isRegularFile() ).isFalse();
        assertThat( attrs.isDirectory() ).isTrue();
        assertThat( attrs.isSymbolicLink() ).isFalse();
        assertThat( attrs.isOther() ).isFalse();

        // now test tomap
        Map<String, Object> expectedAttrsMap = Files.readAttributes( testDir, "*" );
        Map<String, Object> actualAttrsMap = attrs.toMap( "*" );
        assertThat( expectedAttrsMap ).isNotNull();
        assertThat( actualAttrsMap ).isNotNull();
        assertThat( actualAttrsMap.get( "creationTime" ) ).isEqualTo( expectedAttrsMap.get( "creationTime" ) );
        assertThat( actualAttrsMap.get( "lastModifiedTime" ) ).isEqualTo( expectedAttrsMap.get( "lastModifiedTime" ) );
        assertThat( actualAttrsMap.get( "lastAccessTime" ) ).isEqualTo( expectedAttrsMap.get( "lastAccessTime" ) );
        assertThat( actualAttrsMap.get( "size" ) ).isEqualTo( expectedAttrsMap.get( "size" ) );
        assertThat( actualAttrsMap.get( "isDirectory" ) ).isEqualTo( expectedAttrsMap.get( "isDirectory" ) );
        assertThat( actualAttrsMap.get( "isRegularFile" ) ).isEqualTo( expectedAttrsMap.get( "isRegularFile" ) );
        assertThat( actualAttrsMap.get( "isSymbolicLink" ) ).isEqualTo( expectedAttrsMap.get( "isSymbolicLink" ) );
        assertThat( actualAttrsMap.get( "isOther" ) ).isEqualTo( expectedAttrsMap.get( "isOther" ) );
    }

    @Test
    public void testAttributesOfFileEntryInClasspathJarFile() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testFile = this.work.getPath().resolve( "testFile" );
        Files.write( testFile, "value".getBytes(), CREATE_NEW );
        BasicFileAttributeView fileAttrsView = Files.getFileAttributeView( testFile, BasicFileAttributeView.class );
        FileTime modifyTime = FileTime.from( ZonedDateTime.now().minus( 2, DAYS ).withNano( 0 ).toInstant() );
        FileTime accessTime = FileTime.from( ZonedDateTime.now().minus( 1, DAYS ).withNano( 0 ).toInstant() );
        fileAttrsView.setTimes( modifyTime, accessTime, null );

        // create jar file
        Path jarPath = this.work.getPath().resolve( "test.jar" );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( jarPath, CREATE_NEW ) ) )
        {
            JarEntry entry = new JarEntry( testFile.getFileName().toString() );
            entry.setLastModifiedTime( modifyTime );
            entry.setLastAccessTime( accessTime );
            jarOutputStream.putNextEntry( entry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();
        }

        // create classpath file-system with our jar
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { jarPath.toUri().toURL() };
        URLClassLoader urlClassLoader = new URLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file
        ClasspathPath path = fs.getPath( "/testFile" );
        assertThat( Files.exists( path ) ).isTrue();
        assertThat( Files.isRegularFile( path ) ).isTrue();

        // now test accessors
        ClasspathFileAttributesImpl attrs = new ClasspathFileAttributesImpl( path );
        assertThat( attrs.lastModifiedTime() ).isEqualTo( modifyTime );
        assertThat( attrs.size() ).isEqualTo( Files.size( testFile ) );
        assertThat( attrs.isRegularFile() ).isTrue();
        assertThat( attrs.isDirectory() ).isFalse();
        assertThat( attrs.isSymbolicLink() ).isFalse();
        assertThat( attrs.isOther() ).isFalse();

        // now test tomap
        Map<String, Object> expectedAttrsMap = Files.readAttributes( testFile, "*" );
        Map<String, Object> actualAttrsMap = attrs.toMap( "*" );
        assertThat( expectedAttrsMap ).isNotNull();
        assertThat( actualAttrsMap ).isNotNull();
        assertThat( actualAttrsMap.get( "lastModifiedTime" ) ).isEqualTo( expectedAttrsMap.get( "lastModifiedTime" ) );
        assertThat( actualAttrsMap.get( "size" ) ).isEqualTo( expectedAttrsMap.get( "size" ) );
        assertThat( actualAttrsMap.get( "isDirectory" ) ).isEqualTo( expectedAttrsMap.get( "isDirectory" ) );
        assertThat( actualAttrsMap.get( "isRegularFile" ) ).isEqualTo( expectedAttrsMap.get( "isRegularFile" ) );
        assertThat( actualAttrsMap.get( "isSymbolicLink" ) ).isEqualTo( expectedAttrsMap.get( "isSymbolicLink" ) );
        assertThat( actualAttrsMap.get( "isOther" ) ).isEqualTo( expectedAttrsMap.get( "isOther" ) );
    }

    @Test
    public void testAttributesOfDirEntryInClasspathJarFile() throws IOException
    {
        // prepare a file which will be included in the classpath, and give it specific create/modify/access times
        Path testFile = this.work.getPath().resolve( "testFile" );
        Files.write( testFile, "value".getBytes(), CREATE_NEW );
        BasicFileAttributeView fileAttrsView = Files.getFileAttributeView( testFile, BasicFileAttributeView.class );
        FileTime modifyTime = FileTime.from( ZonedDateTime.now().minus( 2, DAYS ).withNano( 0 ).toInstant() );
        FileTime accessTime = FileTime.from( ZonedDateTime.now().minus( 1, DAYS ).withNano( 0 ).toInstant() );
        fileAttrsView.setTimes( modifyTime, accessTime, null );

        // create jar file
        Path jarPath = this.work.getPath().resolve( "test.jar" );
        try( JarOutputStream jarOutputStream = new JarOutputStream( newOutputStream( jarPath, CREATE_NEW ) ) )
        {
            JarEntry dirEntry = new JarEntry( "testDir/" );
            dirEntry.setLastModifiedTime( modifyTime );
            dirEntry.setLastAccessTime( accessTime );
            jarOutputStream.putNextEntry( dirEntry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();

            JarEntry fileEntry = new JarEntry( "testDir/" + testFile.getFileName().toString() );
            fileEntry.setLastModifiedTime( modifyTime );
            fileEntry.setLastAccessTime( accessTime );
            jarOutputStream.putNextEntry( fileEntry );
            jarOutputStream.write( Files.readAllBytes( testFile ) );
            jarOutputStream.closeEntry();
        }

        // create classpath file-system with our jar
        ClassLoader classLoader = getClass().getClassLoader();
        URL[] classPathUrls = { jarPath.toUri().toURL() };
        URLClassLoader urlClassLoader = new URLClassLoader( classPathUrls, classLoader );
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, urlClassLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        // locate our test file
        ClasspathPath path = fs.getPath( "/testDir" );
        assertThat( Files.exists( path ) ).isTrue();
        assertThat( Files.isDirectory( path ) ).isTrue();

        // now test accessors
        ClasspathFileAttributesImpl attrs = new ClasspathFileAttributesImpl( path );
        assertThat( attrs.lastModifiedTime() ).isEqualTo( modifyTime );
        assertThat( attrs.size() ).isEqualTo( Files.size( testFile ) );
        assertThat( attrs.isRegularFile() ).isFalse();
        assertThat( attrs.isDirectory() ).isTrue();
        assertThat( attrs.isSymbolicLink() ).isFalse();
        assertThat( attrs.isOther() ).isFalse();

        // now test tomap
        Map<String, Object> expectedAttrsMap = Files.readAttributes( testFile, "*" );
        Map<String, Object> actualAttrsMap = attrs.toMap( "*" );
        assertThat( expectedAttrsMap ).isNotNull();
        assertThat( actualAttrsMap ).isNotNull();
        assertThat( actualAttrsMap.get( "lastModifiedTime" ) ).isEqualTo( expectedAttrsMap.get( "lastModifiedTime" ) );
        assertThat( actualAttrsMap.get( "isDirectory" ) ).isEqualTo( true );
        assertThat( actualAttrsMap.get( "isRegularFile" ) ).isEqualTo( false );
        assertThat( actualAttrsMap.get( "isSymbolicLink" ) ).isEqualTo( false );
        assertThat( actualAttrsMap.get( "isOther" ) ).isEqualTo( false );
    }
}
