/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.test.JulToSlfRule;
import com.kfirs.common.util.io.path.classpath.ByteArraySeekableByteChannel;
import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.junit.Rule;
import org.junit.Test;

import static java.util.Arrays.copyOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * @author arik
 * @on 4/10/16.
 */
public class ByteArraySeekableByteChannelTest
{
    @Rule
    public JulToSlfRule julToSlfRule = new JulToSlfRule();

    @Test
    public void testAlwaysOpen()
    {
        ByteArraySeekableByteChannel channel = new ByteArraySeekableByteChannel( new byte[ 10 ] );
        assertThat( channel.isOpen() ).isTrue();
        channel.close();
        assertThat( channel.isOpen() ).isTrue();
    }

    @Test
    public void testReadIntoByteBuffer() throws Exception
    {
        final byte[] bytes = new byte[] {
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9
        };

        testReadIntoByteBuffer( new ByteArraySeekableByteChannel( bytes ),
                                ByteBuffer.allocate( bytes.length ),
                                bytes.length,
                                bytes.length,
                                bytes.length,
                                bytes );

        testReadIntoByteBuffer( new ByteArraySeekableByteChannel( bytes ),
                                ByteBuffer.allocate( bytes.length / 2 ),
                                bytes.length / 2,
                                bytes.length / 2,
                                bytes.length / 2,
                                copyOf( bytes, bytes.length / 2 ) );

        ByteArraySeekableByteChannel src = new ByteArraySeekableByteChannel( bytes );
        ByteBuffer dst = ByteBuffer.allocate( bytes.length * 2 );
        testReadIntoByteBuffer( src,
                                dst,
                                bytes.length,
                                bytes.length,
                                bytes.length,
                                copyOf( bytes, bytes.length * 2 ) );
        testReadIntoByteBuffer( src,
                                dst,
                                0,
                                bytes.length,
                                bytes.length,
                                copyOf( bytes, bytes.length * 2 ) );

        src.position( 0 );
        byte[] expectedBytes = copyOf( bytes, bytes.length * 2 );
        System.arraycopy( bytes, 0, expectedBytes, bytes.length, bytes.length );
        testReadIntoByteBuffer( src,
                                dst,
                                bytes.length,
                                bytes.length,
                                bytes.length * 2,
                                expectedBytes );
    }

    private void testReadIntoByteBuffer( @NotNull ByteArraySeekableByteChannel src,
                                         @NotNull ByteBuffer dst,
                                         int expectedBytesRead,
                                         long expectedSrcPosition,
                                         long expectedDstPosition,
                                         byte... expectedDstBytes ) throws IOException
    {
        assertThat( src.read( dst ) ).isEqualTo( expectedBytesRead );
        assertThat( src.position() ).isEqualTo( expectedSrcPosition );
        assertThat( dst.position() ).isEqualTo( ( int ) expectedDstPosition );
        assertThat( dst.array() ).isEqualTo( expectedDstBytes );
    }

    @Test
    public void testWrite()
    {
        ByteArraySeekableByteChannel channel = new ByteArraySeekableByteChannel( new byte[ 10 ] );
        try
        {
            channel.write( ByteBuffer.allocate( 10 ) );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testPosition() throws IOException
    {
        ByteArraySeekableByteChannel channel = new ByteArraySeekableByteChannel( new byte[ 10 ] );
        assertThat( channel.position() ).isZero();
        channel.read( ByteBuffer.allocate( 3 ) );
        assertThat( channel.position() ).isEqualTo( 3 );
        channel.read( ByteBuffer.allocate( 0 ) );
        assertThat( channel.position() ).isEqualTo( 3 );

        channel = new ByteArraySeekableByteChannel( new byte[] { 0, 1, 2, 3 } );
        channel.position( 2 );
        ByteBuffer buf = ByteBuffer.allocate( 3 );
        assertThat( channel.read( buf ) ).isEqualTo( 2 );
        assertThat( buf.array() ).isEqualTo( new byte[] { 2, 3, 0 } );
    }

    @Test
    public void testSize() throws IOException
    {
        ByteArraySeekableByteChannel channel = new ByteArraySeekableByteChannel( new byte[ 10 ] );
        assertThat( channel.size() ).isEqualTo( 10 );
    }

    @Test
    public void testTruncate() throws IOException
    {
        ByteArraySeekableByteChannel channel = new ByteArraySeekableByteChannel( new byte[ 10 ] );
        try
        {
            channel.truncate( 1 );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }
}
