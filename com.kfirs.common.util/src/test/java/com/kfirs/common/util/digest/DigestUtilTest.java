/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.digest;

import com.kfirs.common.test.WorkPathRule;
import com.kfirs.common.util.math.HexUtil;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import org.junit.Rule;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author arik
 * @on 3/1/16.
 */
public class DigestUtilTest
{
    public static final byte[] BYTES = new byte[] {
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29
    };

    @Rule
    public WorkPathRule workPathRule = new WorkPathRule();

    @Test
    public void testDigestBytes()
    {
        byte[] md5Bytes = DigestUtil.digest( "MD5", BYTES );
        assertThat( HexUtil.bytesToHex( md5Bytes ) ).isEqualTo( "82e36f4ff37158fef5c68a080bb1a06b" );
    }

    @Test
    public void testDigestFile() throws IOException
    {
        Path file = this.workPathRule.getPath().resolve( "file" );
        Files.write( file, BYTES, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE );

        byte[] md5Bytes = DigestUtil.digest( "MD5", file );
        assertThat( HexUtil.bytesToHex( md5Bytes ) ).isEqualTo( "82e36f4ff37158fef5c68a080bb1a06b" );
    }

    @Test
    public void testThreadSafety() throws IOException, InterruptedException
    {
        ExecutorService executor = Executors.newFixedThreadPool( 128 );
        List<Future<?>> futures = new LinkedList<>();
        for( int i = 0; i < 1_000_000; i++ )
        {
            futures.add( executor.submit( this::testDigestBytes ) );
        }
        executor.shutdown();
        executor.awaitTermination( 5, TimeUnit.MINUTES );

        futures.forEach( future ->
                         {
                             try
                             {
                                 future.get();
                             }
                             catch( InterruptedException e )
                             {
                                 throw new IllegalStateException( e );
                             }
                             catch( ExecutionException e )
                             {
                                 e.printStackTrace();
                             }
                         } );
    }
}
