/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.test.JulToSlfRule;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttributeView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import static com.kfirs.common.util.io.path.classpath.ClasspathUtils.CLASS_LOADER_KEY;
import static java.lang.System.identityHashCode;
import static java.util.Collections.emptySet;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * @author arik
 * @on 4/10/16.
 */
public class ClasspathFileSystemProviderTest
{
    @Rule
    public JulToSlfRule julToSlfRule = new JulToSlfRule();

    @Test
    public void testNewFileSystemForPathIsUnsupported() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.newFileSystem( Paths.get( "classpath:///" ), Collections.emptyMap() );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }

        try
        {
            provider.newFileSystem( Paths.get( "file:///" ), Collections.emptyMap() );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testNewFileSystemValidatesUriScheme() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.newFileSystem( URI.create( "file:///" ), Collections.emptyMap() );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testNewFileSystemDefaultsToSystemClassLoader() throws IOException
    {
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), Collections.emptyMap() );
        assertThat( fs ).isNotNull();
        assertThat( fs.getClassLoader() ).isSameAs( systemClassLoader );
    }

    @Test
    public void testNewFileSystemPrefersClassLoaderFromEnv() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();

        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ),
                                                         singletonMap( CLASS_LOADER_KEY, classLoader ) );
        assertThat( fs ).isNotNull();
        assertThat( fs.getClassLoader() ).isSameAs( classLoader );
    }

    @Test
    public void testTwoFileSystemsForSameClassLoaderThrowsFileSystemAlreadyExists() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();

        Map<String, Object> env1 = new HashMap<>();
        env1.put( CLASS_LOADER_KEY, classLoader );

        // just create, so 2nd call will fail
        provider.newFileSystem( URI.create( "classpath:///" ), env1 );

        // now the 2nd call which is expected to fail
        try
        {
            provider.newFileSystem( URI.create( "classpath:///" ), singletonMap( CLASS_LOADER_KEY, classLoader ) );
            failBecauseExceptionWasNotThrown( FileSystemAlreadyExistsException.class );
        }
        catch( FileSystemAlreadyExistsException ignore )
        {
        }
    }

    @Test
    public void testGetFileSystemByUri() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();

        Map<String, Object> env = new HashMap<>();
        env.put( CLASS_LOADER_KEY, classLoader );
        FileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        FileSystem fsById = provider.getFileSystem( URI.create( "classpath://" + identityHashCode( classLoader ) + "/" ) );
        assertThat( fsById ).isSameAs( fs );

        try
        {
            provider.getFileSystem( URI.create( "classpath://12345/" ) );
            failBecauseExceptionWasNotThrown( FileSystemNotFoundException.class );
        }
        catch( FileSystemNotFoundException ignore )
        {
        }
    }

    @Test
    public void testCloseFileSystem() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();
        URI fsUri = URI.create( "classpath://" + identityHashCode( classLoader ) + "/" );

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();

        Map<String, Object> env = new HashMap<>();
        env.put( CLASS_LOADER_KEY, classLoader );
        FileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        assertThat( provider.getFileSystem( fsUri ) ).isSameAs( fs );
        fs.close();
        try
        {
            provider.getFileSystem( fsUri );
            failBecauseExceptionWasNotThrown( FileSystemNotFoundException.class );
        }
        catch( FileSystemNotFoundException ignore )
        {
        }
    }

    @Test
    public void testGetPathFailsOnUnknownFs() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.getPath( URI.create( "classpath://12345/arik" ) );
            failBecauseExceptionWasNotThrown( FileSystemNotFoundException.class );
        }
        catch( FileSystemNotFoundException ignore )
        {
        }
    }

    @Test
    public void testNewFileChannelFailsOnUnknownFs() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.newFileChannel( Paths.get( "file:///arik" ), Collections.emptySet() );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testNewByteChannelFailsOnUnknownFs() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.newByteChannel( Paths.get( "file:///arik" ), Collections.emptySet() );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testNewDirectoryStreamFailsOnUnknownFs() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.newDirectoryStream( Paths.get( "file:///arik" ), entry -> true );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testCopyIsUnsupported() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.copy( Mockito.mock( Path.class ), Mockito.mock( Path.class ) );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testCreateDirectoryIsUnsupported() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.createDirectory( Mockito.mock( Path.class ) );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testDeleteIsUnsupported() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.delete( Mockito.mock( Path.class ) );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testMoveIsUnsupported() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.move( Mockito.mock( Path.class ), Mockito.mock( Path.class ) );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testIsHiddenFailsOnNonClasspathPaths() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.isHidden( Mockito.mock( Path.class ) );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testGetFileStoreFailsOnNonClasspathPaths() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.getFileStore( Mockito.mock( Path.class ) );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testGetFileAttributeViewFailsOnNonClasspathPaths() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.getFileAttributeView( Mockito.mock( Path.class ), FileAttributeView.class );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testReadAttributesFailsOnNonClasspathPaths() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.readAttributes( Mockito.mock( Path.class ), BasicFileAttributes.class );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }

        try
        {
            provider.readAttributes( Mockito.mock( Path.class ), "" );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testSetAttributeIsNotSupported() throws IOException
    {
        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        try
        {
            provider.setAttribute( Mockito.mock( Path.class ), "", "" );
            failBecauseExceptionWasNotThrown( UnsupportedOperationException.class );
        }
        catch( UnsupportedOperationException ignore )
        {
        }
    }

    @Test
    public void testNewFileChannel() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        try( ByteArrayFileChannel channel = provider.newFileChannel( fs.getPath( "/test/test1/README.txt" ),
                                                                     emptySet() ) )
        {
            assertThat( channel ).isNotNull();
        }

        try
        {
            provider.newFileChannel( Paths.get( "/" ), emptySet() );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }

    @Test
    public void testNewByteChannel() throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        ClasspathFileSystemProvider provider = new ClasspathFileSystemProvider();
        Map<String, ClassLoader> env = singletonMap( CLASS_LOADER_KEY, classLoader );
        ClasspathFileSystem fs = provider.newFileSystem( URI.create( "classpath:///" ), env );

        try( ByteArraySeekableByteChannel channel = provider.newByteChannel( fs.getPath( "/test/test1/README.txt" ),
                                                                             emptySet() ) )
        {
            assertThat( channel ).isNotNull();
        }

        try
        {
            provider.newByteChannel( Paths.get( "/" ), emptySet() );
            failBecauseExceptionWasNotThrown( IllegalArgumentException.class );
        }
        catch( IllegalArgumentException ignore )
        {
        }
    }
}
