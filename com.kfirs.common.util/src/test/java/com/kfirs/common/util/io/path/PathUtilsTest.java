/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path;

import com.kfirs.common.test.WorkPathRule;
import com.kfirs.common.util.io.path.classpath.ClasspathUtils;
import java.io.IOException;
import java.nio.file.*;
import org.junit.Rule;
import org.junit.Test;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * @author arik
 * @on 4/19/16.
 */
public class PathUtilsTest
{
    @Rule
    public WorkPathRule workPathRule = new WorkPathRule();

    @Test
    public void testDeleteFile() throws IOException
    {
        Path expectedTarget = this.workPathRule.getPath().resolve( "DEST.txt" );
        Path unrelatedFile = this.workPathRule.getPath().resolve( "unrelated.txt" );
        Files.write( expectedTarget, "Initial content".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );
        Files.write( unrelatedFile, "unrelated file".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );
        assertThat( Files.exists( expectedTarget ) ).isTrue();
        assertThat( Files.exists( unrelatedFile ) ).isTrue();

        PathUtils.delete( expectedTarget );
        assertThat( Files.exists( expectedTarget ) ).isFalse();
        assertThat( Files.exists( unrelatedFile ) ).isTrue();
    }

    @Test
    public void testDeleteDir() throws IOException
    {
        Path expectedTarget = this.workPathRule.getPath().resolve( "a/b/DEST.txt" );
        Path unrelatedFile = this.workPathRule.getPath().resolve( "unrelated.txt" );

        Files.createDirectories( expectedTarget.getParent() );
        Files.write( expectedTarget, "Initial content".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );
        Files.write( unrelatedFile, "unrelated file".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );
        assertThat( Files.exists( expectedTarget ) ).isTrue();
        assertThat( Files.exists( unrelatedFile ) ).isTrue();

        PathUtils.delete( this.workPathRule.getPath().resolve( "a" ) );
        assertThat( Files.exists( expectedTarget ) ).isFalse();
        assertThat( Files.exists( expectedTarget.getParent() ) ).isFalse();
        assertThat( Files.exists( expectedTarget.getParent().getParent() ) ).isFalse();
        assertThat( Files.exists( unrelatedFile ) ).isTrue();
    }

    @Test
    public void testCopyNonExistantSource() throws IOException
    {
        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        try
        {
            PathUtils.copy( classpath.getPath( "/unknown-file" ), this.workPathRule.getPath() );
            failBecauseExceptionWasNotThrown( NoSuchFileException.class );
        }
        catch( NoSuchFileException ignore )
        {
        }
    }

    @Test
    public void testCopyFileIntoExistingDirectory() throws IOException
    {
        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        PathUtils.copy( classpath.getPath( "/test/test1/README.txt" ), this.workPathRule.getPath() );

        Path expectedTarget = this.workPathRule.getPath().resolve( "README.txt" );
        assertThat( Files.exists( expectedTarget ) ).isTrue();
        assertThat( Files.isRegularFile( expectedTarget ) ).isTrue();
        assertThat( new String( readAllBytes( expectedTarget ), "UTF-8" ) ).isEqualTo( "Hello from test1\n" );
    }

    @Test
    public void testCopyFileIntoExistingFileWithReplaceFlag() throws IOException
    {
        Path expectedTarget = this.workPathRule.getPath().resolve( "DEST.txt" );
        Files.write( expectedTarget, "Initial content".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );

        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        PathUtils.copy( classpath.getPath( "/test/test1/README.txt" ), expectedTarget, REPLACE_EXISTING );

        assertThat( Files.exists( expectedTarget ) ).isTrue();
        assertThat( Files.isRegularFile( expectedTarget ) ).isTrue();
        assertThat( new String( readAllBytes( expectedTarget ), "UTF-8" ) ).isEqualTo( "Hello from test1\n" );
    }

    @Test
    public void testCopyFileIntoExistingFileNoReplaceFlag() throws IOException
    {
        Path expectedTarget = this.workPathRule.getPath().resolve( "DEST.txt" );
        Files.write( expectedTarget, "Initial content".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );

        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        try
        {
            PathUtils.copy( classpath.getPath( "/test/test1/README.txt" ), expectedTarget );
            failBecauseExceptionWasNotThrown( FileAlreadyExistsException.class );
        }
        catch( FileAlreadyExistsException ignore )
        {
        }
    }

    @Test
    public void testCopyFileIntoNonExistingFile() throws IOException
    {
        Path expectedTarget = this.workPathRule.getPath().resolve( "target/DEST.txt" );

        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        PathUtils.copy( classpath.getPath( "/test/test1/README.txt" ), expectedTarget );

        assertThat( Files.exists( expectedTarget ) ).isTrue();
        assertThat( Files.isRegularFile( expectedTarget ) ).isTrue();
        assertThat( new String( readAllBytes( expectedTarget ), "UTF-8" ) ).isEqualTo( "Hello from test1\n" );
    }

    @Test
    public void testCopyDirIntoExistingDir() throws IOException
    {
        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        PathUtils.copy( classpath.getPath( "/test/test1" ), this.workPathRule.getPath() );

        Path expectedTarget = this.workPathRule.getPath();
        assertThat( Files.exists( expectedTarget ) ).isTrue();
        assertThat( Files.isDirectory( expectedTarget ) ).isTrue();
        assertThat( new String( readAllBytes( expectedTarget.resolve( "README.txt" ) ), "UTF-8" ) ).isEqualTo( "Hello from test1\n" );
        assertThat( new String( readAllBytes( expectedTarget.resolve( "sub/README.txt" ) ), "UTF-8" ) ).isEqualTo( "I am sub.\n" );
    }

    @Test
    public void testCopyDirIntoExistingFile() throws IOException
    {
        Path expectedTarget = this.workPathRule.getPath().resolve( "DEST.txt" );
        Files.write( expectedTarget, "Initial content".getBytes( "UTF-8" ), StandardOpenOption.CREATE_NEW );

        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        try
        {
            PathUtils.copy( classpath.getPath( "/test/test1" ), expectedTarget );
            failBecauseExceptionWasNotThrown( FileAlreadyExistsException.class );
        }
        catch( FileAlreadyExistsException ignore )
        {
        }
    }

    @Test
    public void testCopyDirIntoNonExistingDir() throws IOException
    {
        Path target = this.workPathRule.getPath().resolve( "dst" );

        FileSystem classpath = ClasspathUtils.getFileSystem( getClass().getClassLoader() );
        PathUtils.copy( classpath.getPath( "/test/test1" ), target );

        assertThat( Files.exists( target ) ).isTrue();
        assertThat( Files.isDirectory( target ) ).isTrue();
        assertThat( new String( readAllBytes( target.resolve( "README.txt" ) ), "UTF-8" ) ).isEqualTo( "Hello from test1\n" );
        assertThat( new String( readAllBytes( target.resolve( "sub/README.txt" ) ), "UTF-8" ) ).isEqualTo( "I am sub.\n" );
    }
}
