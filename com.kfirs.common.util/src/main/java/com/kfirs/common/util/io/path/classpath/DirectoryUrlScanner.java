/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableSet;

/**
 * @author arik
 * @on 4/28/16.
 */
class DirectoryUrlScanner extends UrlScanner
{
    private static final Set<Pattern> SUPPORTED_PATTERNS;

    static
    {
        SUPPORTED_PATTERNS = unmodifiableSet( new LinkedHashSet<>( singletonList(
                Pattern.compile( "^file:([^!]+)$" )
        ) ) );
    }

    @Override
    boolean supports( @NotNull ClassLoader classLoader, @NotNull URL url )
    {
        String externalUrlForm = url.toExternalForm();
        for( Pattern pattern : SUPPORTED_PATTERNS )
        {
            Matcher matcher = pattern.matcher( externalUrlForm );
            if( matcher.matches() )
            {
                String pathName = matcher.group( 1 );
                Path path = Paths.get( pathName );
                return Files.isDirectory( path );
            }
        }
        return false;
    }

    @Override
    void scan( @NotNull PathNode root, @NotNull ClassLoader classLoader, @NotNull URL url ) throws IOException
    {
        String externalUrlForm = url.toExternalForm();
        for( Pattern pattern : SUPPORTED_PATTERNS )
        {
            Matcher matcher = pattern.matcher( externalUrlForm );
            if( matcher.matches() )
            {
                String pathName = matcher.group( 1 );
                Path path = Paths.get( pathName );
                if( Files.isDirectory( path ) )
                {
                    Files.walk( path )
                         .filter( p -> !p.equals( path ) )
                         .forEach( p -> {
                             PathNode child = root.addChild( path.relativize( p ).toString() );
                             child.addResource( new PathResource( p ) );
                         } );
                }
                return;
            }
        }
        throw new IllegalStateException( "unsupported URL for JAR file scanner: " + url );
    }

    private static class PathResource extends Resource
    {
        @NotNull
        private final Path path;

        private PathResource( @NotNull Path path )
        {
            this.path = path;
        }

        @Override
        public boolean equals( Object o )
        {
            if( this == o )
            {
                return true;
            }
            if( o == null || getClass() != o.getClass() )
            {
                return false;
            }
            PathResource that = ( PathResource ) o;
            return Objects.equals( this.path, that.path );
        }

        @Override
        public int hashCode()
        {
            return Objects.hash( this.path );
        }

        @Override
        public String toString()
        {
            return getClass().getSimpleName() + "[" + this.path + "]";
        }

        @NotNull
        @Override
        InputStream getInputStream() throws IOException
        {
            return Files.newInputStream( this.path, StandardOpenOption.READ );
        }

        @NotNull
        @Override
        FileTime getCreationTime()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).creationTime();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }

        @NotNull
        @Override
        FileTime getLastModifiedTime()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).lastModifiedTime();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }

        @NotNull
        @Override
        FileTime getLastAccessTime()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).lastAccessTime();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }

        @Override
        boolean isDirectory()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).isDirectory();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }

        @Override
        boolean isRegularFile()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).isRegularFile();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }

        @Override
        boolean isSymbolicLink()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).isSymbolicLink();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }

        @Override
        boolean isOther()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).isOther();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }

        @Override
        long getSize()
        {
            try
            {
                return Files.readAttributes( this.path, BasicFileAttributes.class ).size();
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( "Error reading attributes for '" + this.path + "'", e );
            }
        }
    }
}
