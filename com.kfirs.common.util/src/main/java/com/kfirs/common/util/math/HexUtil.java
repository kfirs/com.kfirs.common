/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.math;

import com.kfirs.common.util.lang.NotNull;

/**
 * @author arik
 * @on 2/26/16.
 */
public final class HexUtil
{
    @NotNull
    public static String bytesToHex( @NotNull byte[] bytes )
    {
        StringBuilder hex = new StringBuilder( "" );
        for( byte digestByte : bytes )
        {
            hex.append( Integer.toString( ( digestByte & 0xff ) + 0x100, 16 ).substring( 1 ) );
        }
        return hex.toString();
    }

    private HexUtil()
    {
    }
}
