/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.messages;

import org.springframework.boot.context.properties.ConfigurationProperties;

import static java.util.Objects.requireNonNull;

/**
 * @author arik
 * @on 2/7/17.
 */
@ConfigurationProperties( prefix = "kfirs.messages" )
public class MessageSourceProperties
{
    private boolean concurrentRefresh = true;

    private boolean alwaysUseMessageFormat = true;

    private String[] baseNames = new String[] { "file:./config/messages/messages", "classpath:/messages" };

    private int cacheSeconds = 5;

    private String defaultEncoding = "UTF-8";

    private boolean fallbackToSystemLocale = true;

    public boolean isConcurrentRefresh()
    {
        return concurrentRefresh;
    }

    public void setConcurrentRefresh( boolean concurrentRefresh )
    {
        this.concurrentRefresh = concurrentRefresh;
    }

    public boolean isAlwaysUseMessageFormat()
    {
        return alwaysUseMessageFormat;
    }

    public void setAlwaysUseMessageFormat( boolean alwaysUseMessageFormat )
    {
        this.alwaysUseMessageFormat = alwaysUseMessageFormat;
    }

    public String[] getBaseNames()
    {
        return baseNames;
    }

    public void setBaseNames( String[] baseNames )
    {
        if( baseNames == null || baseNames.length == 0 )
        {
            throw new IllegalArgumentException();
        }
        this.baseNames = baseNames;
    }

    public int getCacheSeconds()
    {
        return cacheSeconds;
    }

    public void setCacheSeconds( int cacheSeconds )
    {
        if( cacheSeconds < 2 )
        {
            throw new IllegalArgumentException();
        }
        this.cacheSeconds = cacheSeconds;
    }

    public String getDefaultEncoding()
    {
        return defaultEncoding;
    }

    public void setDefaultEncoding( String defaultEncoding )
    {
        this.defaultEncoding = requireNonNull( defaultEncoding );
    }

    public boolean isFallbackToSystemLocale()
    {
        return fallbackToSystemLocale;
    }

    public void setFallbackToSystemLocale( boolean fallbackToSystemLocale )
    {
        this.fallbackToSystemLocale = fallbackToSystemLocale;
    }
}
