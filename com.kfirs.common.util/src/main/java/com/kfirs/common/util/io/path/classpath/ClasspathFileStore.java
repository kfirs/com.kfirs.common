/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.Nullable;
import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileAttributeView;
import java.nio.file.attribute.FileStoreAttributeView;

/**
 * @author arik
 * @on 4/1/16.
 */
class ClasspathFileStore extends FileStore
{
    @Override
    public String name()
    {
        return "classpath";
    }

    @Override
    public String type()
    {
        return "classpath";
    }

    @Override
    public boolean isReadOnly()
    {
        return true;
    }

    @Override
    public long getTotalSpace() throws IOException
    {
        return 0;
    }

    @Override
    public long getUsableSpace() throws IOException
    {
        return 0;
    }

    @Override
    public long getUnallocatedSpace() throws IOException
    {
        return 0;
    }

    @Override
    public boolean supportsFileAttributeView( Class<? extends FileAttributeView> type )
    {
        return type.equals( BasicFileAttributeView.class ) || type.equals( ClasspathFileAttributesView.class );
    }

    @Override
    public boolean supportsFileAttributeView( String name )
    {
        return name.equalsIgnoreCase( "basic" ) || name.equalsIgnoreCase( "classpath" );
    }

    @Nullable
    @Override
    public <V extends FileStoreAttributeView> V getFileStoreAttributeView( Class<V> type )
    {
        if( type == null )
        {
            throw new NullPointerException();
        }
        else
        {
            return null;
        }
    }

    @Override
    public Object getAttribute( String attribute ) throws IOException
    {
        switch( attribute )
        {
            case "totalSpace":
                return this.getTotalSpace();
            case "usableSpace":
                return this.getUsableSpace();
            case "unallocatedSpace":
                return this.getUnallocatedSpace();
            default:
                throw new UnsupportedOperationException( "classpath file-store does not support attribute '" + attribute + "'" );
        }
    }
}
