/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.reflection;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;

import static java.util.Objects.requireNonNull;
import static org.springframework.util.ReflectionUtils.*;

/**
 * @author arik
 * @on 3/16/17.
 */
public class ReflectionHelper
{
    @NotNull
    public Type getGenericMemberType( @NotNull Member member )
    {
        if( member instanceof Field )
        {
            Field field = ( Field ) member;
            return field.getGenericType();
        }
        else if( member instanceof Method )
        {
            PropertyDescriptor propDesc = BeanUtils.findPropertyForMethod( ( Method ) member );
            Method readMethod = propDesc.getReadMethod();
            if( readMethod != null )
            {
                return readMethod.getGenericReturnType();
            }
            Method writeMethod = propDesc.getWriteMethod();
            if( writeMethod != null )
            {
                return writeMethod.getGenericReturnType();
            }
            throw new IllegalStateException();
        }
        else
        {
            throw new IllegalStateException( "unsupported member type '" + member.getClass().getName() + "'" );
        }
    }

    @Nullable
    public Object getMemberValue( @NotNull Member member, @NotNull Object object )
    {
        if( member instanceof Field )
        {
            return getField( ( Field ) member, object );
        }
        else if( member instanceof Method )
        {
            PropertyDescriptor propDesc = BeanUtils.findPropertyForMethod( ( Method ) member );
            return invokeMethod( requireNonNull( propDesc.getReadMethod() ), object );
        }
        else
        {
            throw new IllegalStateException( "unsupported member type '" + member.getClass().getName() + "'" );
        }
    }

    public void setMemberValue( @NotNull Member member, @NotNull Object object, @Nullable Object value )
    {
        if( member instanceof Field )
        {
            setField( ( Field ) member, object, value );
        }
        else if( member instanceof Method )
        {
            PropertyDescriptor propDesc = BeanUtils.findPropertyForMethod( ( Method ) member );
            invokeMethod( requireNonNull( propDesc.getWriteMethod() ), object, value );
        }
        else
        {
            throw new UnsupportedOperationException( "unsupported member type '" + member + "' in object '" + object + "''" );
        }
    }

    @Nullable
    public <T extends Annotation> T getMemberAnnotation( @NotNull Member member, @NotNull Class<T> annotationType )
    {
        if( member instanceof Field )
        {
            Field field = ( Field ) member;
            return field.getAnnotation( annotationType );
        }
        else if( member instanceof Method )
        {
            PropertyDescriptor propDesc = BeanUtils.findPropertyForMethod( ( Method ) member );
            Method readMethod = propDesc.getReadMethod();
            if( readMethod != null )
            {
                T annotation = readMethod.getAnnotation( annotationType );
                if( annotation != null )
                {
                    return annotation;
                }
            }
            Method writeMethod = propDesc.getWriteMethod();
            if( writeMethod != null )
            {
                T annotation = writeMethod.getAnnotation( annotationType );
                if( annotation != null )
                {
                    return annotation;
                }
            }
            return null;
        }
        else
        {
            throw new IllegalStateException( "unsupported member type '" + member.getClass().getName() + "'" );
        }
    }
}
