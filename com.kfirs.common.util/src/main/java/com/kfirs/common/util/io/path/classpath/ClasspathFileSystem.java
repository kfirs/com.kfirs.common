/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.*;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.nio.file.spi.FileSystemProvider;
import java.util.*;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;

/**
 * @author arik
 * @on 4/1/16.
 */
class ClasspathFileSystem extends FileSystem implements Runnable
{
    private static final String GLOB_SYNTAX = "glob";

    private static final String REGEX_SYNTAX = "regex";

    @NotNull
    private static final Set<String> supportedFileAttributeViews = buildSupportedFileAttributeViews();

    private static Set<String> buildSupportedFileAttributeViews()
    {
        Set<String> supportedFileAttributeViews = new HashSet<>( asList( "basic", "classpath" ) );
        supportedFileAttributeViews.add( "basic" );
        supportedFileAttributeViews.add( "classpath" );
        return Collections.unmodifiableSet( supportedFileAttributeViews );
    }

    @NotNull
    private final ClasspathFileSystemProvider provider;

    @NotNull
    private final ClassLoader classLoader;

    @NotNull
    private final ClasspathPath rootPath;

    @NotNull
    private final PathNode rootNode;

    @NotNull
    private final FileStore fileStore = new ClasspathFileStore();

    private boolean closed;

    ClasspathFileSystem( @NotNull ClasspathFileSystemProvider provider, @NotNull ClassLoader classLoader )
            throws IOException
    {
        this.provider = provider;
        this.classLoader = classLoader;
        this.rootNode = UrlEntryExtractor.extractPathNodes( this );
        this.rootPath = new ClasspathPath( this, Paths.get( "/" ).getRoot(), this.rootNode );
    }

    @Override
    public void run()
    {
        printNode( this.rootNode );
    }

    private void printNode( @NotNull PathNode node )
    {
        System.out.println( node.getPath() );
        for( Resource resource : node.getResources() )
        {
            System.out.println( "\t" + resource );
        }
        for( PathNode childNode : node.getChildren() )
        {
            printNode( childNode );
        }
    }

    @NotNull
    public ClassLoader getClassLoader()
    {
        validateFileSystemNotClosed();
        return this.classLoader;
    }

    @NotNull
    public FileStore getFileStore()
    {
        validateFileSystemNotClosed();
        return this.fileStore;
    }

    @NotNull
    @Override
    public FileSystemProvider provider()
    {
        validateFileSystemNotClosed();
        return this.provider;
    }

    @Override
    public void close() throws IOException
    {
        this.provider.closeFileSystem( this );
        this.closed = true;
    }

    @Override
    public boolean isOpen()
    {
        return !this.closed;
    }

    @Override
    public boolean isReadOnly()
    {
        validateFileSystemNotClosed();
        return true;
    }

    @NotNull
    @Override
    public String getSeparator()
    {
        validateFileSystemNotClosed();
        return "/";
    }

    @NotNull
    @Override
    public List<Path> getRootDirectories()
    {
        validateFileSystemNotClosed();
        List<Path> roots = new LinkedList<>();
        roots.add( this.rootPath );
        return roots;
    }

    @NotNull
    @Override
    public List<FileStore> getFileStores()
    {
        validateFileSystemNotClosed();
        List<FileStore> fileStores = new LinkedList<>();
        fileStores.add( this.fileStore );
        return fileStores;
    }

    @NotNull
    @Override
    public Set<String> supportedFileAttributeViews()
    {
        validateFileSystemNotClosed();
        return supportedFileAttributeViews;
    }

    @NotNull
    @Override
    public ClasspathPath getPath( String first, String... more )
    {
        validateFileSystemNotClosed();

        Path shadowPath = Paths.get( first, more );
        if( shadowPath.toString().equals( "/" ) )
        {
            return this.rootPath;
        }
        else
        {
            PathNode node = this.rootNode.getChild( shadowPath.normalize().toString() );
            return new ClasspathPath( this, shadowPath, node );
        }
    }

    @Override
    public PathMatcher getPathMatcher( String syntaxAndInput )
    {
        validateFileSystemNotClosed();
        int pos = syntaxAndInput.indexOf( ':' );
        if( pos <= 0 || pos == syntaxAndInput.length() )
        {
            throw new IllegalArgumentException();
        }

        String syntax = syntaxAndInput.substring( 0, pos );
        String input = syntaxAndInput.substring( pos + 1 );

        final Pattern pattern;
        switch( syntax )
        {
            case GLOB_SYNTAX:
                if( System.getProperty( "os.name" ).startsWith( "Windows" ) )
                {
                    pattern = Pattern.compile( Globs.toWindowsRegexPattern( input ) );
                }
                else
                {
                    pattern = Pattern.compile( Globs.toUnixRegexPattern( input ) );
                }
                return path -> pattern.matcher( path.toString() ).matches();

            case REGEX_SYNTAX:
                pattern = Pattern.compile( input );
                return path -> pattern.matcher( path.toString() ).matches();

            default:
                throw new UnsupportedOperationException( "Syntax '" + syntax + "' not recognized" );
        }
    }

    @Override
    public UserPrincipalLookupService getUserPrincipalLookupService()
    {
        validateFileSystemNotClosed();
        throw new UnsupportedOperationException();
    }

    @Override
    public WatchService newWatchService() throws IOException
    {
        validateFileSystemNotClosed();
        throw new UnsupportedOperationException();
    }

    @NotNull
    public PathNode getRootNode()
    {
        return this.rootNode;
    }

    @NotNull
    public ClasspathPath getRoot()
    {
        validateFileSystemNotClosed();
        return this.rootPath;
    }

    @NotNull
    public DirectoryStream<Path> newDirectoryStream( @NotNull Path dir,
                                                     @NotNull DirectoryStream.Filter<? super Path> filter )
    {
        validateFileSystemNotClosed();

        // validate this is our path
        if( dir.getFileSystem() != this )
        {
            throw new IllegalArgumentException( "path '" + dir + "' was not created by file-system '" + this + "'" );
        }

        return new ClasspathDirectoryStream( ( ClasspathPath ) dir, filter );
    }

    void validateFileSystemNotClosed()
    {
        if( this.closed )
        {
            throw new ClosedFileSystemException();
        }
    }

    public void checkAccess( @NotNull Path path, @NotNull AccessMode... modes ) throws IOException
    {
        // validate this is our path
        if( path.getFileSystem() != this )
        {
            throw new IllegalArgumentException( "path '" + path + "' was not created by file-system '" + this + "'" );
        }

        // find resource
        ClasspathPath classpathPath = ( ( ClasspathPath ) path ).normalize();
        classpathPath.checkAccess( modes );
    }

    private class ClasspathDirectoryStream implements DirectoryStream<Path>
    {
        @NotNull
        private final Iterator<Path> iterator;

        private boolean closed;

        public ClasspathDirectoryStream( @NotNull ClasspathPath dir, @NotNull Filter<? super Path> filter )
        {
            this.iterator = new ReflectionsClasspathDirectoryStreamIterator( dir, filter );
        }

        private void validateStreamNotClosed()
        {
            validateFileSystemNotClosed();
            if( this.closed )
            {
                throw new ClosedDirectoryStreamException();
            }
        }

        @Override
        public Iterator<Path> iterator()
        {
            validateStreamNotClosed();
            return this.iterator;
        }

        @Override
        public void close() throws IOException
        {
            this.closed = true;
        }

        private class ReflectionsClasspathDirectoryStreamIterator implements Iterator<Path>
        {
            @NotNull
            private final Filter<? super Path> filter;

            @NotNull
            private final Iterator<PathNode> resourceIterator;

            private boolean eof;

            @Nullable
            private ClasspathPath next;

            public ReflectionsClasspathDirectoryStreamIterator( @NotNull ClasspathPath root,
                                                                @NotNull Filter<? super Path> filter )
            {
                validateStreamNotClosed();

                this.filter = filter;

                PathNode pathNode = root.getPathNode();
                if( pathNode == null )
                {
                    this.resourceIterator = Collections.<PathNode>emptyList().iterator();
                }
                else
                {
                    this.resourceIterator = pathNode.getChildren().iterator();
                }
            }

            @Override
            public boolean hasNext()
            {
                validateStreamNotClosed();
                if( this.eof )
                {
                    return false;
                }
                else if( this.next != null )
                {
                    return true;
                }
                else
                {
                    while( this.next == null && this.resourceIterator.hasNext() )
                    {
                        PathNode node = this.resourceIterator.next();
                        ClasspathPath path = getPath( node.getPath() );
                        try
                        {
                            if( filter.accept( path ) )
                            {
                                this.next = path;
                                return true;
                            }
                        }
                        catch( IOException e )
                        {
                            throw new UncheckedIOException( e );
                        }
                    }
                    this.eof = true;
                    return false;
                }
            }

            @Override
            public Path next()
            {
                validateStreamNotClosed();
                if( this.hasNext() )
                {
                    if( this.next != null )
                    {
                        Path next = this.next;
                        this.next = null;
                        return next;
                    }
                    else
                    {
                        throw new IllegalStateException();
                    }
                }
                else
                {
                    throw new NoSuchElementException();
                }
            }
        }
    }
}
