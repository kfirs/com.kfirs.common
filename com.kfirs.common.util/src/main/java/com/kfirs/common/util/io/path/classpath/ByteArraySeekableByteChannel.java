/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;

import static java.lang.Integer.min;

/**
 * @author arik
 * @on 4/2/16.
 */
class ByteArraySeekableByteChannel implements SeekableByteChannel
{
    @NotNull
    private final ByteBuffer buffer;

    ByteArraySeekableByteChannel( @NotNull byte[] bytes )
    {
        this.buffer = ByteBuffer.wrap( bytes );
    }

    @Override
    public void close()
    {
        // no-op
    }

    @Override
    public boolean isOpen()
    {
        return true;
    }

    @Override
    public synchronized int read( @NotNull ByteBuffer dst ) throws IOException
    {
        int bytesToRead = min( dst.limit(), this.buffer.remaining() );

        byte[] bytes = new byte[ bytesToRead ];
        this.buffer.get( bytes );
        dst.put( bytes );

        return bytesToRead;
    }

    @Override
    public int write( @NotNull ByteBuffer src )
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public synchronized long position() throws IOException
    {
        return this.buffer.position();
    }

    @Override
    public synchronized ByteArraySeekableByteChannel position( long newPosition ) throws IOException
    {
        this.buffer.position( ( int ) newPosition );
        return this;
    }

    @Override
    public synchronized long size() throws IOException
    {
        return this.buffer.limit();
    }

    @Override
    public ByteArraySeekableByteChannel truncate( long size ) throws IOException
    {
        throw new UnsupportedOperationException();
    }
}
