/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.messages;

import com.kfirs.common.util.lang.NotNull;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ResourceLoader;

/**
 * @author arik
 * @on 2/6/17.
 */
@Configuration
@EnableConfigurationProperties( MessageSourceProperties.class )
public class MessagesSourceConfig
{
    @NotNull
    private final MessageSourceProperties messageSourceProperties;

    public MessagesSourceConfig( @NotNull MessageSourceProperties messageSourceProperties )
    {
        this.messageSourceProperties = messageSourceProperties;
    }

    @Bean
    public MessageSource messageSource( @NotNull ResourceLoader resourceLoader )
    {
        ReloadableResourceBundleMessageSource reloadableMessageSource = new ReloadableResourceBundleMessageSource();
        reloadableMessageSource.setConcurrentRefresh( this.messageSourceProperties.isConcurrentRefresh() );
        reloadableMessageSource.setResourceLoader( resourceLoader );
        reloadableMessageSource.setAlwaysUseMessageFormat( this.messageSourceProperties.isAlwaysUseMessageFormat() );
        reloadableMessageSource.setBasenames( this.messageSourceProperties.getBaseNames() );
        reloadableMessageSource.setCacheSeconds( this.messageSourceProperties.getCacheSeconds() );
        reloadableMessageSource.setDefaultEncoding( this.messageSourceProperties.getDefaultEncoding() );
        reloadableMessageSource.setFallbackToSystemLocale( this.messageSourceProperties.isFallbackToSystemLocale() );
        return new CodeDelegatingMessageSource( reloadableMessageSource );
    }
}
