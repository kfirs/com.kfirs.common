/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.csv;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;

/**
 * @author arik
 * @on 3/25/17.
 */
public class ValueParser
{
    @NotNull
    private final List<String> tokens;

    public ValueParser( @NotNull String... values )
    {
        String input = Stream.of( values ).collect( joining( "," ) );
        MappingIterator<String[]> it;
        try
        {
            CsvMapper mapper = new CsvMapper();
            mapper.enable( CsvParser.Feature.WRAP_AS_ARRAY );
            it = mapper.readerFor( String[].class ).readValues( input );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e );
        }

        List<String> tokens = new LinkedList<>();
        while( it.hasNext() )
        {
            String[] row = it.next();
            tokens.addAll( asList( row ) );
        }
        this.tokens = tokens;
    }

    public int getSize()
    {
        return this.tokens.size();
    }

    public boolean isEmpty()
    {
        return this.tokens.isEmpty();
    }

    @NotNull
    public String takeValue()
    {
        return this.tokens.remove( 0 );
    }

    @NotNull
    public List<String> takeValues()
    {
        List<String> values = new LinkedList<>( this.tokens );
        this.tokens.clear();
        return values;
    }

    @NotNull
    public String takeLastValue()
    {
        String value = takeValue();
        if( !isEmpty() )
        {
            throw new IllegalStateException( "more than one (" + getSize() + ") token is available" );
        }
        else
        {
            return value;
        }
    }
}
