/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.NoSuchFileException;
import java.nio.file.attribute.FileTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author arik
 * @on 4/3/16.
 */
class ClasspathFileAttributesImpl implements ClasspathFileAttributes
{
    @NotNull
    private final ClasspathPath path;

    @NotNull
    private final Resource resource;

    ClasspathFileAttributesImpl( @NotNull ClasspathPath path ) throws IOException
    {
        this.path = path;

        PathNode pathNode = this.path.getPathNode();
        if( pathNode == null )
        {
            throw new NoSuchFileException( "path '" + path + "' does not exist" );
        }

        Resource resource = pathNode.getFirstResource();
        if( resource == null )
        {
            throw new NoSuchFileException( "path '" + path + "' does not exist" );
        }

        this.resource = resource;
    }

    @Override
    public FileTime creationTime()
    {
        return this.resource.getCreationTime();
    }

    @Override
    public FileTime lastModifiedTime()
    {
        return this.resource.getLastModifiedTime();
    }

    @Override
    public FileTime lastAccessTime()
    {
        return this.resource.getLastAccessTime();
    }

    @Override
    public boolean isRegularFile()
    {
        return this.resource.isRegularFile();
    }

    @Override
    public boolean isDirectory()
    {
        return this.resource.isDirectory();
    }

    @Override
    public boolean isSymbolicLink()
    {
        return this.resource.isSymbolicLink();
    }

    @Override
    public boolean isOther()
    {
        return this.resource.isOther();
    }

    @Override
    public long size()
    {
        return this.resource.getSize();
    }

    @Nullable
    @Override
    public Object fileKey()
    {
        return null;
    }

    @NotNull
    public Map<String, Object> toMap( @SuppressWarnings( "UnusedParameters" ) @NotNull String attributes,
                                      @SuppressWarnings( "UnusedParameters" ) @NotNull LinkOption... options )
    {
        Map<String, Object> attrs = new HashMap<>();
        attrs.put( "creationTime", creationTime() );
        attrs.put( "lastModifiedTime", lastModifiedTime() );
        attrs.put( "lastAccessTime", lastAccessTime() );
        attrs.put( "size", size() );
        attrs.put( "isDirectory", isDirectory() );
        attrs.put( "isRegularFile", isRegularFile() );
        attrs.put( "isSymbolicLink", isSymbolicLink() );
        attrs.put( "isOther", isOther() );
        return attrs;
    }
}
