/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.resource;

import com.kfirs.common.util.lang.NotNull;
import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.autoconfigure.condition.ConditionMessage;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;

import static org.springframework.boot.autoconfigure.condition.ConditionMessage.Style.QUOTE;

/**
 * @author arik
 * @on 2/15/17.
 */
public class OnMissingResourceCondition extends SpringBootCondition
{
    @NotNull
    private final ResourceLoader defaultResourceLoader = new DefaultResourceLoader();

    @Override
    public ConditionOutcome getMatchOutcome( @NotNull ConditionContext context,
                                             @NotNull AnnotatedTypeMetadata metadata )
    {
        MultiValueMap<String, Object> attributes =
                metadata.getAllAnnotationAttributes( ConditionalOnMissingResource.class.getName(),
                                                     true );

        ResourceLoader loader =
                context.getResourceLoader() == null ? this.defaultResourceLoader : context.getResourceLoader();

        List<String> locations = new ArrayList<>();
        collectValues( locations, attributes.get( "resources" ) );
        Assert.isTrue( !locations.isEmpty(),
                       "@ConditionalOnMissingResource annotations must specify at least one resource location" );

        List<String> exist = new ArrayList<>();
        for( String location : locations )
        {
            String resource = context.getEnvironment().resolvePlaceholders( location );
            if( loader.getResource( resource ).exists() )
            {
                exist.add( location );
            }
        }
        if( !exist.isEmpty() )
        {
            return ConditionOutcome.noMatch(
                    ConditionMessage.forCondition( ConditionalOnMissingResource.class )
                                    .found( "resource", "resources" )
                                    .items( QUOTE, exist ) );
        }
        else
        {
            return ConditionOutcome.match(
                    ConditionMessage.forCondition( ConditionalOnMissingResource.class )
                                    .didNotFind( "location", "locations" )
                                    .items( locations ) );
        }
    }

    private void collectValues( List<String> names, List<Object> values )
    {
        for( Object value : values )
        {
            for( Object item : ( Object[] ) value )
            {
                names.add( ( String ) item );
            }
        }
    }
}
