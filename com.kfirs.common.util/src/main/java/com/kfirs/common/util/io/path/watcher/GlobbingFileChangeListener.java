/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.watcher;

import com.kfirs.common.util.io.path.matcher.PathMatcher;
import com.kfirs.common.util.lang.NotNull;
import java.nio.file.Path;
import java.util.List;

import static java.nio.file.Files.isDirectory;
import static java.nio.file.Files.isRegularFile;

/**
 * @author arik
 * @on 5/4/16.
 */
class GlobbingFileChangeListener implements FileChangeListener
{
    @NotNull
    private final PathMatcher pathMatcher;

    @NotNull
    private final FileChangeListener target;

    @NotNull
    private final List<String> globs;

    GlobbingFileChangeListener( @NotNull PathMatcher pathMatcher,
                                @NotNull FileChangeListener target,
                                @NotNull List<String> globs )
    {
        this.pathMatcher = pathMatcher;
        this.target = target;
        this.globs = globs;
    }

    @Override
    public void fileChanged( @NotNull FileChangedEvent event )
    {
        Path watchedPath = event.getWatchedPath();
        if( isDirectory( watchedPath ) && isRegularFile( event.getAbsoluteChangedPath() ) )
        {
            Path relativeChangedPath = event.getRelativeChangedPath();
            if( this.pathMatcher.match( relativeChangedPath.toString(), this.globs ) )
            {
                this.target.fileChanged( event );
            }
        }
    }
}
