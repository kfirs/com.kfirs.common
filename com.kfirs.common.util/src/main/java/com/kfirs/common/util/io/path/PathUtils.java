/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * @author arik
 * @on 4/16/16.
 */
public final class PathUtils
{
    public static void createDirectories( @NotNull Path path )
    {
        try
        {
            Files.createDirectories( path );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( "failed creating '" + path + "'", e );
        }
    }

    public static void delete( @NotNull Path path )
    {
        if( Files.exists( path ) )
        {
            try
            {
                Files.walkFileTree( path, new SimpleFileVisitor<Path>()
                {
                    @Override
                    public FileVisitResult visitFile( @NotNull Path file, @NotNull BasicFileAttributes attrs )
                            throws IOException
                    {
                        Files.delete( file );
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory( @NotNull Path dir, IOException exc ) throws IOException
                    {
                        if( exc != null )
                        {
                            throw exc;
                        }
                        else
                        {
                            Files.delete( dir );
                            return FileVisitResult.CONTINUE;
                        }
                    }
                } );
            }
            catch( IOException e )
            {
                throw new UncheckedIOException( e );
            }
        }
    }

    public static void copy( @NotNull Path src, @NotNull Path dst, @NotNull CopyOption... options ) throws IOException
    {
        copy( src, dst, path -> true, options );
    }

    public static void copy( @NotNull Path src,
                             @NotNull Path dst,
                             @NotNull Predicate<Path> filter,
                             @NotNull CopyOption... options ) throws IOException
    {
        Path absoluteSrc = src.toAbsolutePath();
        Path absoluteDst = dst.toAbsolutePath();

        // TEST: test filter

        Set<CopyOption> opts = new HashSet<>();
        Collections.addAll( opts, options );

        if( Files.notExists( absoluteSrc ) )
        {
            throw new NoSuchFileException( "'" + absoluteSrc + "' does not exist" );
        }
        else if( filter.test( absoluteSrc ) )
        {
            if( Files.isRegularFile( absoluteSrc ) )
            {
                copyFile( filter, absoluteSrc, absoluteDst, opts, options );
            }
            else if( Files.isDirectory( absoluteSrc ) )
            {
                copyDirectory( filter, absoluteSrc, absoluteDst, options );
            }
            else
            {
                throw new IllegalArgumentException( "unsupported file-type of '" + absoluteSrc + "'" );
            }
        }
    }

    private static void copyFile( @NotNull Predicate<Path> filter,
                                  @NotNull Path absoluteSrc,
                                  @NotNull Path absoluteDst,
                                  @NotNull Set<CopyOption> opts,
                                  @NotNull CopyOption[] options ) throws IOException
    {
        if( Files.exists( absoluteDst ) )
        {
            if( Files.isDirectory( absoluteDst ) )
            {
                // SRC FILE INTO DST DIR
                copy( absoluteSrc, absoluteDst.resolve( absoluteSrc.getFileName().toString() ), filter, options );
            }
            else if( Files.isRegularFile( absoluteDst ) )
            {
                // SRC FILE INTO DST FILE

                // if REPLACE_EXISTING is not specified, fail since dest exists
                if( !opts.contains( REPLACE_EXISTING ) )
                {
                    throw new FileAlreadyExistsException( absoluteDst.toString() );
                }

                // if ONLY_IF_NEWER is specific, and dest is newer, skip
                if( opts.contains( ExtCopyOption.ONLY_IF_NEWER ) )
                {
                    Instant srcLastMod = Files.getLastModifiedTime( absoluteSrc ).toInstant();
                    Instant dstLastMod = Files.getLastModifiedTime( absoluteDst ).toInstant();
                    if( srcLastMod.equals( dstLastMod ) || srcLastMod.isBefore( dstLastMod ) )
                    {
                        // dest is newer, skip copy
                        return;
                    }
                }

                // copy
                Files.copy( absoluteSrc, absoluteDst, REPLACE_EXISTING );
            }
            else
            {
                throw new IllegalStateException();
            }
        }
        else
        {
            // SRC FILE INTO DST FILE
            Files.createDirectories( absoluteDst.getParent() );
            Files.copy( absoluteSrc, absoluteDst );
        }
    }

    private static void copyDirectory( @NotNull Predicate<Path> filter,
                                       @NotNull Path absoluteSrc,
                                       @NotNull Path absoluteDst,
                                       @NotNull CopyOption[] options ) throws IOException
    {
        if( Files.exists( absoluteDst ) && !Files.isDirectory( absoluteDst ) )
        {
            throw new FileAlreadyExistsException( "cannot copy directory '" + absoluteSrc + "' into a file ('" + absoluteDst + "')" );
        }

        // SRC DIR INTO DST DIR (/my/src to /dst becomes /dst)
        Files.createDirectories( absoluteDst );
        Files.walk( absoluteSrc )
             .filter( path -> !absoluteSrc.equals( path ) )
             .filter( filter )
             .forEach( srcPath ->
                       {
                           Path relativeSrc = absoluteSrc.relativize( srcPath );
                           Path target = absoluteDst.resolve( relativeSrc.toString() );
                           try
                           {
                               if( Files.isDirectory( srcPath ) )
                               {
                                   Files.createDirectories( target );
                               }
                               else if( Files.isRegularFile( srcPath ) )
                               {
                                   copy( srcPath, target, options );
                               }
                               else
                               {
                                   throw new IllegalStateException();
                               }
                           }
                           catch( IOException e )
                           {
                               throw new UncheckedIOException( e );
                           }
                       } );
    }

    private PathUtils()
    {
    }
}
