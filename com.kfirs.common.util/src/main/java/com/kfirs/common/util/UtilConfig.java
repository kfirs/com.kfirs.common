/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util;

import com.kfirs.common.util.aop.AopConfig;
import com.kfirs.common.util.git.GitReplacementConfig;
import com.kfirs.common.util.io.path.matcher.PathMatcherConfig;
import com.kfirs.common.util.io.path.watcher.FileWatcherConfig;
import com.kfirs.common.util.messages.MessagesSourceConfig;
import com.kfirs.common.util.reflection.ReflectionConfig;
import com.kfirs.common.util.system.SystemStartupTimeProviderConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author arik
 * @on 3/26/17.
 */
@Configuration
@Import( {
                 SystemStartupTimeProviderConfig.class,
                 GitReplacementConfig.class,
                 PathMatcherConfig.class,
                 FileWatcherConfig.class,
                 MessagesSourceConfig.class,
                 AopConfig.class,
                 ReflectionConfig.class
         } )
public class UtilConfig
{
}
