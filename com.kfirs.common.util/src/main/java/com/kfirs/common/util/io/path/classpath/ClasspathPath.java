/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileAttributeView;
import java.util.*;
import java.util.function.Consumer;

import static java.lang.System.identityHashCode;

/**
 * @author arik
 * @on 4/1/16.
 */
class ClasspathPath implements Path
{
    @NotNull
    private final ClasspathFileSystem fileSystem;

    @NotNull
    private final Path shadowPath;

    @Nullable
    private final PathNode pathNode;

    // TEST: test ClasspathPath using the table below
    // --------------------------------
    // TYPE                 PATH    NAME-COUNT  NAMES       FILE-NAME   PARENT
    // root component       "/"     0           []          null        null
    // empty component      ""      1           [""]        ""          null
    // absolute component   "/a"    1           ["a"]       "a"         "/"
    // relative component   "a"     1           ["a"]       "a"         null
    // absolute component   "/a/b"  2           ["a","b"]   "b"         "/a"
    // relative component   "a/b"   2           ["a","b"]   "b"         "a"

    ClasspathPath( @NotNull ClasspathFileSystem fileSystem, @NotNull Path shadowPath, @Nullable PathNode pathNode )
    {
        this.fileSystem = fileSystem;
        this.shadowPath = shadowPath;
        this.pathNode = pathNode;
    }

    @Override
    public boolean equals( Object o )
    {
        if( this == o )
        {
            return true;
        }
        if( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        ClasspathPath that = ( ClasspathPath ) o;
        return Objects.equals( this.fileSystem, that.fileSystem ) && this.shadowPath.equals( that.shadowPath );
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( this.fileSystem, this.shadowPath );
    }

    @Override
    public String toString()
    {
        return this.shadowPath.toString();
    }

    @Nullable
    PathNode getPathNode()
    {
        return pathNode;
    }

    @NotNull
    @Override
    public ClasspathFileSystem getFileSystem()
    {
        return this.fileSystem;
    }

    @Override
    public int compareTo( Path other )
    {
        if( this.fileSystem != other.getFileSystem() )
        {
            throw new IllegalArgumentException();
        }
        else if( !ClasspathPath.class.isInstance( other ) )
        {
            throw new IllegalArgumentException();
        }
        else
        {
            ClasspathPath that = ( ClasspathPath ) other;
            return this.shadowPath.compareTo( that.shadowPath );
        }
    }

    @Override
    public boolean isAbsolute()
    {
        return this.shadowPath.isAbsolute();
    }

    @Nullable
    @Override
    public ClasspathPath getRoot()
    {
        return this.shadowPath.getRoot() == null ? null : this.fileSystem.getRoot();
    }

    @Nullable
    @Override
    public ClasspathPath getFileName()
    {
        Path shadowFileName = this.shadowPath.getFileName();
        return shadowFileName == null ? null : this.fileSystem.getPath( shadowFileName.toString() );
    }

    @Nullable
    @Override
    public ClasspathPath getParent()
    {
        Path shadowParent = this.shadowPath.getParent();
        return shadowParent == null ? null : this.fileSystem.getPath( shadowParent.toString() );
    }

    @Override
    public int getNameCount()
    {
        return this.shadowPath.getNameCount();
    }

    @Nullable
    @Override
    public ClasspathPath getName( int index )
    {
        Path shadowName = this.shadowPath.getName( index );
        return shadowName == null ? null : this.fileSystem.getPath( shadowName.toString() );
    }

    @Nullable
    @Override
    public ClasspathPath subpath( int beginIndex, int endIndex )
    {
        Path shadowSubPath = this.shadowPath.subpath( beginIndex, endIndex );
        return shadowSubPath == null ? null : this.fileSystem.getPath( shadowSubPath.toString() );
    }

    @Override
    public boolean startsWith( Path other )
    {
        if( !ClasspathPath.class.isInstance( other ) )
        {
            return false;
        }

        ClasspathPath that = ( ClasspathPath ) other;
        return this.shadowPath.startsWith( that.shadowPath );
    }

    @Override
    public boolean startsWith( String other )
    {
        return startsWith( this.fileSystem.getPath( other ) );
    }

    @Override
    public boolean endsWith( Path other )
    {
        if( !ClasspathPath.class.isInstance( other ) )
        {
            return false;
        }

        ClasspathPath that = ( ClasspathPath ) other;
        return this.shadowPath.endsWith( that.shadowPath );
    }

    @Override
    public boolean endsWith( String other )
    {
        return endsWith( this.fileSystem.getPath( other ) );
    }

    @Override
    public ClasspathPath normalize()
    {
        Path normalizedShadowPath = this.shadowPath.normalize();
        return this.fileSystem.getPath( normalizedShadowPath.toString() );
    }

    @Override
    public ClasspathPath resolve( Path other )
    {
        if( !ClasspathPath.class.isInstance( other ) )
        {
            throw new IllegalArgumentException();
        }

        ClasspathPath that = ( ClasspathPath ) other;
        Path resolvedShadowPath = this.shadowPath.resolve( that.shadowPath );
        return this.fileSystem.getPath( resolvedShadowPath.toString() );
    }

    @Override
    public ClasspathPath resolve( String other )
    {
        return resolve( this.fileSystem.getPath( other ) );
    }

    @Override
    public ClasspathPath resolveSibling( Path other )
    {
        if( !ClasspathPath.class.isInstance( other ) )
        {
            throw new IllegalArgumentException();
        }

        ClasspathPath that = ( ClasspathPath ) other;
        Path resolvedSiblingShadowPath = this.shadowPath.resolveSibling( that.shadowPath );
        return this.fileSystem.getPath( resolvedSiblingShadowPath.toString() );
    }

    @Override
    public ClasspathPath resolveSibling( String other )
    {
        return resolveSibling( this.fileSystem.getPath( other ) );
    }

    @Override
    public ClasspathPath relativize( Path other )
    {
        if( !ClasspathPath.class.isInstance( other ) )
        {
            throw new IllegalArgumentException();
        }

        ClasspathPath that = ( ClasspathPath ) other;
        Path relativizedShadowPath = this.shadowPath.relativize( that.shadowPath );
        return this.fileSystem.getPath( relativizedShadowPath.toString() );
    }

    @Override
    public URI toUri()
    {
        int classLoaderId = identityHashCode( this.fileSystem.getClassLoader() );
        return URI.create( "classpath://" + classLoaderId + toAbsolutePath().shadowPath );
    }

    @Override
    public ClasspathPath toAbsolutePath()
    {
        return isAbsolute() ? this : this.fileSystem.getPath( Paths.get( "/" + this.shadowPath ).toString() );
    }

    @Override
    public Iterator<Path> iterator()
    {
        List<Path> paths = new LinkedList<>();
        Consumer<Path> collector = path -> paths.add( this.fileSystem.getPath( path.toString() ) );
        this.shadowPath.iterator().forEachRemaining( collector );
        return paths.iterator();
    }

    @NotNull
    ByteArrayFileChannel newFileChannel( @NotNull Set<? extends OpenOption> options,
                                         @NotNull FileAttribute<?>... attrs )
            throws IOException
    {
        // if no node, we don't exist
        if( this.pathNode == null )
        {
            throw new NoSuchFileException( "path '" + this + "' does not exist" );
        }
        else
        {
            return this.pathNode.newFileChannel( options, attrs );
        }
    }

    @NotNull
    ByteArraySeekableByteChannel newByteChannel( @NotNull Set<? extends OpenOption> options,
                                                 @NotNull FileAttribute<?>... attrs )
            throws IOException
    {
        // if no node, we don't exist
        if( this.pathNode == null )
        {
            throw new NoSuchFileException( "path '" + this + "' does not exist" );
        }
        else
        {
            return this.pathNode.newByteChannel( options, attrs );
        }
    }

    void checkAccess( @NotNull AccessMode... modes ) throws IOException
    {
        // validate options
        for( AccessMode mode : modes )
        {
            if( mode == AccessMode.WRITE || mode == AccessMode.EXECUTE )
            {
                throw new IOException( "unsupported access mode option '" + mode + "'" );
            }
        }

        // find resource
        if( this.pathNode == null )
        {
            throw new NoSuchFileException( "path '" + this + "' does not exist" );
        }
        else if( this.pathNode.getResources().isEmpty() && this.pathNode.getChildren().isEmpty() )
        {
            throw new NoSuchFileException( "path '" + this + "' does not exist" );
        }
    }

    @Nullable
    public <V extends FileAttributeView> V getFileAttributesView( @NotNull Class<V> type,
                                                                  @NotNull LinkOption[] options )
    {
        for( LinkOption option : options )
        {
            if( option == LinkOption.NOFOLLOW_LINKS )
            {
                throw new IllegalArgumentException( LinkOption.NOFOLLOW_LINKS + " is not supported" );
            }
        }

        if( BasicFileAttributeView.class.equals( type ) || ClasspathFileAttributesView.class.equals( type ) )
        {
            return type.cast( new ClasspathFileAttributesViewImpl( this ) );
        }
        else
        {
            return null;
        }
    }

    @Nullable
    public <A extends BasicFileAttributes> A readAttributes( @NotNull Class<A> type,
                                                             @SuppressWarnings( "UnusedParameters" )
                                                                     LinkOption... options ) throws IOException
    {
        if( BasicFileAttributes.class.equals( type ) || ClasspathFileAttributes.class.equals( type ) )
        {
            return type.cast( new ClasspathFileAttributesImpl( this ) );
        }
        else
        {
            return null;
        }
    }

    @Override
    public Path toRealPath( LinkOption... options ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public File toFile()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public WatchKey register( WatchService watcher, WatchEvent.Kind<?>[] events, WatchEvent.Modifier... modifiers )
            throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public WatchKey register( WatchService watcher, WatchEvent.Kind<?>... events ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public boolean isHidden()
    {
        return false;
    }

}
