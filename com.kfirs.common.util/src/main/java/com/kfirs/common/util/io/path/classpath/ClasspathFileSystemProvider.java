/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileAttributeView;
import java.nio.file.spi.FileSystemProvider;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Provider for classpath {@link Path} file-system. <p> <p>NOTE: this class MUST be public, since it's a service used by
 * {@link java.util.ServiceLoader}.</p>
 *
 * @author arik
 * @on 4/1/16.
 */
public class ClasspathFileSystemProvider extends FileSystemProvider
{
    @NotNull
    private final ConcurrentMap<ClassLoader, ClasspathFileSystem> fileSystems = new ConcurrentHashMap<>();

    @Override
    public String getScheme()
    {
        return "classpath";
    }

    @Override
    public synchronized ClasspathFileSystem newFileSystem( URI uri, Map<String, ?> env ) throws IOException
    {
        validateScheme( uri );

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();

        Object classLoaderEntryValue = env.get( ClasspathUtils.CLASS_LOADER_KEY );
        if( classLoaderEntryValue != null )
        {
            if( classLoaderEntryValue instanceof ClassLoader )
            {
                classLoader = ( ClassLoader ) classLoaderEntryValue;
            }
            else
            {
                String msg = "env entry '" + ClasspathUtils.CLASS_LOADER_KEY + "' is not an instance of ClassLoader";
                throw new IllegalArgumentException( msg );
            }
        }

        if( this.fileSystems.containsKey( classLoader ) )
        {
            throw new FileSystemAlreadyExistsException();
        }
        else
        {
            ClasspathFileSystem fileSystem = new ClasspathFileSystem( this, classLoader );
            this.fileSystems.put( classLoader, fileSystem );
            return fileSystem;
        }
    }

    @Override
    public ClasspathFileSystem getFileSystem( URI uri )
    {
        ClasspathFileSystem fileSystem = getClasspathFileSystem( uri );
        if( fileSystem == null )
        {
            throw new FileSystemNotFoundException();
        }
        else
        {
            return fileSystem;
        }
    }

    void closeFileSystem( @NotNull ClasspathFileSystem fileSystem )
    {
        this.fileSystems.remove( fileSystem.getClassLoader() );
    }

    @Override
    public Path getPath( URI uri )
    {
        ClasspathFileSystem fileSystem = getClasspathFileSystem( uri );
        if( fileSystem == null )
        {
            throw new FileSystemNotFoundException( "no file-system found for URI '" + uri + "'" );
        }
        else
        {
            return fileSystem.getPath( uri.getPath() );
        }
    }

    @Override
    public ByteArrayFileChannel newFileChannel( Path path,
                                                Set<? extends OpenOption> options,
                                                FileAttribute<?>... attrs )
            throws IOException
    {
        if( path instanceof ClasspathPath )
        {
            ClasspathPath classpathPath = ( ClasspathPath ) path;
            return classpathPath.newFileChannel( options, attrs );
        }
        throw new IllegalArgumentException( "path does not belong to a classpath file-system" );
    }

    @Override
    public ByteArraySeekableByteChannel newByteChannel( Path path,
                                                        Set<? extends OpenOption> options,
                                                        FileAttribute<?>... attrs )
            throws IOException
    {
        if( path instanceof ClasspathPath )
        {
            ClasspathPath classpathPath = ( ClasspathPath ) path;
            return classpathPath.newByteChannel( options, attrs );
        }
        throw new IllegalArgumentException( "path does not belong to a classpath file-system" );
    }

    @Override
    public DirectoryStream<Path> newDirectoryStream( @NotNull Path dir,
                                                     @NotNull DirectoryStream.Filter<? super Path> filter )
            throws IOException
    {
        FileSystem fileSystem = dir.getFileSystem();
        if( fileSystem instanceof ClasspathFileSystem )
        {
            ClasspathFileSystem classpathFileSystem = ( ClasspathFileSystem ) fileSystem;
            return classpathFileSystem.newDirectoryStream( dir, filter );
        }
        throw new IllegalArgumentException( "path does not belong to a classpath file-system" );
    }

    @Override
    public void copy( Path source, Path target, CopyOption... options ) throws IOException
    {
        // this method is only called with BOTH the source & target are from this provider; since classpath file systems
        // are read-only, there's no scenario where a target can be a classpath file, we can safely not implement this
        throw new UnsupportedOperationException();
    }

    @Override
    public void createDirectory( Path dir, FileAttribute<?>... attrs ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete( Path path ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void move( Path source, Path target, CopyOption... options ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isSameFile( Path path, Path path2 ) throws IOException
    {
        return Objects.equals( path, path2 );
    }

    @Override
    public boolean isHidden( Path path ) throws IOException
    {
        if( path instanceof ClasspathPath )
        {
            ClasspathPath classpathPath = ( ClasspathPath ) path;
            return classpathPath.isHidden();
        }
        throw new IllegalArgumentException( "path does not belong to a classpath file-system" );
    }

    @Override
    public FileStore getFileStore( Path path ) throws IOException
    {
        FileSystem fileSystem = path.getFileSystem();
        if( fileSystem instanceof ClasspathFileSystem )
        {
            ClasspathFileSystem classpathFileSystem = ( ClasspathFileSystem ) fileSystem;
            return classpathFileSystem.getFileStore();
        }
        throw new IllegalArgumentException( "path '" + path + "' is not a classpath path" );
    }

    @Override
    public void checkAccess( Path path, AccessMode... modes ) throws IOException
    {
        FileSystem fileSystem = path.getFileSystem();
        if( fileSystem instanceof ClasspathFileSystem )
        {
            ClasspathFileSystem classpathFileSystem = ( ClasspathFileSystem ) fileSystem;
            classpathFileSystem.checkAccess( path, modes );
        }
        else
        {
            throw new IllegalArgumentException( "path '" + path + "' is not a classpath path" );
        }
    }

    @Nullable
    @Override
    public <V extends FileAttributeView> V getFileAttributeView( Path path, Class<V> type, LinkOption... options )
    {
        if( path instanceof ClasspathPath )
        {
            ClasspathPath classpathPath = ( ClasspathPath ) path;
            return classpathPath.getFileAttributesView( type, options );
        }
        else
        {
            throw new IllegalArgumentException( "path '" + path + "' does not belong to a classpath file-system" );
        }
    }

    @Nullable
    @Override
    public <A extends BasicFileAttributes> A readAttributes( Path path, Class<A> type, LinkOption... options )
            throws IOException
    {
        if( path instanceof ClasspathPath )
        {
            ClasspathPath classpathPath = ( ClasspathPath ) path;
            return classpathPath.readAttributes( type, options );
        }
        else
        {
            throw new IllegalArgumentException( "path '" + path + "' does not belong to a classpath file-system" );
        }
    }

    @Override
    public Map<String, Object> readAttributes( Path path, String attributes, LinkOption... options ) throws IOException
    {
        ClasspathFileAttributes attrs = readAttributes( path, ClasspathFileAttributes.class, options );
        if( attrs == null )
        {
            return Collections.emptyMap();
        }
        else
        {
            return ( ( ClasspathFileAttributesImpl ) attrs ).toMap( attributes );
        }
    }

    @Override
    public void setAttribute( Path path, String attribute, Object value, LinkOption... options ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    private void validateScheme( @NotNull URI uri )
    {
        String scheme = uri.getScheme();
        if( !scheme.equalsIgnoreCase( "classpath" ) )
        {
            throw new IllegalArgumentException( "not a classpath file-system URI" );
        }
    }

    @Nullable
    private ClasspathFileSystem getClasspathFileSystem( @NotNull URI uri )
    {
        validateScheme( uri );

        int classLoaderId = getClassLoaderIdFromUri( uri );
        for( Map.Entry<ClassLoader, ClasspathFileSystem> entry : this.fileSystems.entrySet() )
        {
            int id = System.identityHashCode( entry.getKey() );
            if( id == classLoaderId )
            {
                return entry.getValue();
            }
        }

        return null;
    }

    private int getClassLoaderIdFromUri( @NotNull URI uri )
    {
        try
        {
            return Integer.parseInt( uri.getHost() );
        }
        catch( NumberFormatException e )
        {
            throw new IllegalArgumentException( "not a classpath file-system URI (illegal class-loader ID '" + uri.getHost() + "')" );
        }
    }
}
