/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.aop;

import com.kfirs.common.util.lang.NotNull;
import org.aspectj.bridge.AbortException;
import org.aspectj.bridge.IMessage;
import org.springframework.aop.aspectj.AspectJWeaverMessageHandler;

/**
 * @author arik
 * @on 2/1/17.
 */
public class SpringBootAwareAspectJWeaverMessageHandler extends AspectJWeaverMessageHandler
{
    private static boolean springBootInitialized = Boolean.getBoolean( "aopDebug" );

    public static void setSpringBootInitialized( boolean springBootInitialized )
    {
        SpringBootAwareAspectJWeaverMessageHandler.springBootInitialized = springBootInitialized;
    }

    @Override
    public boolean handleMessage( @NotNull IMessage message ) throws AbortException
    {
        return springBootInitialized && super.handleMessage( message );
    }
}
