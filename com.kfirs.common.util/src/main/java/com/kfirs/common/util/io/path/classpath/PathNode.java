/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NoSuchFileException;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileAttribute;
import java.util.*;

/**
 * @author arik
 * @on 4/16/16.
 */
class PathNode
{
    @NotNull
    private final ClasspathFileSystem fileSystem;

    @NotNull
    private final String name;

    @Nullable
    private final PathNode parent;

    @NotNull
    private final Map<String, PathNode> children = new TreeMap<String, PathNode>();

    @Nullable
    private Resource firstResource;

    @NotNull
    private Set<Resource> resources = Collections.emptySet();

    PathNode( @NotNull ClasspathFileSystem fileSystem, @NotNull String name, @Nullable PathNode parent )
    {
        this.fileSystem = fileSystem;
        this.name = name;
        this.parent = parent;
    }

    @NotNull
    String getName()
    {
        return this.name;
    }

    @NotNull
    Optional<PathNode> getParent()
    {
        return Optional.ofNullable( this.parent );
    }

    @Nullable
    Resource getFirstResource()
    {
        return this.firstResource;
    }

    @NotNull
    Set<Resource> getResources()
    {
        return this.resources;
    }

    synchronized void addResource( @NotNull Resource resource )
    {
        if( this.firstResource == null )
        {
            this.firstResource = resource;
        }
        if( !this.resources.contains( resource ) )
        {
            // TODO arik: slow recreation of set each time... 
            Set<Resource> newResources = new LinkedHashSet<>( this.resources );
            newResources.add( resource );
            this.resources = newResources;
        }
    }

    @Nullable
    PathNode getChild( @NotNull String name )
    {
        if( name.isEmpty() )
        {
            throw new IllegalArgumentException();
        }
        else if( name.startsWith( "/" ) )
        {
            return this.fileSystem.getRootNode().getChild( name.substring( 1 ) );
        }

        int firstSeparatorIndex = name.indexOf( '/' );
        if( firstSeparatorIndex < 0 )
        {
            return this.children.get( name );
        }
        else
        {
            String first = name.substring( 0, firstSeparatorIndex );
            PathNode directChild = this.children.get( first );
            if( directChild != null )
            {
                return directChild.getChild( name.substring( firstSeparatorIndex + 1 ) );
            }
            else
            {
                return null;
            }
        }
    }

    @NotNull
    Collection<PathNode> getChildren()
    {
        return this.children.values();
    }

    @NotNull
    synchronized PathNode addChild( @NotNull String name )
    {
        if( name.isEmpty() )
        {
            throw new IllegalArgumentException();
        }
        else if( name.startsWith( "/" ) )
        {
            return this.fileSystem.getRootNode().addChild( name.substring( 1 ) );
        }
        else
        {
            while( name.endsWith( "/" ) )
            {
                name = name.substring( 0, name.length() - 1 );
            }
        }

        int firstSeparatorIndex = name.indexOf( '/' );
        if( firstSeparatorIndex < 0 )
        {
            PathNode childNode = this.children.get( name );
            if( childNode == null )
            {
                childNode = new PathNode( this.fileSystem, name, this );
                this.children.put( name, childNode );
            }
            return childNode;
        }
        else
        {
            String first = name.substring( 0, firstSeparatorIndex );
            String rest = name.substring( firstSeparatorIndex + 1 );

            PathNode directChild = this.children.get( first );
            if( directChild == null )
            {
                directChild = new PathNode( this.fileSystem, first, this );
                this.children.put( first, directChild );
            }
            return directChild.addChild( rest );
        }
    }

    @NotNull
    String getPath()
    {
        if( this.parent != null )
        {
            return this.parent.getPath() + "/" + this.name;
        }
        else
        {
            return this.name;
        }
    }

    @NotNull
    ByteArrayFileChannel newFileChannel( @NotNull Set<? extends OpenOption> options,
                                         @NotNull FileAttribute<?>... attrs ) throws IOException
    {
        this.fileSystem.validateFileSystemNotClosed();

        // validate options
        for( OpenOption option : options )
        {
            if( option instanceof StandardOpenOption )
            {
                StandardOpenOption stdOpenOption = ( StandardOpenOption ) option;
                switch( stdOpenOption )
                {
                    case WRITE:
                    case APPEND:
                    case TRUNCATE_EXISTING:
                    case CREATE:
                    case CREATE_NEW:
                    case DELETE_ON_CLOSE:
                    case SPARSE:
                    case SYNC:
                    case DSYNC:
                        throw new IllegalArgumentException( "unsupported open option '" + stdOpenOption + "'" );
                    default:
                        break;
                }
            }
            else
            {
                throw new IllegalArgumentException( "unsupported open option '" + option + "'" );
            }
        }

        // validate attributes
        if( attrs.length > 0 )
        {
            throw new IllegalArgumentException( "passing attributes is not supported" );
        }

        // create channel
        return new ByteArrayFileChannel( readAllBytesFromPath() );
    }

    @NotNull
    ByteArraySeekableByteChannel newByteChannel( @NotNull Set<? extends OpenOption> options,
                                                 @NotNull FileAttribute<?>... attrs ) throws IOException
    {
        this.fileSystem.validateFileSystemNotClosed();

        // validate options
        for( OpenOption option : options )
        {
            if( option instanceof StandardOpenOption )
            {
                StandardOpenOption stdOpenOption = ( StandardOpenOption ) option;
                switch( stdOpenOption )
                {
                    case WRITE:
                    case APPEND:
                    case TRUNCATE_EXISTING:
                    case CREATE:
                    case CREATE_NEW:
                    case DELETE_ON_CLOSE:
                    case SPARSE:
                    case SYNC:
                    case DSYNC:
                        throw new IllegalArgumentException( "unsupported open option '" + stdOpenOption + "'" );
                    default:
                        break;
                }
            }
            else
            {
                throw new IllegalArgumentException( "unsupported open option '" + option + "'" );
            }
        }

        // validate attributes
        if( attrs.length > 0 )
        {
            throw new IllegalArgumentException( "passing attributes is not supported" );
        }

        // create channel
        return new ByteArraySeekableByteChannel( readAllBytesFromPath() );
    }

    @NotNull
    private byte[] readAllBytesFromPath() throws IOException
    {
        for( Resource resource : this.resources )
        {
            if( resource.isRegularFile() )
            {
                byte[] bytes;
                try( InputStream inputStream = resource.getInputStream() )
                {
                    ByteArrayOutputStream out = new ByteArrayOutputStream( 8196 );

                    byte[] buffer = new byte[ 8196 ];
                    int bytesRead;
                    while( ( bytesRead = inputStream.read( buffer ) ) != -1 )
                    {
                        out.write( buffer, 0, bytesRead );
                    }
                    out.flush();

                    bytes = out.toByteArray();
                    return bytes;
                }
            }
        }
        throw new NoSuchFileException( "could not read from '" + getPath() + "' - not a readable resource" );
    }
}
