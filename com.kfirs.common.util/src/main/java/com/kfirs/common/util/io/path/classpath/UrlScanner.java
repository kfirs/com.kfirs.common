/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.net.URL;

/**
 * @author arik
 * @on 4/28/16.
 */
abstract class UrlScanner
{
    abstract boolean supports( @NotNull ClassLoader classLoader, @NotNull URL url );

    abstract void scan( @NotNull PathNode root, @NotNull ClassLoader classLoader, @NotNull URL url ) throws IOException;
}
