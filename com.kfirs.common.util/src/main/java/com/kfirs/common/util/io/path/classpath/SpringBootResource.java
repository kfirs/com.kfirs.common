/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.attribute.FileTime;
import java.util.Objects;
import java.util.Optional;
import java.util.jar.JarEntry;

import static java.time.Instant.EPOCH;

/**
 * @author arik
 * @on 6/30/17.
 */
class SpringBootResource extends Resource
{
    @NotNull
    private final URL url;

    @NotNull
    private final FileTime creationTime;

    @NotNull
    private final FileTime lastModifiedTime;

    @NotNull
    private final FileTime lastAccessTime;

    private final long size;

    SpringBootResource( @NotNull JarEntry jarEntry, @NotNull URL url )
    {
        this.url = url;
        this.creationTime = Optional.ofNullable( jarEntry.getCreationTime() ).orElseGet( () -> FileTime.from( EPOCH ) );
        this.lastModifiedTime = Optional.ofNullable( jarEntry.getLastModifiedTime() ).orElseGet( () -> FileTime.from( EPOCH ) );
        this.lastAccessTime = Optional.ofNullable( jarEntry.getLastAccessTime() ).orElseGet( () -> FileTime.from( EPOCH ) );
        this.size = jarEntry.getSize();
    }

    @Override
    public boolean equals( Object o )
    {
        if( this == o )
        {
            return true;
        }
        if( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        SpringBootResource resource = ( SpringBootResource ) o;
        return Objects.equals( this.url, resource.url );
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( this.url );
    }

    @Override
    public String toString()
    {
        return getClass().getSimpleName() + "[" + this.url + "]";
    }

    @NotNull
    @Override
    InputStream getInputStream() throws IOException
    {
        return this.url.openStream();
    }

    @NotNull
    @Override
    FileTime getCreationTime()
    {
        return this.creationTime;
    }

    @NotNull
    @Override
    FileTime getLastModifiedTime()
    {
        return this.lastModifiedTime;
    }

    @NotNull
    @Override
    FileTime getLastAccessTime()
    {
        return this.lastAccessTime;
    }

    @Override
    boolean isDirectory()
    {
        return this.url.toExternalForm().endsWith( "/" );
    }

    @Override
    boolean isRegularFile()
    {
        return !this.url.toExternalForm().endsWith( "/" );
    }

    @Override
    boolean isSymbolicLink()
    {
        return false;
    }

    @Override
    boolean isOther()
    {
        return false;
    }

    @Override
    long getSize()
    {
        return this.size;
    }
}
