/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.Instant.EPOCH;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableSet;

/**
 * @author arik
 * @on 4/28/16.
 */
class JarFileUrlScanner extends UrlScanner
{
    private static final Set<Pattern> SUPPORTED_PATTERNS;

    static
    {
        SUPPORTED_PATTERNS = unmodifiableSet( new LinkedHashSet<>( asList(
                Pattern.compile( "^file:([^!]+\\.jar)$" ),
                Pattern.compile( "^jar:file:([^!]+\\.jar)!/$" ),
                Pattern.compile( "^file:([^!]+\\.jar)!/$" )
        ) ) );
    }

    @Override
    boolean supports( @NotNull ClassLoader classLoader, @NotNull URL url )
    {
        String externalUrlForm = url.toExternalForm();
        for( Pattern pattern : SUPPORTED_PATTERNS )
        {
            if( pattern.matcher( externalUrlForm ).matches() )
            {
                return true;
            }
        }
        return false;
    }

    @Override
    void scan( @NotNull PathNode root, @NotNull ClassLoader classLoader, @NotNull URL url ) throws IOException
    {
        String externalUrlForm = url.toExternalForm();
        for( Pattern pattern : SUPPORTED_PATTERNS )
        {
            Matcher matcher = pattern.matcher( externalUrlForm );
            if( matcher.matches() )
            {
                String pathName = matcher.group( 1 );
                JarFile jarFile = new JarFile( Paths.get( pathName ).toFile() );
                jarFile.stream()
                       .filter( jarEntry -> !jarEntry.getName().isEmpty() )
                       .forEach( entry ->
                                 {
                                     URL entryUrl;
                                     try
                                     {
                                         entryUrl = new URL( "jar:" + url.toExternalForm() + "!/" + entry.getName() );
                                     }
                                     catch( MalformedURLException e )
                                     {
                                         throw new UncheckedIOException( e );
                                     }

                                     PathNode node = root.addChild( entry.getName() );
                                     node.addResource( new JarEntryResource( entry, entryUrl ) );
                                 } );
                return;
            }
        }
        throw new IllegalStateException( "unsupported URL for JAR file scanner: " + url );
    }

    static class JarEntryResource extends Resource
    {
        @NotNull
        private final URL url;

        @NotNull
        private final FileTime creationTime;

        @NotNull
        private final FileTime lastModifiedTime;

        @NotNull
        private final FileTime lastAccessTime;

        private final long size;

        JarEntryResource( @NotNull JarEntry jarEntry, @NotNull URL url )
        {
            this.url = url;
            this.creationTime = Optional.ofNullable( jarEntry.getCreationTime() ).orElseGet( () -> FileTime.from( EPOCH ) );
            this.lastModifiedTime = Optional.ofNullable( jarEntry.getLastModifiedTime() ).orElseGet( () -> FileTime.from( EPOCH ) );
            this.lastAccessTime = Optional.ofNullable( jarEntry.getLastAccessTime() ).orElseGet( () -> FileTime.from( EPOCH ) );
            this.size = jarEntry.getSize();
        }

        @Override
        public boolean equals( Object o )
        {
            if( this == o )
            {
                return true;
            }
            if( o == null || getClass() != o.getClass() )
            {
                return false;
            }
            JarEntryResource resource = ( JarEntryResource ) o;
            return Objects.equals( this.url, resource.url );
        }

        @Override
        public int hashCode()
        {
            return Objects.hash( this.url );
        }

        @Override
        public String toString()
        {
            return getClass().getSimpleName() + "[" + this.url + "]";
        }

        @NotNull
        @Override
        InputStream getInputStream() throws IOException
        {
            return this.url.openStream();
        }

        @NotNull
        @Override
        FileTime getCreationTime()
        {
            return this.creationTime;
        }

        @NotNull
        @Override
        FileTime getLastModifiedTime()
        {
            return this.lastModifiedTime;
        }

        @NotNull
        @Override
        FileTime getLastAccessTime()
        {
            return this.lastAccessTime;
        }

        @Override
        boolean isDirectory()
        {
            return this.url.toExternalForm().endsWith( "/" );
        }

        @Override
        boolean isRegularFile()
        {
            return !this.url.toExternalForm().endsWith( "/" );
        }

        @Override
        boolean isSymbolicLink()
        {
            return false;
        }

        @Override
        boolean isOther()
        {
            return false;
        }

        @Override
        long getSize()
        {
            return this.size;
        }
    }
}
