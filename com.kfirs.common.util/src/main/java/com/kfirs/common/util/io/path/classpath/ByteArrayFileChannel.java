/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import static java.lang.Integer.min;

/**
 * @author arik
 * @on 4/2/16.
 * @test create unit tests
 */
class ByteArrayFileChannel extends FileChannel
{
    @NotNull
    private final ByteBuffer buffer;

    ByteArrayFileChannel( @NotNull byte[] bytes )
    {
        this.buffer = ByteBuffer.wrap( bytes );
    }

    @Override
    public synchronized int read( ByteBuffer dst ) throws IOException
    {
        int bytesToRead = min( dst.limit(), this.buffer.remaining() );

        byte[] bytes = new byte[ bytesToRead ];
        this.buffer.get( bytes );
        dst.put( bytes );

        return bytesToRead;
    }

    @Override
    public synchronized long read( ByteBuffer[] dsts, int offset, int length ) throws IOException
    {
        long bytesRead = 0;
        for( int i = offset; i < offset + length && this.buffer.hasRemaining(); i++ )
        {
            bytesRead += read( dsts[ i ] );
        }
        return bytesRead;
    }

    @Override
    public synchronized long position() throws IOException
    {
        return this.buffer.position();
    }

    @Override
    public synchronized FileChannel position( long newPosition ) throws IOException
    {
        this.buffer.position( ( int ) newPosition );
        return this;
    }

    @Override
    public synchronized long size() throws IOException
    {
        return this.buffer.limit();
    }

    @Override
    public synchronized long transferTo( long position, long count, WritableByteChannel target ) throws IOException
    {
        if( position < 0 )
        {
            throw new IllegalArgumentException();
        }
        else if( position >= this.buffer.limit() )
        {
            return 0;
        }
        else
        {
            count = min( ( int ) count, ( int ) ( this.buffer.limit() - position ) );
        }

        ByteBuffer buffer = ByteBuffer.wrap( this.buffer.array(), ( int ) position, ( int ) count );
        target.write( buffer );
        return count;
    }

    @Override
    public synchronized int read( ByteBuffer dst, long position ) throws IOException
    {
        int count;
        if( position < 0 )
        {
            throw new IllegalArgumentException();
        }
        else if( position >= this.buffer.limit() )
        {
            return 0;
        }
        else
        {
            count = ( int ) ( this.buffer.limit() - position );
        }

        dst.put( this.buffer.array(), ( int ) position, count );
        return count;
    }

    @Override
    public MappedByteBuffer map( MapMode mode, long position, long size ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public FileLock lock( long position, long size, boolean shared ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public FileLock tryLock( long position, long size, boolean shared ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void implCloseChannel() throws IOException
    {
        // no-op
    }

    @Override
    public FileChannel truncate( long size ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public int write( ByteBuffer src ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public long write( ByteBuffer[] srcs, int offset, int length ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void force( boolean metaData ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public long transferFrom( ReadableByteChannel src, long position, long count ) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public int write( ByteBuffer src, long position ) throws IOException
    {
        throw new UnsupportedOperationException();
    }
}
