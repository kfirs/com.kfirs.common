/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.IOException;

/**
 * @author arik
 * @on 3/31/17.
 */
public class PasswordSerializer extends ToStringSerializer
{
    @Override
    public void serialize( Object value, JsonGenerator gen, SerializerProvider provider ) throws IOException
    {
        super.serialize( "********", gen, provider );
    }

    @Override
    public void serializeWithType( Object value,
                                   JsonGenerator gen,
                                   SerializerProvider provider,
                                   TypeSerializer typeSer ) throws IOException
    {
        super.serializeWithType( "********", gen, provider, typeSer );
    }
}
