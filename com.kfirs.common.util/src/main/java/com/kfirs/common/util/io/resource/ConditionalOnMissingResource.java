/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.resource;

import java.lang.annotation.*;
import org.springframework.context.annotation.Conditional;

/**
 * {@link Conditional} that only matches when ALL the specified resources are NOT on the classpath.
 *
 * @author arik
 */
@Target( { ElementType.TYPE, ElementType.METHOD } )
@Retention( RetentionPolicy.RUNTIME )
@Documented
@Conditional( OnMissingResourceCondition.class )
public @interface ConditionalOnMissingResource
{
    /**
     * The resources that must NOT be present.
     *
     * @return the resource paths that must NOT be present.
     */
    String[] resources() default { };
}
