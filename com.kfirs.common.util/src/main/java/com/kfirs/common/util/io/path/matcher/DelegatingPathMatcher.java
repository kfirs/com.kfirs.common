/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.matcher;

import com.kfirs.common.util.lang.NotNull;
import java.util.List;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import static java.util.Arrays.asList;

/**
 * @author arik
 * @on 2/1/16.
 */
public class DelegatingPathMatcher implements com.kfirs.common.util.io.path.matcher.PathMatcher
{
    @NotNull
    private final PathMatcher delegate;

    public DelegatingPathMatcher()
    {
        this( new AntPathMatcher() );
    }

    public DelegatingPathMatcher( @NotNull PathMatcher delegate )
    {
        this.delegate = delegate;
    }

    @Override
    public boolean match( @NotNull String path, @NotNull String... patterns )
    {
        return match( path, asList( patterns ) );
    }

    @Override
    public boolean match( @NotNull String path, @NotNull List<String> patterns )
    {
        boolean matching = false;
        for( String pattern : patterns )
        {
            boolean negate = false;
            while( pattern.startsWith( "!" ) )
            {
                pattern = pattern.substring( 1 );
                negate = !negate;
            }

            if( this.delegate.match( pattern, path ) )
            {
                // if we're NOT negating (NOT starting with "!") then it's a positive glob; set to TRUE
                // otherwise, if WE ARE negating (DOES start with "!") then it's a negative glob; set to FALSE
                matching = !negate;
            }
        }
        return matching;
    }
}
