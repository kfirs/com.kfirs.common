/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.messages;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;

import static org.springframework.util.ObjectUtils.isEmpty;

/**
 * @author arik
 * @on 2/13/17.
 */
public class CodeDelegatingMessageSource implements MessageSource
{
    @NotNull
    private static final Pattern RECURSIVE_CODE_PATTERN = Pattern.compile( "<([^>]+)>" );

    @NotNull
    private final MessageSource messageSource;

    public CodeDelegatingMessageSource( @NotNull MessageSource messageSource )
    {
        this.messageSource = messageSource;
    }

    @NotNull
    public MessageSource getTargetMessageSource()
    {
        return this.messageSource;
    }

    @Nullable
    @Override
    public String getMessage( @Nullable String code,
                              @Nullable Object[] args,
                              @Nullable String defaultMessage,
                              @Nullable Locale locale )
    {
        String message = this.messageSource.getMessage( code, args, defaultMessage, locale );
        if( message == null )
        {
            return null;
        }

        Matcher matcher = RECURSIVE_CODE_PATTERN.matcher( message );
        if( matcher.matches() )
        {
            String newCode = matcher.group( 1 );
            return getMessage( newCode, args, defaultMessage, locale );
        }
        else
        {
            return message;
        }
    }

    @NotNull
    @Override
    public String getMessage( @Nullable String code, @Nullable Object[] args, @Nullable Locale locale )
            throws NoSuchMessageException
    {
        String message = this.messageSource.getMessage( code, args, locale );

        Matcher matcher = RECURSIVE_CODE_PATTERN.matcher( message );
        if( matcher.matches() )
        {
            String newCode = matcher.group( 1 );
            return getMessage( newCode, args, locale );
        }
        else
        {
            return message;
        }
    }

    @NotNull
    @Override
    public String getMessage( @NotNull MessageSourceResolvable resolvable, @Nullable Locale locale )
            throws NoSuchMessageException
    {
        return Optional.ofNullable( resolvable.getCodes() )
                       .map( Stream::of )
                       .orElseGet( Stream::empty )
                       .sequential()
                       .map( code -> getMessage( code, resolvable.getArguments(), null, locale ) )
                       .filter( Objects::nonNull )
                       .findFirst()
                       .orElseGet( () -> getDefaultMessage( resolvable, locale ) );
    }

    @NotNull
    private String getDefaultMessage( @NotNull MessageSourceResolvable resolvable, @Nullable Locale locale )
    {
        String defaultMessage = resolvable.getDefaultMessage();
        if( defaultMessage == null )
        {
            String[] codes = resolvable.getCodes();
            String code = isEmpty( codes ) ? null : codes[ codes.length - 1 ];
            throw new NoSuchMessageException( code );
        }

        Matcher matcher = RECURSIVE_CODE_PATTERN.matcher( defaultMessage );
        if( matcher.matches() )
        {
            String newCode = matcher.group( 1 );
            return getMessage( newCode, resolvable.getArguments(), locale );
        }
        else
        {
            return defaultMessage;
        }
    }
}
