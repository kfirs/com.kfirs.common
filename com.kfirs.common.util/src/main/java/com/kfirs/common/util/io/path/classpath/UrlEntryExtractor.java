/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author arik
 * @on 4/22/16.
 */
final class UrlEntryExtractor
{
    private static final Logger LOG = Logger.getLogger( UrlEntryExtractor.class.getName() );

    public static final String SPRING_BOOT_NESTED_JAR_URL_SCANNER_CLASSNAME = "com.kfirs.common.util.io.path.classpath.SpringBootNestedJarUrlScanner";

    public static final String SPRING_BOOT_NESTED_BOOTINF_URL_SCANNER_CLASSNAME = "com.kfirs.common.util.io.path.classpath.SpringBootNestedBootInfUrlScanner";

    private static final Set<UrlScanner> URL_SCANNERS;

    static
    {
        Set<UrlScanner> urlScanners = new LinkedHashSet<>();
        urlScanners.add( new DirectoryUrlScanner() );
        urlScanners.add( new JarFileUrlScanner() );

        ClassLoader classLoader = UrlEntryExtractor.class.getClassLoader();
        try
        {
            classLoader.loadClass( "org.springframework.boot.loader.LaunchedURLClassLoader" );
            urlScanners.add( ( UrlScanner ) classLoader.loadClass( SPRING_BOOT_NESTED_JAR_URL_SCANNER_CLASSNAME ).newInstance() );
            urlScanners.add( ( UrlScanner ) classLoader.loadClass( SPRING_BOOT_NESTED_BOOTINF_URL_SCANNER_CLASSNAME ).newInstance() );
        }
        catch( ClassNotFoundException ignore )
        {
            LoggerFactory.getLogger( UrlEntryExtractor.class ).warn( "Spring Boot uber-JAR class-loader not found (probably running in exploded mode)" );
            // ignore if Spring Boot's URL class-loader is not found - then we just won't support it..
        }
        catch( InstantiationException | IllegalAccessException e )
        {
            LOG.log( Level.WARNING, "Could not load Spring Boot URL scanner", e );
        }

        URL_SCANNERS = Collections.unmodifiableSet( urlScanners );
    }

    @NotNull
    static PathNode extractPathNodes( @NotNull ClasspathFileSystem fileSystem ) throws IOException
    {
        // we don't use lists/sets of URL objects, because of the infamous implementation of equals/hashCode in java.net.URL
        // see http://michaelscharf.blogspot.co.il/2006/11/javaneturlequals-and-hashcode-make.html

        ClassLoader classLoader = fileSystem.getClassLoader();
        PathNode rootNode = new PathNode( fileSystem, "", null );
        for( URL url : getClassLoaderUrls( classLoader ) )
        {
            scanUrl( rootNode, classLoader, url );
        }


        return rootNode;
    }

    @NotNull
    private static URL[] getClassLoaderUrls( @NotNull ClassLoader classLoader )
    {
        URL[] urls = new URL[ 0 ];

        if( classLoader instanceof URLClassLoader )
        {
            URLClassLoader urlClassLoader = ( URLClassLoader ) classLoader;
            urls = urlClassLoader.getURLs();
        }

        ClassLoader parent = classLoader.getParent();
        if( parent != null )
        {
            URL[] parentUrls = getClassLoaderUrls( parent );
            if( parentUrls.length > 0 )
            {
                URL[] newUrls = new URL[ urls.length + parentUrls.length ];
                System.arraycopy( parentUrls, 0, newUrls, 0, parentUrls.length );
                System.arraycopy( urls, 0, newUrls, parentUrls.length, urls.length );
                urls = newUrls;
            }
        }

        return urls;
    }

    private static void scanUrl( @NotNull PathNode root, @NotNull ClassLoader classLoader, @NotNull URL url )
            throws IOException
    {
        for( UrlScanner scanner : URL_SCANNERS )
        {
            if( scanner.supports( classLoader, url ) )
            {
                scanner.scan( root, classLoader, url );
                return;
            }
        }
        LOG.log( Level.WARNING, "Unsupported URL for scanning: " + url );
    }

    private UrlEntryExtractor()
    {
    }
}
