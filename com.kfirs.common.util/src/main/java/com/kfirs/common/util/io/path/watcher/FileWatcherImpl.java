/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.watcher;

import com.kfirs.common.util.io.path.matcher.PathMatcher;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Stream;
import org.slf4j.Logger;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;
import static java.nio.file.Files.*;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 * @on 12/25/15.
 */
public class FileWatcherImpl implements FileWatcher
{
    private static final Logger LOG = getLogger( FileWatcherImpl.class );

    @NotNull
    private final ConcurrentMap<Path, ResourceChangeScanner> watchedResources = new ConcurrentHashMap<>();

    @NotNull
    private final PathMatcher pathMatcher;

    @NotNull
    private final WatchEventsConsumer watchEventsConsumer;

    private int scanInterval = 50;

    public FileWatcherImpl( @NotNull PathMatcher pathMatcher ) throws IOException
    {
        this.pathMatcher = pathMatcher;
        this.watchEventsConsumer = new WatchEventsConsumer();

        Thread thread = new Thread( this.watchEventsConsumer, "file-watcher" );
        thread.setDaemon( true );
        thread.setUncaughtExceptionHandler(
                ( t, e ) -> LOG.error( "Unexpected exception thrown from file watcher: {}", e.getMessage(), e ) );
        thread.start();
    }

    @SuppressWarnings( "unused" )
    public int getScanInterval()
    {
        return this.scanInterval;
    }

    public void setScanInterval( int scanInterval )
    {
        this.scanInterval = scanInterval;
    }

    @Override
    public void watch( @NotNull Path resource, @NotNull FileChangeListener listener, @NotNull String... globs )
    {
        addListener( resource, listener, globs.length == 0 ? null : asList( globs ) );
    }

    @Override
    public void watch( @NotNull Path resource, @NotNull FileChangeListener listener, @NotNull List<String> globs )
    {
        addListener( resource, listener, globs );
    }

    private void addListener( @NotNull Path resource,
                              @NotNull FileChangeListener listener,
                              @Nullable List<String> globs )
    {
        if( !this.watchEventsConsumer.running )
        {
            throw new IllegalStateException( "file watcher not active" );
        }

        this.watchedResources.compute( resource, ( path, resourceChangeScanner ) ->
        {
            if( resourceChangeScanner == null )
            {
                resourceChangeScanner = new ResourceChangeScanner( resource );
            }

            FileChangeListener l = globs == null || globs.isEmpty()
                                   ? listener
                                   : new GlobbingFileChangeListener( this.pathMatcher, listener, globs );

            resourceChangeScanner.listeners.add( l );
            return resourceChangeScanner;
        } );
    }

    @Override
    public void stopWatching( @NotNull Path resource ) throws IOException
    {
        this.watchedResources.remove( resource );
    }

    public void close()
    {
        this.watchEventsConsumer.running = false;
    }

    private class ResourceChangeScanner
    {
        @NotNull
        private final Path root;

        @NotNull
        private final List<FileChangeListener> listeners = new CopyOnWriteArrayList<>();

        @NotNull
        private final Map<Path, Instant> knownFileModTimes = new ConcurrentHashMap<>();

        private ResourceChangeScanner( @NotNull Path root )
        {
            this.root = root;

            if( Files.exists( this.root ) )
            {
                try( Stream<Path> stream = walk( this.root, FOLLOW_LINKS ) )
                {
                    stream.forEach( file ->
                                    {
                                        try
                                        {
                                            if( isRegularFile( file ) )
                                            {
                                                this.knownFileModTimes.put( file, getLastModifiedTime( file ).toInstant() );
                                            }
                                        }
                                        catch( IOException e )
                                        {
                                            LOG.warn( "Error inspecting '{}': {}", file, e.getMessage(), e );
                                        }
                                    } );
                }
                catch( IOException e )
                {
                    LOG.warn( "Error inspecting '{}': {}", this.root, e.getMessage(), e );
                }
            }
        }

        private void scan()
        {
            try
            {
                if( Files.exists( this.root ) )
                {
                    // publish CREATED/MODIFIED events for files that have been updated, and update their recorded timestamp
                    try( Stream<Path> stream = walk( this.root, FOLLOW_LINKS ) )
                    {
                        stream.filter( Files::isRegularFile ).forEach( this::handleResource );
                    }
                }

                // publish DELETED event for files that no longer exist, and remove their recorded timestamp
                this.knownFileModTimes.keySet().removeAll( findAndNotifyNonExistingFiles() );
            }
            catch( NoSuchFileException ignore )
            {
            }
            catch( Exception e )
            {
                LOG.warn( "Error scanning '{}': {}", this.root, e.getMessage(), e );
            }
        }

        private void handleResource( @NotNull Path resource )
        {
            try
            {
                Instant currentModTime = getLastModifiedTime( resource ).toInstant();
                Instant knownModTime = this.knownFileModTimes.get( resource );
                if( knownModTime == null )
                {
                    this.knownFileModTimes.put( resource, currentModTime );
                    notifyListeners( resource, FileChangedEvent.ChangeType.CREATED );
                }
                else
                {
                    long diff = currentModTime.toEpochMilli() - knownModTime.toEpochMilli();
                    if( diff > 200 )
                    {
                        this.knownFileModTimes.put( resource, currentModTime );
                        notifyListeners( resource, FileChangedEvent.ChangeType.MODIFIED );
                    }
                }
            }
            catch( IOException e )
            {
                LOG.warn( "Error inspecting '{}': {}", resource, e.getMessage(), e );
            }
        }

        @NotNull
        private List<Path> findAndNotifyNonExistingFiles()
        {
            return this.knownFileModTimes
                    .keySet()
                    .stream()
                    .filter( Files::notExists )
                    .peek( resource -> notifyListeners( resource, FileChangedEvent.ChangeType.DELETED ) )
                    .collect( toList() );
        }

        private void notifyListeners( @NotNull Path resource, @NotNull FileChangedEvent.ChangeType changeType )
        {
            Path relativePath = this.root.equals( resource ) ? this.root : this.root.relativize( resource );
            FileChangedEvent event = new FileChangedEvent( this.root, relativePath, changeType );

            for( FileChangeListener listener : this.listeners )
            {
                try
                {
                    listener.fileChanged( event );
                }
                catch( Exception e )
                {
                    String msg = "Listener '{}' threw exception while being notified about {} event for '{}'";
                    LOG.warn( msg, listener, changeType, resource, e );
                }
            }
        }
    }

    private class WatchEventsConsumer implements Runnable
    {
        private boolean running = true;

        @Override
        public void run()
        {
            while( this.running )
            {
                watchedResources.values().stream().forEach( ResourceChangeScanner::scan );

                // sleep for a while...
                try
                {
                    Thread.sleep( FileWatcherImpl.this.scanInterval );
                }
                catch( InterruptedException e )
                {
                    LOG.warn( "File watcher interrupted" );
                    break;
                }
            }
        }
    }
}
