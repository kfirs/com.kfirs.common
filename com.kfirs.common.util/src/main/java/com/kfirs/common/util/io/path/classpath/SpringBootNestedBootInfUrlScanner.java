/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.boot.loader.LaunchedURLClassLoader;

/**
 * @author arik
 * @on 6/30/17.
 */
@SuppressWarnings( "unused" )
class SpringBootNestedBootInfUrlScanner extends UrlScanner
{
    private static final Logger LOG = Logger.getLogger( SpringBootNestedBootInfUrlScanner.class.getName() );

    @NotNull
    private static final Pattern PATTERN = Pattern.compile( "jar:file:([^!]+\\.jar)!/BOOT-INF/classes!/" );

    @Override
    boolean supports( @NotNull ClassLoader classLoader, @NotNull URL url )
    {
        return classLoader instanceof LaunchedURLClassLoader && PATTERN.matcher( url.toExternalForm() ).matches();
    }

    @Override
    void scan( @NotNull PathNode root, @NotNull ClassLoader classLoader, @NotNull URL url ) throws IOException
    {
        Matcher matcher = PATTERN.matcher( url.toExternalForm() );
        if( !matcher.matches() )
        {
            throw new IllegalStateException( "unsupported URL for Spring Boot JAR file scanner: " + url );
        }

        String outerJarFilePathname = matcher.group( 1 );
        try( JarFile outerJarFile = new JarFile( Paths.get( outerJarFilePathname ).toFile() ) )
        {
            outerJarFile.stream()
                        .filter( entry -> entry.getName().startsWith( "BOOT-INF/classes/" ) )
                        .filter( entry -> !entry.getName().equalsIgnoreCase( "BOOT-INF/classes/" ) )
                        .filter( entry -> !entry.getName().equalsIgnoreCase( "BOOT-INF/classes" ) )
                        .sorted( Comparator.comparing( JarEntry::getName ) )
                        .forEach( entry -> {
                            try
                            {
                                String entryName = entry.getName().substring( "BOOT-INF/classes/".length() );
                                URL entryUrl = new URL( url, entryName );
                                SpringBootResource resource = new SpringBootResource( entry, entryUrl );
                                root.addChild( entryName ).addResource( resource );
                            }
                            catch( MalformedURLException e )
                            {
                                throw new UncheckedIOException( e );
                            }
                        } );
        }
    }
}
