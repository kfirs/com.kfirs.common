/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.jdbc;

import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.math.BigDecimal;
import java.sql.JDBCType;

import static java.util.Objects.requireNonNull;

/**
 * @author arik
 * @on 4/17/17.
 */
public final class JdbcTypeUtils
{
    @Nullable
    public static Object convertToJdbcType( @Nullable String stringValue, int jdbcType )
    {
        return convertToJdbcType( stringValue, JDBCType.valueOf( jdbcType ) );
    }

    @Nullable
    public static Object convertToJdbcType( @Nullable String stringValue, @NotNull JDBCType jdbcType )
    {
        switch( jdbcType )
        {
            case NULL:
                return null;
            case BIT:
            case BOOLEAN:
                return "1".equals( stringValue ) || Boolean.valueOf( stringValue );
            case TINYINT:
            case SMALLINT:
                return Short.valueOf( requireNonNull( stringValue ) );
            case INTEGER:
                return Integer.valueOf( requireNonNull( stringValue ) );
            case BIGINT:
                return Long.valueOf( requireNonNull( stringValue ) );
            case REAL:
                return Float.valueOf( requireNonNull( stringValue ) );
            case DOUBLE:
            case FLOAT:
                return Double.valueOf( requireNonNull( stringValue ) );
            case NUMERIC:
            case DECIMAL:
                return new BigDecimal( requireNonNull( stringValue ) );
            case DATE:
                return java.sql.Date.valueOf( requireNonNull( stringValue ) );
            case TIME:
                return java.sql.Time.valueOf( requireNonNull( stringValue ) );
            case TIMESTAMP:
                return java.sql.Timestamp.valueOf( requireNonNull( stringValue ) );
            case CHAR:
            case VARCHAR:
            case LONGVARCHAR:
                return stringValue;
            default:
                throw new IllegalStateException( "unsupported JDBC type for '" + stringValue + "' in table '" );
        }
    }

    @NotNull
    public static String convertToSqlString( @Nullable String stringValue, int jdbcType )
    {
        return convertToSqlString( stringValue, JDBCType.valueOf( jdbcType ) );
    }

    @NotNull
    public static String convertToSqlString( @Nullable String stringValue, @NotNull JDBCType jdbcType )
    {
        switch( jdbcType )
        {
            case NULL:
                return "NULL";
            case BIT:
            case BOOLEAN:
                return ( Boolean.valueOf( requireNonNull( stringValue ) ) + "" ).toUpperCase();
            case TINYINT:
            case SMALLINT:
                return Short.valueOf( requireNonNull( stringValue ) ) + "";
            case INTEGER:
                return Integer.valueOf( requireNonNull( stringValue ) ) + "";
            case BIGINT:
                return Long.valueOf( requireNonNull( stringValue ) ) + "";
            case REAL:
                return Float.valueOf( requireNonNull( stringValue ) ) + "";
            case DOUBLE:
            case FLOAT:
                return Double.valueOf( requireNonNull( stringValue ) ) + "";
            case NUMERIC:
            case DECIMAL:
                return new BigDecimal( requireNonNull( stringValue ) ) + "";
            case DATE:
                return "'" + requireNonNull( stringValue ) + "'";
            case TIME:
                return "'" + requireNonNull( stringValue ) + "'";
            case TIMESTAMP:
                return "'" + requireNonNull( stringValue ) + "'";
            case CHAR:
            case VARCHAR:
            case LONGVARCHAR:
                return "'" + requireNonNull( stringValue ) + "'";
            default:
                throw new IllegalStateException( "unsupported JDBC type for '" + stringValue + "' in table '" );
        }
    }

    private JdbcTypeUtils()
    {
    }
}
