/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.classpath;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.nio.file.*;
import sun.reflect.Reflection;

import static java.lang.System.identityHashCode;
import static java.util.Collections.singletonMap;

/**
 * @author arik
 * @on 4/6/16.
 */
public final class ClasspathUtils
{
    public static final String CLASS_LOADER_KEY = "classLoader";

    @NotNull
    public static FileSystem getFileSystem() throws IOException
    {
        Class<?> callerClass = Reflection.getCallerClass();
        return getFileSystem( callerClass == null ? ClassLoader.getSystemClassLoader() : callerClass.getClassLoader() );
    }

    @NotNull
    public static FileSystem getFileSystem( @NotNull Class<?> cls )
    {
        return getFileSystem( cls.getClassLoader() );
    }

    @NotNull
    public static FileSystem getFileSystem( @NotNull ClassLoader classLoader )
    {
        URI uri = URI.create( String.format( "classpath://%s/", identityHashCode( classLoader ) ) );
        try
        {
            return FileSystems.getFileSystem( uri );
        }
        catch( FileSystemNotFoundException | ProviderNotFoundException e )
        {
            try
            {
                return FileSystems.newFileSystem( uri,
                                                  singletonMap( CLASS_LOADER_KEY, classLoader ),
                                                  ClasspathUtils.class.getClassLoader() );
            }
            catch( IOException e1 )
            {
                throw new UncheckedIOException( "could not create classpath file-system", e1 );
            }
        }
    }

    @NotNull
    public static Path getPath( @NotNull ClassLoader classLoader, @NotNull String first, @NotNull String... more )
    {
        return getFileSystem( classLoader ).getPath( first, more );
    }

    private ClasspathUtils()
    {
    }
}
