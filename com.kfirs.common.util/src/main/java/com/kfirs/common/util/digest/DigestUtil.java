/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.digest;

import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author arik
 * @on 2/26/16.
 */
public final class DigestUtil
{
    private static final ThreadLocal<AlgorithmSupplier> algorithms = new ThreadLocal<AlgorithmSupplier>()
    {
        @Override
        protected AlgorithmSupplier initialValue()
        {
            return new AlgorithmSupplier();
        }
    };

    @NotNull
    public static byte[] digest( @NotNull String algorithm, @NotNull byte[] bytes )
    {
        MessageDigest digest = algorithms.get().getMessageDigest( algorithm );
        digest.reset();
        digest.update( bytes );
        return digest.digest();
    }

    @NotNull
    public static byte[] digest( @NotNull String algorithm, @NotNull Path file )
    {
        try
        {
            return digest( algorithm, Files.readAllBytes( file ) );
        }
        catch( IOException e )
        {
            throw new UncheckedIOException( e.getMessage(), e );
        }
    }

    private static class AlgorithmSupplier
    {
        @NotNull
        private final Map<String, MessageDigest> algorithms = new HashMap<>( 10 );

        @NotNull
        private MessageDigest getMessageDigest( @NotNull String algorithm )
        {
            return this.algorithms.computeIfAbsent( algorithm, key -> {
                try
                {
                    return MessageDigest.getInstance( key );
                }
                catch( NoSuchAlgorithmException e )
                {
                    throw new IllegalStateException( "'" + key + "' algorithm not found!", e );
                }
            } );
        }
    }

    private DigestUtil()
    {
    }
}
