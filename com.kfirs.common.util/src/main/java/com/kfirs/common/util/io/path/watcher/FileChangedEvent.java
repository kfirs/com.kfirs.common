/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.util.io.path.watcher;

import com.kfirs.common.util.lang.NotNull;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Objects;

/**
 * @author arik
 * @on 12/25/15.
 */
public class FileChangedEvent
{
    @NotNull
    private final Instant timestamp = Instant.now();

    @NotNull
    private final Path watchedPath;

    @NotNull
    private final Path relativePath;

    @NotNull
    private final ChangeType changeType;

    public FileChangedEvent( @NotNull Path watchedPath, @NotNull Path relativePath, @NotNull ChangeType changeType )
    {
        this.watchedPath = watchedPath;
        this.relativePath = relativePath;
        this.changeType = changeType;
    }

    @NotNull
    public Instant getTimestamp()
    {
        return timestamp;
    }

    @NotNull
    public Path getWatchedPath()
    {
        return watchedPath;
    }

    @NotNull
    public Path getRelativeChangedPath()
    {
        return relativePath;
    }

    @NotNull
    public Path getAbsoluteChangedPath()
    {
        return this.watchedPath.resolve( this.relativePath );
    }

    @NotNull
    public ChangeType getChangeType()
    {
        return changeType;
    }

    @Override
    public boolean equals( Object o )
    {
        if( this == o )
        {
            return true;
        }
        if( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        FileChangedEvent that = ( FileChangedEvent ) o;
        return Objects.equals( this.watchedPath, that.watchedPath ) && Objects.equals( this.relativePath,
                                                                                       that.relativePath ) && changeType == that.changeType;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( this.watchedPath, this.relativePath, changeType );
    }

    @Override
    public String toString()
    {
        return "FileChangedEvent[ changeType=" + changeType + ", watchedPath=" + this.watchedPath + ", relativePath=" + this.relativePath + "]";
    }

    public enum ChangeType
    {
        CREATED,
        MODIFIED,
        DELETED
    }
}
