## Using the nullability annotations

This module provides two annotations - `@NotNull` and `@Nullable` that
can annotate method/constructor parameters, methods and fields. They can
be used in two manners:

* Development time, by IDEs: some IDEs can inspect and infer nullability
  problems when you annotate certain symbols using these annotations.
  For example, if you annotate a method parameter with `@NotNull`, and
  then call that method with a `null` symbol, IntelliJ IDEA can be 
  configured to highlight a warning to indicate that you might have a
  bug.
  
* Runtime, through a Java Agent: while not currently implemented, I do
  intend to provide a JVM agent which will enforce `@NotNull` checks in
  runtime.

### Configuring IntelliJ IDEA

To configure IntelliJ IDEA to use detect these annotations:

1. Open your IntelliJ IDE Settings
2. In the settings tree, go to `Editor` > `Inspections`
3. Find the `@NotNull/@Nullable problems` inspection.
   <img src="intellij-null-inspection.png" width="400"/>
   
4. Click the `Configure annotations` button
5. Add the `@Nullable` annotation to the `@Nullable annotations` list
6. Add the `@NotNull` annotation to the `@NotNull annotations` list
   <img src="intellij-null-annotations.png" width="400"/>

That's it!
