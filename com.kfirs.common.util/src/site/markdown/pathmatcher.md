## Welcome to the KFIRS Common Path Matcher project

This module provides an enhanced path matcher component, building on top
of Spring's PathMatcher, but the API follows Gulp's way of providing
patterns.

### Usage ###

### PathMatcher API ###

The `PathMatcher` interface provides two methods that accept a path, and
a list of patterns. Both methods provide the same functionality, but one
accepts a `String` varargs parameter, and the other a `List`.

Both methods will return `true` if:

- at least one of the patterns matches the given path, **and**
- no other pattern _that appears after_ that matching pattern excludes the path

Examples:

    PathMatcher pathMatcher = ... // see later on below about obtaining instances of PathMatcher
    assert true == pathMatcher.matches( "/foo/bar/file.txt", "/foo/**" );
    assert true == pathMatcher.matches( "/foo/bar/file.txt", "/foo/bar/**" );
    assert true == pathMatcher.matches( "/foo/bar/file.txt", 
                                        "/home/user",       // not a matching pattern 
                                        "/foo/bar/**"       // this one matches!
                                        );
    assert false == pathMatcher.matches( "/foo/bar/file.txt", 
                                         "/home/user",           // not a matching pattern 
                                         "/foo/bar/**"           // this one matches!
                                         "!/foo/bar/file.txt"    // this one negates the previous one!
                                         );

### Obtaining a PathMatcher ###

Instances of `PathMatcher` can be created by simply creating a new instance
of `DelegatingPathMatcher`. If, however, you're using `Spring Boot`, you
can simply have it injected (through `@Autowired` or XML configuration)
into your `Spring` components, if you also add the `commons-autoconfigure` module.
