## Java 7 Classpath file-system

Inspecting and traversing the classpath of the JVM is a difficult thing,
but can be useful for several use-cases such as embdedded resources
lookup, extraction of such resources (without knowing in advance their
name), auto registration of resources or classes based on their presence
in the classpath, etc.

The problem, however, is that although quite a few libraries attempt to
provide a solution to this need, they are all (in my honest oppinion)
either partial (no traveral, only direct lookup by knowing the names in
advance) or incur costs such as imposing too many classpath dependencies.

This (small) library attempts to solve this by:

* Providing a standard API - the classpath is simply exposed as another
  file-system through the standard Java 7 Path API. So you would simply
  use the standard `java.nio.file.Path` class, and all its utilities
  such as the `Files.walk()`, `Files.isDirectory`, `Files.newInputStream`, etc.
  
* Minimal required dependencies - just put this dependency in the
  classpath!
  
* Support for inspecting the classpath of specific class-loaders! Standard
  class-loaders such as `URLClassLoader` are supported.
  
* Support for traversal is done through the standard `Files` class of
  the JVM (and other standard JVM APIs). No need to learn a new API.

### Usage ###

To use, first add `com.kfirs.common.classpath` dependency to your POM, like
this:

    <dependency>
      <groupId>com.kfirs.common</groupId>
      <artifactId>com.kfirs.common.classpath</artifactId>
      <version>1.0.3</version> <!-- use latest version if possible -->
    </dependency>
    
Then, use like this:

    public class MyClass
    {
        public void doSomething() throws IOException
        {
            FileSystem fs = ClasspathUtils.getFileSystem();
            
            Path classpathPropsFile = fs.getPath("/my/package/my.properties");
            assert Files.isRegularFile(classpathPropsFile);
            
            Path classpath2ndPropsFile = classpathPropsFile.resolveSibling("my2.properties");
            assert Files.exists(classpath2ndPropsFile);
            
            Path classpathPackage = fs.getPath("/my/package");
            assert Files.isDirectory(classpathPackage);
            
            Files.walk(classpathPackage)
                 .filter( path-> path.getFileName().toString().startsWith("my") )
                 .forEach( path -> System.out.println(path) );
        }
    }
