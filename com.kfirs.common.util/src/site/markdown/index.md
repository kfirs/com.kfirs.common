## Welcome to the KFIRS Common Utilities project

This module provides low-level utilities that are useful to any project
of any size, and are too small to be broken into their own module.
