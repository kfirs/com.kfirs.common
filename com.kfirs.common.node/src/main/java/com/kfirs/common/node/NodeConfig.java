/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node;

import com.kfirs.common.node.impl.NodeImpl;
import com.kfirs.common.node.impl.NodeInstallerImpl;
import com.kfirs.common.node.impl.NodeProcessExecutor;
import com.kfirs.common.node.impl.NodeProperties;
import com.kfirs.common.util.lang.NotNull;
import java.nio.file.Paths;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author arik
 * @on 3/28/17.
 */
@Configuration
public class NodeConfig
{
    @Bean
    @ConfigurationProperties( prefix = "kfirs.node" )
    public NodeProperties nodeProperties()
    {
        return new NodeProperties();
    }

    @Bean
    public NodeInstaller nodeInstaller( @NotNull NodeProperties nodeProperties )
    {
        return new NodeInstallerImpl( Paths.get( nodeProperties.getUnpackPath() ) );
    }

    @Bean
    public NodeProcessExecutor nodeProcessExecutor()
    {
        return new NodeProcessExecutor();
    }

    @Bean
    public NodeImpl node( @NotNull NodeProperties nodeProperties,
                          @NotNull NodeInstaller installer,
                          @NotNull NodeProcessExecutor nodeProcessExecutor )
    {
        return new NodeImpl( installer.getNodeInstallation( nodeProperties.getVersion() ), nodeProcessExecutor );
    }
}
