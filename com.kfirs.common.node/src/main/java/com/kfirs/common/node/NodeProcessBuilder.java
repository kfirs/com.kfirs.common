/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node;

import com.kfirs.common.util.lang.NotNull;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Consumer;
import org.slf4j.Logger;

/**
 * @author arik
 * @on 5/20/16.
 */
public interface NodeProcessBuilder
{
    @NotNull
    NodeProcessBuilder withWorkDir( @NotNull Path path );

    @NotNull
    NodeProcessBuilder addCommand( @NotNull String... command );

    @NotNull
    NodeProcessBuilder withCommand( @NotNull String... command );

    @NotNull
    NodeProcessBuilder withCommand( @NotNull List<String> command );

    @NotNull
    NodeProcessBuilder withStdoutConsumer( @NotNull Consumer<String> consumer );

    @NotNull
    NodeProcessBuilder withStderrConsumer( @NotNull Consumer<String> consumer );

    @NotNull
    NodeProcessBuilder withStdoutLogging( @NotNull Logger logger );

    @NotNull
    NodeProcessBuilder withStdoutLogging( @NotNull String loggerName );

    @NotNull
    NodeProcessBuilder withStderrLogging( @NotNull Logger logger );

    @NotNull
    NodeProcessBuilder withStderrLogging( @NotNull String loggerName );

    @NotNull
    NodeProcessBuilder withResultLogger();

    @NotNull
    NodeProcessBuilder withResultLogger( @NotNull String logger );

    @NotNull
    NodeProcessBuilder withResultLogger( @NotNull Logger logger );

    @NotNull
    NodeProcessBuilder withResultConsumer( @NotNull Consumer<NodeProcessResult> consumer );

    @NotNull
    NodeProcess execute();
}
