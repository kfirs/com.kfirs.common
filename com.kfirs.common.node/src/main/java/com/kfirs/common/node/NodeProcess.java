/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node;

import com.kfirs.common.util.lang.NotNull;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author arik
 * @on 12/22/15.
 */
public interface NodeProcess extends CompletionStage<NodeProcessResult>
{
    @NotNull
    Path getWorkDirectory();

    @NotNull
    List<String> getCommand();

    boolean isAlive();

    void destroy( long timeout, @NotNull TimeUnit timeUnit ) throws InterruptedException, TimeoutException;

    void destroyForcibly( long timeout, @NotNull TimeUnit timeUnit ) throws InterruptedException, TimeoutException;

    @NotNull
    NodeProcessResult waitFor() throws InterruptedException;

    @NotNull
    NodeProcessResult waitFor( long duration, @NotNull TimeUnit timeUnit ) throws InterruptedException,
                                                                                  TimeoutException;
}
