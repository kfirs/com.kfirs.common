/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node.impl;

import com.kfirs.common.util.lang.NotNull;
import java.nio.file.Paths;

/**
 * @author arik
 * @on 12/22/15.
 */
public class NodeProperties
{
    private String version = "7.7.4";

    private String arch = "i386".equalsIgnoreCase( System.getProperty( "os.arch", "x64" ) ) ? "x86" : "x64";

    private String unpackPath;

    public NodeProperties()
    {
        this.unpackPath = Paths.get( System.getProperty( "java.io.tmpdir" ), "kfirs/node" ).toString();
    }

    @NotNull
    public String getVersion()
    {
        return version;
    }

    public void setVersion( @NotNull String version )
    {
        this.version = version;
    }

    public String getArch()
    {
        return arch;
    }

    public void setArch( String arch )
    {
        this.arch = arch;
    }

    public String getUnpackPath()
    {
        return unpackPath;
    }

    public void setUnpackPath( String unpackPath )
    {
        this.unpackPath = unpackPath;
    }
}
