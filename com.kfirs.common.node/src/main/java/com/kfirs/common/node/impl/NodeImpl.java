/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node.impl;

import com.kfirs.common.node.Node;
import com.kfirs.common.node.NodeProcess;
import com.kfirs.common.node.NodeProcessBuilder;
import com.kfirs.common.node.NodeProcessResult;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author arik
 * @on 12/23/15.
 */
public class NodeImpl implements Node
{
    @NotNull
    private final Path installation;

    @NotNull
    private final Path nodeExecutable;

    @NotNull
    private final Path npmExecutable;

    @NotNull
    private final NodeProcessExecutor executionQueue;

    public NodeImpl( @NotNull Path path, @NotNull NodeProcessExecutor executionQueue )
    {
        this.installation = path;
        this.nodeExecutable = this.installation.resolve( "bin/node" );
        this.npmExecutable = installation.resolve( "bin/npm" );
        this.executionQueue = executionQueue;
    }

    @NotNull
    @Override
    public Path getPath()
    {
        return this.installation;
    }

    @NotNull
    @Override
    public NodeProcessBuilder nodeBuilder()
    {
        return new NodeProcessBuilderImpl(
                ( workDir,
                  stdoutConsumer,
                  stderrConsumer,
                  resultConsumer,
                  command ) -> executeNode( workDir,
                                            this.nodeExecutable,
                                            stdoutConsumer,
                                            stderrConsumer,
                                            resultConsumer,
                                            command ) );
    }

    @NotNull
    @Override
    public NodeProcessBuilder npmBuilder()
    {
        return new NodeProcessBuilderImpl(
                ( workDir,
                  stdoutConsumer,
                  stderrConsumer,
                  successExitCode,
                  command ) -> executeNode( workDir,
                                            this.npmExecutable,
                                            stdoutConsumer,
                                            stderrConsumer,
                                            successExitCode,
                                            command ) );
    }

    @NotNull
    private NodeProcess executeNode( @NotNull Path workDir,
                                     @NotNull Path executable,
                                     @NotNull Consumer<String> stdoutConsumer,
                                     @NotNull Consumer<String> stderrConsumer,
                                     @Nullable Consumer<NodeProcessResult> resultConsumer,
                                     @NotNull List<String> command )
    {
        List<String> commandList = new LinkedList<>( command );
        commandList.add( 0, executable.toString() );
        return this.executionQueue.scheduleNodeProcess(
                commandList.toString(),
                this.installation,
                workDir,
                commandList,
                stdoutConsumer,
                stderrConsumer,
                resultConsumer );
    }
}
