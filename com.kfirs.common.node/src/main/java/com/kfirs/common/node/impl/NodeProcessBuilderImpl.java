/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node.impl;

import com.kfirs.common.node.Node;
import com.kfirs.common.node.NodeProcess;
import com.kfirs.common.node.NodeProcessBuilder;
import com.kfirs.common.node.NodeProcessResult;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Arrays.asList;

/**
 * @author arik
 * @on 5/20/16.
 */
class NodeProcessBuilderImpl implements NodeProcessBuilder
{
    public static final Logger NODE_LOGGER = LoggerFactory.getLogger( Node.class.getPackage().getName() );

    interface NodeExecutor
    {
        @NotNull
        NodeProcess executeNode( @NotNull Path workDir,
                                 @NotNull Consumer<String> stdoutConsumer,
                                 @NotNull Consumer<String> stderrConsumer,
                                 @Nullable Consumer<NodeProcessResult> resultConsumer,
                                 @NotNull List<String> command );
    }

    @NotNull
    private final NodeExecutor executor;

    @Nullable
    private Path workDir;

    @Nullable
    private List<String> command;

    @Nullable
    private Consumer<String> stdoutConsumer;

    @Nullable
    private Consumer<String> stderrConsumer;

    @Nullable
    private Consumer<NodeProcessResult> resultConsumer;

    NodeProcessBuilderImpl( @NotNull NodeExecutor executor )
    {
        this.executor = executor;
    }

    @NotNull
    @Override
    public NodeProcess execute()
    {
        Path workDir = this.workDir == null ? Paths.get( System.getProperty( "user.dir" ) ) : this.workDir;

        if( this.command == null )
        {
            throw new IllegalStateException( "command has not been set on this builder" );
        }

        Consumer<String> stdoutConsumer = this.stdoutConsumer;
        if( stdoutConsumer == null )
        {
            stdoutConsumer = new LoggingOutputConsumer( NODE_LOGGER, false );
        }

        Consumer<String> stderrConsumer = this.stderrConsumer;
        if( stderrConsumer == null )
        {
            stderrConsumer = new LoggingOutputConsumer( NODE_LOGGER, true );
        }

        return this.executor.executeNode( workDir, stdoutConsumer, stderrConsumer, this.resultConsumer, this.command );
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withWorkDir( @NotNull Path path )
    {
        this.workDir = path;
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl addCommand( @NotNull String... command )
    {
        if( this.command == null )
        {
            this.command = new LinkedList<>();
        }
        this.command.addAll( asList( command ) );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withCommand( @NotNull String... command )
    {
        if( this.command == null )
        {
            this.command = new LinkedList<>();
        }
        else
        {
            this.command.clear();
        }
        this.command.addAll( asList( command ) );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withCommand( @NotNull List<String> command )
    {
        this.command = new LinkedList<>( command );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withStdoutConsumer( @NotNull Consumer<String> consumer )
    {
        this.stdoutConsumer = consumer;
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withStderrConsumer( @NotNull Consumer<String> consumer )
    {
        this.stderrConsumer = consumer;
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withStdoutLogging( @NotNull Logger logger )
    {
        this.stdoutConsumer = new LoggingOutputConsumer( logger, false );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withStdoutLogging( @NotNull String loggerName )
    {
        this.stdoutConsumer = new LoggingOutputConsumer( LoggerFactory.getLogger( loggerName ), false );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withStderrLogging( @NotNull Logger logger )
    {
        this.stderrConsumer = new LoggingOutputConsumer( logger, true );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withStderrLogging( @NotNull String loggerName )
    {
        this.stderrConsumer = new LoggingOutputConsumer( LoggerFactory.getLogger( loggerName ), true );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withResultLogger()
    {
        return withResultLogger( NODE_LOGGER );
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withResultLogger( @NotNull String loggerName )
    {
        return withResultLogger( LoggerFactory.getLogger( loggerName ) );
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withResultLogger( @NotNull Logger logger )
    {
        this.resultConsumer = result -> logger.info( "Node process " + this.command + " terminated with exit code " + result.getExitCode() );
        return this;
    }

    @NotNull
    @Override
    public NodeProcessBuilderImpl withResultConsumer( @NotNull Consumer<NodeProcessResult> consumer )
    {
        this.resultConsumer = consumer;
        return this;
    }

    private class LoggingOutputConsumer implements Consumer<String>
    {
        @NotNull
        private final Logger logger;

        private final boolean error;

        private LoggingOutputConsumer( @NotNull Logger logger, boolean error )
        {
            this.logger = logger;
            this.error = error;
        }

        @Override
        public void accept( String line )
        {
            if( line.matches( ".*\\bWARN\\b.*" ) )
            {
                this.logger.warn( line );
            }
            else if( this.error )
            {
                this.logger.error( line );
            }
            else
            {
                this.logger.info( line );
            }
        }
    }
}
