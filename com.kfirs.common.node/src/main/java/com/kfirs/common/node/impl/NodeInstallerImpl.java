/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node.impl;

import com.kfirs.common.node.NodeInstaller;
import com.kfirs.common.util.lang.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.StandardOpenOption.*;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 * @on 12/23/15.
 */
public class NodeInstallerImpl implements NodeInstaller
{
    private static final Logger LOG = getLogger( NodeInstallerImpl.class );

    private static final String NODE_DOWNLOAD_URL_PATTERN = "https://nodejs.org/dist/v%s/node-v%s-linux-%s.tar.gz";

    private static final String NODE_DIR = "node-v%s-linux-%s";

    @NotNull
    private final String arch = "i386".equalsIgnoreCase( System.getProperty( "os.arch", "x64" ) ) ? "x86" : "x64";

    @NotNull
    private final String os = System.getProperty( "os.name" );

    @NotNull
    private final Path unpackPath;

    public NodeInstallerImpl( @NotNull Path unpackPath )
    {
        this.unpackPath = unpackPath;
    }

    @NotNull
    public Path getNodeInstallation( @NotNull String version )
    {
        if( !this.os.startsWith( "Linux" ) && !this.os.startsWith( "LINUX" ) )
        {
            throw new IllegalStateException( "only Linux is currently supported by Node installer" );
        }

        Path installDir = this.unpackPath.resolve( String.format( NODE_DIR, version, this.arch ) );
        if( Files.exists( installDir ) )
        {
            LOG.info( "Found Node v{}-{} at: {}", version, arch, installDir );
            return installDir;
        }

        // we expect the tar to create the 'installDir' for us
        extractTar( extractTarGz( downloadFile( version ) ) );

        if( Files.exists( installDir ) )
        {
            return installDir;
        }

        throw new IllegalStateException( "error obtaining Node" );
    }

    @NotNull
    private Path downloadFile( @NotNull String version )
    {
        URL url;
        try
        {
            url = new URL( String.format( NODE_DOWNLOAD_URL_PATTERN, version, version, this.arch ) );
        }
        catch( MalformedURLException e )
        {
            throw new UncheckedIOException( e );
        }

        String fileName = url.getPath();
        if( fileName == null || fileName.isEmpty() )
        {
            throw new IllegalArgumentException( "URL does not denote a file name: " + url );
        }

        int lastSlashIndex = fileName.lastIndexOf( '/' );
        if( lastSlashIndex >= 0 )
        {
            fileName = fileName.substring( lastSlashIndex + 1 );
        }

        LOG.info( "Downloading Node v{}-{} from: {}", version, this.arch, url );
        Path file = Paths.get( System.getProperty( "java.io.tmpdir" ) ).resolve( fileName );
        if( Files.notExists( file ) )
        {
            try( InputStream inputStream = url.openStream();
                 OutputStream outputStream = newOutputStream( file, CREATE, TRUNCATE_EXISTING, WRITE ) )
            {
                IOUtils.copy( inputStream, outputStream );
            }
            catch( IOException e )
            {
                String message = "could not download file from '" + url + "': " + e.getMessage();
                throw new UncheckedIOException( message, e );
            }
        }
        else
        {
            LOG.info( "Node v{}-{} already downloded to '{}' - using that", version, this.arch, file );
        }

        return file;
    }

    @NotNull
    private Path extractTarGz( @NotNull Path tarGzFile )
    {
        String tarFileName = tarGzFile.getFileName().toString().toLowerCase();
        int dotGzIndex = tarFileName.lastIndexOf( ".gz" );
        if( dotGzIndex < 0 )
        {
            throw new IllegalArgumentException( "file is not a .gz file (by name)" );
        }

        tarFileName = tarFileName.substring( 0, dotGzIndex );
        Path tarFile = Paths.get( System.getProperty( "java.io.tmpdir" ) ).resolve( tarFileName );

        LOG.info( "Extracting {}...", tarGzFile );
        try( InputStream inputStream = new GzipCompressorInputStream( newInputStream( tarGzFile, READ ) );
             OutputStream outputStream = newOutputStream( tarFile, CREATE, TRUNCATE_EXISTING, WRITE ) )
        {
            IOUtils.copy( inputStream, outputStream );
            return tarFile;
        }
        catch( IOException e )
        {
            String message = "could not decompress GZIP file at '" + tarGzFile + "': " + e.getMessage();
            throw new UncheckedIOException( message, e );
        }
    }

    private void extractTar( @NotNull Path tarFile )
    {
        LOG.info( "Extracting {}...", tarFile );
        try( TarArchiveInputStream inputStream = new TarArchiveInputStream( newInputStream( tarFile, READ ) ) )
        {
            TarArchiveEntry entry = inputStream.getNextTarEntry();
            while( entry != null )
            {
                Path entryPath = this.unpackPath.resolve( entry.getName() );
                if( entry.isDirectory() )
                {
                    Files.createDirectories( entryPath );
                    Files.setAttribute( entryPath, "unix:mode", entry.getMode(), LinkOption.NOFOLLOW_LINKS );
                }
                else if( entry.isLink() )
                {
                    throw new UnsupportedOperationException( "links in tar not implemented yet: " + entry.getName() + "  <--->  " + entry
                            .getLinkName() );
                }
                else if( entry.isSymbolicLink() )
                {
                    Files.createDirectories( entryPath.getParent() );
                    Files.createSymbolicLink( entryPath, Paths.get( entry.getLinkName() ) );
                }
                else if( entry.isFile() )
                {
                    Files.createDirectories( entryPath.getParent() );

                    byte[] fileBytes = new byte[ ( int ) entry.getSize() ];
                    int bytesRead = inputStream.read( fileBytes );

                    try( OutputStream outputStream = newOutputStream( entryPath, CREATE, TRUNCATE_EXISTING, WRITE ) )
                    {
                        if( bytesRead > 0 )
                        {
                            outputStream.write( fileBytes );
                        }
                    }

                    Files.setAttribute( entryPath, "unix:mode", entry.getMode(), LinkOption.NOFOLLOW_LINKS );
                    Files.setLastModifiedTime( entryPath, FileTime.from( entry.getModTime().toInstant() ) );
                }
                else
                {
                    throw new UnsupportedOperationException( "unsupported entry type: " + entry.getName() );
                }

                entry = inputStream.getNextTarEntry();
            }
        }
        catch( IOException e )
        {
            String message = "could not extract TAR file at '" + tarFile + "': " + e.getMessage();
            throw new UncheckedIOException( message, e );
        }
    }
}
