/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node.impl;

import com.kfirs.common.node.NodeProcess;
import com.kfirs.common.node.NodeProcessFailedException;
import com.kfirs.common.node.NodeProcessResult;
import com.kfirs.common.util.lang.NotNull;
import com.kfirs.common.util.lang.Nullable;
import java.io.*;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.function.Consumer;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;

import static java.lang.ProcessBuilder.Redirect.INHERIT;
import static java.lang.ProcessBuilder.Redirect.PIPE;
import static java.util.Collections.unmodifiableList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 * @on 1/29/16.
 */
public class NodeProcessExecutor
{
    private static final Logger LOG = getLogger( NodeProcessExecutor.class );

    @NotNull
    private final Object mutex = new Object();

    @NotNull
    private final BlockingQueue<NodeTask> queue = new LinkedBlockingQueue<>( 32 );

    @NotNull
    private final TaskExecutor taskExecutor;

    public NodeProcessExecutor()
    {
        this.taskExecutor = new TaskExecutor();
        this.taskExecutor.start();
    }

    @PreDestroy
    public void stop()
    {
        this.taskExecutor.stop();
    }

    @NotNull
    NodeProcess scheduleNodeProcess( @NotNull String name,
                                     @NotNull Path nodeDir,
                                     @NotNull Path workDir,
                                     @NotNull List<String> command,
                                     @NotNull Consumer<String> stdoutConsumer,
                                     @NotNull Consumer<String> stderrConsumer,
                                     @Nullable Consumer<NodeProcessResult> resultConsumer )
    {
        synchronized( this.mutex )
        {
            return this.queue.stream()
                             .filter( nodeTask -> nodeTask.nodeDir.equals( nodeDir ) )
                             .filter( nodeTask -> nodeTask.workDir.equals( workDir ) )
                             .filter( nodeTask -> nodeTask.commandList.equals( command ) )
                             .findFirst()
                             .orElseGet( () ->
                                         {
                                             NodeTask task = new NodeTask( name, nodeDir, workDir, command, stdoutConsumer, stderrConsumer, resultConsumer );
                                             try
                                             {
                                                 this.queue.put( task );
                                                 return task;
                                             }
                                             catch( InterruptedException e )
                                             {
                                                 String msg = "interrupted while scheduling Node/NPM process '" + name + "' in '" + workDir + "' with command '" + command + "'";
                                                 throw new IllegalStateException( msg, e );
                                             }
                                         } );
        }
    }

    class NodeTask extends CompletableFuture<NodeProcessResult> implements NodeProcess, NodeProcessResult
    {
        @NotNull
        private final String name;

        @NotNull
        private final Path nodeDir;

        @NotNull
        private final Path workDir;

        @NotNull
        private final List<String> commandList;

        @NotNull
        private final Consumer<String> stdoutConsumer;

        @NotNull
        private final Consumer<String> stderrConsumer;

        @Nullable
        private final Consumer<NodeProcessResult> resultConsumer;

        @Nullable
        private volatile Process process;

        NodeTask( @NotNull String name,
                  @NotNull Path nodeDir,
                  @NotNull Path workDir,
                  @NotNull List<String> commandList,
                  @NotNull Consumer<String> stdoutConsumer,
                  @NotNull Consumer<String> stderrConsumer,
                  @Nullable Consumer<NodeProcessResult> resultConsumer )
        {
            this.name = name;
            this.nodeDir = nodeDir;
            this.workDir = workDir;
            this.commandList = unmodifiableList( new LinkedList<>( commandList ) );
            this.stdoutConsumer = stdoutConsumer;
            this.stderrConsumer = stderrConsumer;
            this.resultConsumer = resultConsumer;
        }

        @NotNull
        @Override
        public Path getWorkDirectory()
        {
            return this.workDir;
        }

        @NotNull
        @Override
        public List<String> getCommand()
        {
            return this.commandList;
        }

        @Override
        public boolean isAlive()
        {
            Process process = this.process;
            return process != null && process.isAlive();
        }

        private void run() throws InterruptedException
        {
            Process process = this.process;
            if( process != null )
            {
                return;
            }

            // start process
            try
            {
                LOG.debug( "Running Node at '{}' with command: {}", this.workDir, this.commandList );
                ProcessBuilder processBuilder = new ProcessBuilder();

                // ensure the "<node-installation>/bin" directory is in the "PATH" env variable of the process
                // this is required because some NPM packages install scripts run additional Node commands, and they
                // do so stupidly, by just running a "node ..." command! without ensuring they do it using the same
                // Node installation they are running in. So, we ensure they get our own Node install by prepending it
                // to the path of OUR process (not for the whole OS).
                String newPath = this.nodeDir.resolve( "bin" ) + File.pathSeparator + System.getenv( "PATH" );
                processBuilder.environment().put( "PATH", newPath );

                // execute
                this.process = process = processBuilder.command( this.commandList )
                                                       .directory( this.workDir.toFile() )
                                                       .redirectInput( INHERIT )
                                                       .redirectOutput( PIPE )
                                                       .redirectError( PIPE )
                                                       .start();
            }
            catch( Throwable e )
            {
                this.completeExceptionally( e );
                return;
            }

            // setup stdout and stderr redirection to SLF4J
            redirectOutput( process.getInputStream(), this.stdoutConsumer );
            redirectOutput( process.getErrorStream(), this.stderrConsumer );

            // setup listener that will wait until process has died, and set this future's result
            try
            {
                process.waitFor();
                this.complete( this );
            }
            catch( InterruptedException e )
            {
                throw e;
            }
            catch( Throwable e )
            {
                this.completeExceptionally( e );
            }

            // run result consumer
            if( this.resultConsumer != null )
            {
                try
                {
                    this.resultConsumer.accept( this );
                }
                catch( Exception e )
                {
                    LOG.warn( "Result consumer failed: {}", e.getMessage(), e );
                }
            }
        }

        @Override
        public void destroy( long timeout, @NotNull TimeUnit timeUnit ) throws InterruptedException, TimeoutException
        {
            if( isDone() )
            {
                return;
            }

            Process process = this.process;
            if( process == null )
            {
                throw new IllegalStateException( "not running" );
            }

            process.destroy();
            if( !process.waitFor( timeout, timeUnit ) )
            {
                throw new TimeoutException( "node process not terminated" );
            }
        }

        @Override
        public void destroyForcibly( long timeout, @NotNull TimeUnit timeUnit )
                throws InterruptedException, TimeoutException
        {
            if( isDone() )
            {
                return;
            }

            Process process = this.process;
            if( process == null )
            {
                throw new IllegalStateException( "not running" );
            }
            else if( !process.destroyForcibly().waitFor( timeout, timeUnit ) )
            {
                throw new TimeoutException( "node process not terminated" );
            }
        }

        @NotNull
        @Override
        public NodeProcessResult waitFor() throws InterruptedException
        {
            try
            {
                return get();
            }
            catch( CancellationException e )
            {
                throw new NodeProcessFailedException( "cancelled", e, this );
            }
            catch( ExecutionException e )
            {
                Throwable cause = e.getCause();
                if( cause instanceof NodeProcessFailedException )
                {
                    throw ( NodeProcessFailedException ) cause;
                }
                else
                {
                    throw new NodeProcessFailedException( cause.getMessage(), cause, this );
                }
            }
        }

        @NotNull
        @Override
        public NodeProcessResult waitFor( long duration, @NotNull TimeUnit timeUnit )
                throws InterruptedException, TimeoutException
        {
            try
            {
                return get( duration, timeUnit );
            }
            catch( CancellationException e )
            {
                throw new NodeProcessFailedException( "cancelled", e, this );
            }
            catch( ExecutionException e )
            {
                Throwable cause = e.getCause();
                if( cause instanceof NodeProcessFailedException )
                {
                    throw ( NodeProcessFailedException ) cause;
                }
                else
                {
                    throw new NodeProcessFailedException( cause.getMessage(), cause, this );
                }
            }
        }

        @NotNull
        @Override
        public NodeProcess getProcess()
        {
            return this;
        }

        @Override
        public int getExitCode()
        {
            try
            {
                waitFor();
            }
            catch( InterruptedException e )
            {
                LOG.warn( "Detected a call to 'getExitCode()' before process has finished (should not happen since access to 'getExitCode()' should only be provided after process has died)" );
                throw new IllegalStateException( e );
            }

            Process process = this.process;
            if( process == null )
            {
                throw new IllegalStateException( "node process could not start" );
            }
            return process.exitValue();
        }

        @Override
        public boolean equals( Object o )
        {
            if( this == o )
            {
                return true;
            }
            if( o == null || getClass() != o.getClass() )
            {
                return false;
            }
            NodeTask that = ( NodeTask ) o;
            return Objects.equals( this.nodeDir, that.nodeDir ) &&
                   Objects.equals( this.workDir, that.workDir ) &&
                   Objects.equals( this.commandList, that.commandList );
        }

        @Override
        public int hashCode()
        {
            return Objects.hash( this.nodeDir, this.workDir, this.commandList );
        }

        private void redirectOutput( @NotNull InputStream stream, @NotNull Consumer<String> consumer )
        {
            StreamRedirector redirector = new StreamRedirector( stream, consumer );
            String threadName = "node-redirector-" + System.identityHashCode( stream );
            Thread thread = new Thread( redirector, threadName );
            thread.setUncaughtExceptionHandler(
                    ( t, e ) -> LOG.error( "Unexpected exception thrown from Node stdout/stderr gobbler: {}",
                                           e.getMessage(),
                                           e ) );
            thread.setDaemon( true );
            thread.start();
        }

        /**
         * Reads given stream of output (can be either stdout or stderr) and redirects it to a line consumer.
         */
        private class StreamRedirector implements Runnable
        {
            @NotNull
            private final BufferedReader stream;

            @NotNull
            private final Consumer<String> consumer;

            private StreamRedirector( @NotNull InputStream stream, @NotNull Consumer<String> consumer )
            {
                this.stream = new BufferedReader( new InputStreamReader( stream ) );
                this.consumer = consumer;
            }

            @Override
            public void run()
            {
                try
                {
                    String line = this.stream.readLine();
                    while( line != null )
                    {
                        this.consumer.accept( line );
                        line = this.stream.readLine();
                    }
                }
                catch( IOException e )
                {
                    LOG.error( "Error reading from process stdout/stderr: {}", e.getMessage(), e );
                }
            }
        }
    }

    private class TaskExecutor implements Runnable
    {
        @Nullable
        private Thread thread;

        private void start()
        {
            synchronized( NodeProcessExecutor.this.mutex )
            {
                Thread thread = this.thread;
                if( thread != null )
                {
                    return;
                }

                thread = new Thread( this, "NodeProcessExecutor" );
                thread.setUncaughtExceptionHandler(
                        ( t, e ) -> LOG.error( "Unexpected exception thrown from Node process executor: {}",
                                               e.getMessage(),
                                               e ) );
                thread.setDaemon( true );
                thread.start();
                this.thread = thread;
            }
        }

        private void stop()
        {
            Thread thread;
            synchronized( NodeProcessExecutor.this.mutex )
            {
                thread = this.thread;
                if( thread == null )
                {
                    return;
                }
                thread.interrupt();
            }

            try
            {
                thread.join();
            }
            catch( InterruptedException e )
            {
                LOG.warn( "Interrupted while trying to stop node process executor" );
            }
            finally
            {
                this.thread = null;
            }
        }

        @Override
        public void run()
        {
            while( true )
            {
                NodeTask task;
                synchronized( NodeProcessExecutor.this.mutex )
                {
                    task = queue.poll();
                }

                if( task == null )
                {
                    //
                    // sleep for a while and then try again
                    //
                    try
                    {
                        Thread.sleep( 500 );
                    }
                    catch( InterruptedException e )
                    {
                        LOG.warn( "Node execution queue has been interrupted" );
                        break;
                    }
                }
                else
                {
                    //
                    // otherwise, we have a task! run it, wait for it to finish, and log result in debug log
                    //
                    LOG.info( "Running Node task: {}", task.name );
                    try
                    {
                        task.run();
                        NodeProcessResult result = task.waitFor();
                        LOG.debug( "Task '{}' finished with exit code: {}", task.name, result.getExitCode() );
                    }
                    catch( InterruptedException e )
                    {
                        LOG.warn( "Node executor interrupted while running task '{}' - will attempt to stop it", task.name );
                        try
                        {
                            task.destroyForcibly( 30, TimeUnit.SECONDS );
                        }
                        catch( InterruptedException | TimeoutException e1 )
                        {
                            LOG.error( "Failed stopping Node task '{}' (might still be running!)", task.name );
                        }
                        break;
                    }
                    catch( Throwable e )
                    {
                        LOG.error( "Failed while waiting for task '{}' to finish (might still be running)", task.name, e );
                    }
                }
            }
        }
    }
}
