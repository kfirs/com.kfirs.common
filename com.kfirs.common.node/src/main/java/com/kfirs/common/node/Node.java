/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.node;

import com.kfirs.common.util.lang.NotNull;
import java.nio.file.Path;

/**
 * @author arik
 * @on 12/24/15.
 */
public interface Node
{
    @NotNull
    Path getPath();

    @NotNull
    NodeProcessBuilder nodeBuilder();

    @NotNull
    NodeProcessBuilder npmBuilder();
}
