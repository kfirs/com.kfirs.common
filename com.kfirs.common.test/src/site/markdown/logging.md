## Logging support

Since `com.kfirs.common.test` depends on `spring-boot-starter-logging`,
you get `SLF4J API`, `Logback` (core & static), and `SLF4J` bridges to 
`Log4J` and `commons-logging` (for free, sort of speak). It also means,
you don't need to create any `Logback` configuration also - since this
module also provides `logback-test.xml` on the classpath!

There is, however, one thing that needs your intervention - but only if
you also want to route logging from JUL (`java.util.logging`) to `SLF4J`
as well. Usually, you don't need this, but if you do, you only need to
add a `JUnit` rule (`@Rule`) which will do this for you:

    public class MyTests
    {
        @Rule
        public JulToSlfRule julToSlfRule = new JulToSlfRule();
        
        ...
        // your tests here...
        ...
    }

