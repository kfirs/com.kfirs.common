## JUnit work path rule

This module provides the `WorkPathRule` that can be used inside `JUnit`
tests to provide a sterile, self-cleaning workspace for each test case.
The rule will avoid deleting your workspace if your test threw an
exception - allowing you to inspect your workspace on failures. But
don't worry about having many "left-overs" from previous runs, because
the rule will re-use the same directory consistently, so at most you'll
only have one "left-over", until the test passes (in which case the 
workspace _will_ be deleted).

The rule is implemented as a `JUnit` rule, meaning it is notified before
a test is started, and after it is finished. This enables it to 
initialize the workspace for you. 

### Usage ###

To use, first add `com.kfirs.common.test` dependency to your POM, like
this:

    <dependency>
      <groupId>com.kfirs.common</groupId>
      <artifactId>com.kfirs.common.test</artifactId>
      <version>${project.version}</version> <!-- use latest released version if possible -->
      <scope>test</scope> <!-- you only need this for your tests -->
    </dependency>
    
Then, in your `JUnit` tests, use it as follows:

    public class MyTests
    {
        @Rule
        public WorkPathRule workPath = new WorkPathRule();
    
        @Before
        public void setUp() throws Exception
        {
            // you can start using your workspace in @Before methods too!
            Path cleanWorkspace = this.workPath.getPath();
            
            // initialize some files in your workspace... 
        }
    
        @After
        public void tearDown() throws Exception
        {
            // no need to clean your workspace in @After methods!
            // it will be done automatically!
        }
    
        @Test
        public void testDownload() throws IOException
        {
            // you can start using your workspace in @Test methods too!
            Path cleanWorkspace = this.workPath.getPath();
            
            // test your code using your workspace here...
        }
    }

### Customizations ###

You can customize the rule not to clean up your workspace on set-up or
tear down, by calling `setDeleteOnSetUp(false)` and/or `setDeleteOnTearDown(false)` 
respectively on your rule instance **as part of its creation** (do NOT
call these methods inside your `@Before` methods, because by then, it
would have been too late - `JUnit` has already called the rule.)

For more advanced customizations, such as deciding _where_ to place the
temporary workspace (by default it is placed in your temporary directory
of the machine) or performing some post-processing before the test
begins, extend the `AbstractTemporaryDirectoryRule` with your own implementation, 
and override one of its `protected` hooks with your own logic.
