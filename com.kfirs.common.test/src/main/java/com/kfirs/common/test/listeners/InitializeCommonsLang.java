/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.test.listeners;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

/**
 * @author arik
 * @on 3/23/17.
 */
public class InitializeCommonsLang extends AbstractTestExecutionListener
{
    @Override
    public void beforeTestClass( TestContext testContext ) throws Exception
    {
        ToStringBuilder.setDefaultStyle( ToStringStyle.SHORT_PREFIX_STYLE );
    }
}
