/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.test.listeners;

import java.lang.management.ManagementFactory;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

/**
 * @author arik
 * @on 2/6/17.
 */
public class AspectJValidatingListener extends AbstractTestExecutionListener
{
    @Override
    public void beforeTestClass( TestContext testContext ) throws Exception
    {
        new AspectJAgentValidator().validateAspectJWeaverAgentPresent();
    }

    /**
     * @todo this duplicates the same class in {@code com.kfirs.common.util.aop.AspectJAgentValidator}
     */
    public class AspectJAgentValidator
    {
        private final Pattern agentPattern;

        public AspectJAgentValidator()
        {
            this( "-javaagent:.*aspectjweaver.*.jar" );
        }

        public AspectJAgentValidator( String agentPattern )
        {
            this.agentPattern = Pattern.compile( "-javaagent:.*aspectjweaver.*.jar" );
        }

        public void validateAspectJWeaverAgentPresent()
        {
            Stream<String> argumentStream = ManagementFactory.getRuntimeMXBean().getInputArguments().stream();
            if( argumentStream.noneMatch( arg -> this.agentPattern.matcher( arg ).matches() ) )
            {
                System.err.println( "AspectJ weaver not found on JVM execution!" );
                System.err.println( "Use \"-javaagent:/path/to/aspectjweaver.jar\"" );
                System.exit( 1 );
            }
        }
    }
}
