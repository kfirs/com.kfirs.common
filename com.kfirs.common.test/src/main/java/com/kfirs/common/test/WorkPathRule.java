/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.test;

/**
 * @author arik
 * @on 12/25/15.
 */
public class WorkPathRule extends AbstractTemporaryDirectoryRule
{
    public WorkPathRule()
    {
        this( "work" );
    }

    public WorkPathRule( String qualifier )
    {
        super( qualifier );
    }

    public boolean isDeleteOnSetUp()
    {
        return this.deleteOnSetUp;
    }

    public WorkPathRule setDeleteOnSetUp( boolean deleteOnSetUp )
    {
        this.deleteOnSetUp = deleteOnSetUp;
        return this;
    }

    public boolean isDeleteOnTearDown()
    {
        return this.deleteOnTearDown;
    }

    public WorkPathRule setDeleteOnTearDown( boolean deleteOnTearDown )
    {
        this.deleteOnTearDown = deleteOnTearDown;
        return this;
    }
}
