/*
 * Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either expressed or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */
package com.kfirs.common.test;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.ZonedDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import static java.nio.file.Files.createDirectories;

/**
 * @author arik
 * @on 12/25/15.
 */
public abstract class AbstractTemporaryDirectoryRule implements TestRule
{
    private final Logger logger = Logger.getLogger( getClass().getName() );

    private final String qualifier;

    protected boolean deleteOnSetUp = true;

    protected boolean deleteOnTearDown = true;

    private Path path;

    public AbstractTemporaryDirectoryRule( String qualifier )
    {
        this.qualifier = qualifier.isEmpty() ? ZonedDateTime.now().toString() : qualifier;
    }

    @Override
    public Statement apply( Statement base, Description description )
    {
        return new Statement()
        {
            @Override
            public void evaluate() throws Throwable
            {
                AbstractTemporaryDirectoryRule.this.path = createPath( description );
                try
                {
                    base.evaluate();

                    // not putting the tear-down in a "finally" clause, so that if the test fails, we leave it intact for the developer to inspect
                    tearDownPath( description );
                }
                finally
                {
                    AbstractTemporaryDirectoryRule.this.path = null;
                }
            }
        };
    }

    public Path getPath()
    {
        if( this.path == null )
        {
            throw new IllegalStateException( "no test is running" );
        }
        return path;
    }

    protected Path createPath( Description description ) throws IOException
    {
        Class<?> testClass = description.getTestClass();
        Path workDir = getCommonBase().resolve( testClass.getPackage().getName().replace( '.', '/' ) )
                                      .resolve( testClass.getSimpleName() )
                                      .resolve( description.getMethodName() )
                                      .resolve( this.qualifier );

        //
        // delete work dir if it exists (start from scratch)
        //
        if( this.deleteOnSetUp && Files.exists( workDir ) )
        {
            delete( workDir );
        }

        //
        // create (and possibly initialize) the work dir
        //
        createDirectories( workDir );
        postProcessPath( workDir );

        //
        // done!
        //
        this.logger.log( Level.FINE, "Workspace created for '" + description.getTestClass().getSimpleName() + "." + description.getMethodName() + "' at: " + workDir );
        return workDir;
    }

    protected Path getCommonBase()
    {
        return Paths.get( System.getProperty( "java.io.tmpdir" ) ).resolve( "tests" );
    }

    protected void postProcessPath( Path path ) throws IOException
    {
        // no-op
    }

    protected void tearDownPath( Description description )
            throws IOException
    {
        if( this.deleteOnTearDown && this.path != null )
        {
            this.logger.log( Level.FINE, "Workspace deleted for test '" + description.getTestClass().getSimpleName() + "." + description.getMethodName() + "'" );
            if( Files.exists( this.path ) )
            {
                delete( this.path );
            }
        }
    }

    private void delete( Path path ) throws IOException
    {
        Files.walkFileTree( path, new SimpleFileVisitor<Path>()
        {
            @Override
            public FileVisitResult visitFile( Path file, BasicFileAttributes attrs ) throws IOException
            {
                Files.delete( file );
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory( Path dir, IOException exc ) throws IOException
            {
                if( exc != null )
                {
                    throw exc;
                }
                else
                {
                    Files.delete( dir );
                    return FileVisitResult.CONTINUE;
                }
            }
        } );
    }
}
