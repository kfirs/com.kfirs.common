<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2016-2017 Arik Kfir and relevant collaborating code authors.
  ~ Licensed under the Apache License, Version 2.0 (the "License"); you may not use
  ~ this file except in compliance with the License. You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software distributed under
  ~ the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
  ~ OF ANY KIND, either expressed or implied.
  ~
  ~ See the License for the specific language governing permissions and limitations under the License.
  ~
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.kfirs.common</groupId>
        <artifactId>com.kfirs.common.parent</artifactId>
        <version>1.2.7-SNAPSHOT</version>
        <relativePath>../com.kfirs.common.parent/pom.xml</relativePath>
    </parent>

    <!-- general project properties -->
    <groupId>com.kfirs.common</groupId>
    <artifactId>com.kfirs.common.test</artifactId>
    <version>1.2.7-SNAPSHOT</version>
    <name>KFIRS :: Common :: Testing</name>
    <description>Common test utilities</description>
    <packaging>jar</packaging>
    <url>http://www.kfirs.com/${project.groupId}/${project.artifactId}/</url>

    <!-- scm -->
    <scm>
        <connection>scm:git:git@bitbucket.org:kfirs/${project.groupId}.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:kfirs/${project.groupId}.git</developerConnection>
        <url>https://bitbucket.org/kfirs/${project.groupId}/src</url>
        <tag>HEAD</tag>
    </scm>

    <!-- distribution -->
    <distributionManagement>
        <site>
            <id>www</id>
            <name>Web site</name>
            <url>scpexe://deploy.www.kfirs.com/opt/kfirs/www/html/${project.groupId}/${project.artifactId}/</url>
        </site>
    </distributionManagement>

    <!-- reporting -->
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <inherited>false</inherited>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>dependencies</report>
                            <report>dependency-convergence</report>
                            <report>dependency-info</report>
                            <report>dependency-management</report>
                            <report>plugin-management</report>
                            <report>plugins</report>
                            <report>summary</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <reportSets>
                    <reportSet>
                        <id>default</id>
                        <reports>
                            <report>javadoc</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
            </plugin>
        </plugins>
    </reporting>

    <!-- dependencies -->
    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-logging</artifactId>
            <!-- NOT in 'test' scope! since we're using JUnit in our main code here! -->
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <!-- NOT in 'test' scope! since we're using JUnit in our main code here! -->
        </dependency>

        <!-- utils -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.5</version>
        </dependency>

    </dependencies>

</project>
